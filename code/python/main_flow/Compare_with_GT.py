from skimage import io
import os
import cv2
import numpy as np
from glob import glob
import pandas as pd 
import itertools
import argparse
from PIL import Image
from Kaze_detector import kaze_detector
import time
import warptransform
from Error_Correction import ErrorCorrection
from binarize import binarisation
from decode_bin_img_barcode import codeword_combo, codeword_extraction, clean_codeword, Decode

# For real image
img_mode = 1

template_path = 'Pdf417_barcodes_c'
GroundTruth_path = 'worked_GTfile.xlsx'
txt_code_path  = 'PDF417codes/pdf417_text_code.txt'
code_comb_path = 'PDF417codes/pdf417_codeword_combos.txt'

Detector=kaze_detector(template_path)

files = pd.read_excel(GroundTruth_path)
files=files.values.tolist()
for i in range(len(files)):
    image_path    ='worked/' + files[i][0]
    print('Image Name : ', files[i][0])
    img_save_path = '/home/recode/Desktop/code/crate_Result'
    save_image = False
    
    start = time.time()
    
    img = cv2.imread(image_path)
    cropped_warp = None
    if img_mode:
        '''Do detection, cropping and transformation only for real image''' 
        marked_image, box      = Detector.detect(img, 10)
        if box is not None:
            cropped_warp           = warptransform.four_point_transform(img, box)
            overlaid               = Detector.overlay_corners(img, box)
        else:
            print('Barcode not detected')
    else:
        cropped_warp = img
        
        
    if cropped_warp is not None:
        gray      = cv2.cvtColor(cropped_warp, cv2.COLOR_BGR2GRAY)
        
        binaryimg = binarisation(gray)
        
        codeword_list = codeword_combo(code_comb_path)
            
        wholewords,wholewordrows = codeword_extraction(binaryimg)
        Decoded =''
        if len(wholewords) > 3:    # check barcode or not
            cleaned_codewords, cleaned_pattern = clean_codeword(codeword_list, wholewords)
            
            if len(cleaned_codewords)>2:   #check the codewords extracted
                error_corr_len = len(cleaned_codewords) - cleaned_codewords[0]  
                
                EC = ErrorCorrection()
                if cleaned_codewords[0]   > 0:
                    error_corrected_words =EC.Testcodewords(cleaned_codewords.copy(), error_corr_len)
                    
                    if error_corrected_words == 0:
                        Decoded=Decode(txt_code_path, cleaned_codewords)
                        print('Decoded Data')
                        print(Decoded)
                    elif error_corrected_words == -1 and len(cleaned_codewords)<4:
                        print('cant decode')
                    elif error_corrected_words == -1 and len(cleaned_codewords)>4:
                        Decoded=Decode(txt_code_path, cleaned_codewords)
                        print('Decoded Data')
                        print(Decoded)
                    else:
                        Decoded = Decode(txt_code_path, error_corrected_words)
                        print('Decoded Data')
                        print(Decoded)
                else:
                    print('No Barcodes found in this image')  
            else:
                print('No Barcodes found in this image')    
        else:
            print('cant extract codewords')
        end = time.time()
        if Decoded == files[i][1]:
            print('The file Decoded correctly')
        else:
            print('The file Decoded Wrongly')
            
        time_interval = end - start   
        print('Execution time : ', time_interval)
        
        if save_image and img_mode:
            '''Save the debug images of real images only'''
            cv2.imwrite(os.path.join(img_save_path, '1.InputImage.png'), img)
            cv2.imwrite(os.path.join(img_save_path, '2.DetectedImage.png'), marked_image)
            cv2.imwrite(os.path.join(img_save_path, '3.FourCorners.png'), overlaid)
            cv2.imwrite(os.path.join(img_save_path, '4.Cropped_Skewcorrected.png'), cropped_warp)
            cv2.imwrite(os.path.join(img_save_path, '5.Binarised.png'), binaryimg)
    else:
        print('barcode not decoded')
        
    print('\n')