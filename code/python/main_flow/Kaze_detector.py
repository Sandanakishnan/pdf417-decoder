import cv2
import math
from scipy import ndimage
import numpy as np
from detection import KeypointObjectDetector

import time
import random
import pandas

class kaze_detector():
    def __init__(self,Train_dir):
        '''Extract and keep the Keypoint Descriptors'''
        detector = KeypointObjectDetector(Train_dir,'KAZE')
        self.detector=detector
    
    def detect(self,frame, exp_factor):
        '''Detect the presence of PDF417 Barcode'''
        detector=self.detector
        start=time.time()
        img = frame.copy()
        marked_image, obj_dict,homography = detector.from_image(img)

        if obj_dict:
            dict_ob=obj_dict['pdf417'][0]['box']
            x,y,w,h=dict_ob
            crp_img=frame[x - exp_factor : w + exp_factor, y-exp_factor:h+exp_factor, :]
            if sum(1 for number in dict_ob if number < 0)<=0 and len(crp_img)>70 and len(crp_img[1:])>70:
                box=np.zeros([4,2])
                box[0,:]=homography[0,0,:]
                
                box[1,:]=homography[1,0,:]
                
                box[2,:]=homography[2,0,:]
                
                box[3,:]=homography[3,0,:]
                
                box[np.where(box<0)]=1
                
        stop=time.time() - start
        print('Detection Time : ',stop)
        return marked_image, box
    
    def overlay_corners(self, img, box): 
        '''Overlay 4 Detected Corners'''
        img_copy = img.copy()
        for i in range(len(box)):
            cv2.circle(img_copy, (int(box[i,0]),int(box[i,1])), 3, [0,0,255], -1)
        return img_copy
