from skimage import io
import os
import cv2
import numpy as np
import pandas as pd 
import itertools
import argparse
from PIL import Image
from Kaze_detector import kaze_detector
import time
import warptransform
from Error_Correction import ErrorCorrection
from binarize import binarisation
from decode_bin_img_barcode import codeword_combo, codeword_extraction, clean_codeword, Decode

# Main Code

'''For console run'''

'''
# For synthetic image
img_mode = 0
image_path    = '/media/Work/Images/Barcode_Reader/Test_Images/Synthetic/01.png'
template_path = '/media/Work/Programs/Barcode_Reader/Code/PDF417_Decoder'\
                '/Python/code-new-versionV1.5/Pdf417_barcodes_synth'
 '''


# For real image
img_mode = 1
# image_path    = '/media/Work/Programs/Barcode_Reader/Code/PDF417_Decoder'\
#                 '/Python/code-new-versionV1.5/worked/325.png'
# template_path = '/media/Work/Programs/Barcode_Reader/Code/PDF417_Decoder'\
#                 '/Python/code-new-versionV1.5/Pdf417_barcodes_c'
                



# img_save_path = '/media/Work/Programs/Barcode_Reader/Code/PDF417_Decoder'\
#                 '/Python/code-new-versionV1.5/crate_Result'

image_path    = '/home/recode/Desktop/code/worked/260.png'
template_path = 'Pdf417_barcodes_c'
                



img_save_path = '/home/recode/Desktop/code/crate_Result'
save_image = True

#For Command-line run
# parser = argparse.ArgumentParser(description="PDF417 Decoder")
# parser.add_argument('--img-mode', type=int, default=1, 
#                     help='set 1 to run on real image, 0 to run on synthetic image')
# parser.add_argument('--img-path', type=str, help='path to input image')
# parser.add_argument('--template-path', type=str, help='path to template image folder')
# parser.add_argument('--img-save-path', type=str, default = '', help='path to debug image save')

# args = parser.parse_args()

# if args.img_save_path:
#     save_image = True
#     img_save_path = args.img_save_path
# else:
#     save_image = False
    
# template_path  = args.template_path
# image_path     = args.img_path
# img_mode       = args.img_mode

txt_code_path  = 'PDF417codes/pdf417_text_code.txt'
code_comb_path = 'PDF417codes/pdf417_codeword_combos.txt'


start = time.time()

img = cv2.imread(image_path)

if img_mode:
    '''Do detection, cropping and transformation only for real image'''
    Detector=kaze_detector(template_path)
    
    marked_image, box      = Detector.detect(img, 10)
    cropped_warp           = warptransform.four_point_transform(img, box)
    overlaid               = Detector.overlay_corners(img, box)
else:
    cropped_warp = img

gray      = cv2.cvtColor(cropped_warp, cv2.COLOR_BGR2GRAY)

binaryimg = binarisation(gray)

codeword_list = codeword_combo(code_comb_path)
    
wholewords,wholewordrows = codeword_extraction(binaryimg)

cleaned_codewords, cleaned_pattern = clean_codeword(codeword_list, wholewords)

error_corr_len = len(cleaned_codewords) - cleaned_codewords[0]  

EC = ErrorCorrection()

error_corrected_words =EC.Testcodewords(cleaned_codewords.copy(), error_corr_len)

end = time.time()

time_interval = end - start

if error_corrected_words == 0:
    Decoded=Decode(txt_code_path, cleaned_codewords)
    print('Decoded Data')
    print(Decoded)
elif error_corrected_words == -1:
    print('cant decode')
else:
    Decoded = Decode(txt_code_path, error_corrected_words)
    print('Decoded Data')
    print(Decoded)
    
print('Execution time', time_interval)

if save_image and img_mode:
    '''Save the debug images of real images only'''
    cv2.imwrite(os.path.join(img_save_path, '1.InputImage.png'), img)
    cv2.imwrite(os.path.join(img_save_path, '2.DetectedImage.png'), marked_image)
    cv2.imwrite(os.path.join(img_save_path, '3.FourCorners.png'), overlaid)
    cv2.imwrite(os.path.join(img_save_path, '4.Cropped_Skewcorrected.png'), cropped_warp)
    cv2.imwrite(os.path.join(img_save_path, '5.Binarised.png'), binaryimg)
