import numpy as np

def binarisation(gray):
    '''Converts a Grayscale image to Binary image'''
    local_thresh=[]
    step_size = 1
    for i in range(0,len(gray),step_size):
        for j in range(0,len(gray[0,:]),step_size):
            imblk=gray[i:i+step_size,j:j+step_size]
            local_thresh.append(imblk.mean())
    
    global_thresh=(sum(local_thresh)/len(local_thresh))*0.95
    
    binaryimg=np.zeros(gray.shape)
    
            
    for i in range(len(gray)):
        for j in range(len(gray[0,:])):
            if gray[i,j]<global_thresh:
                binaryimg[i,j]=0
            else:
                binaryimg[i,j]=255
    return binaryimg