from skimage import io
import cv2
import numpy as np
import pandas as pd 
import itertools
from PIL import Image
import time

def add_quiet_zone(im):
    box = (15, 15, im.size[0]+15, im.size[1]+15)
    img = Image.new('L', (im.size[0]+30, im.size[1]+30), 'white')
    img.paste(im, box)
    return img

def reformat(row_data):
    '''Group the row-wise binary pixels into classes'''
    return [(i[0], len(list(i[1]))) for i in itertools.groupby(row_data)]
    
def filter_quitezone(row_data):
    '''Filter Quite Zone from Left and Right of Image'''
    if row_data[0][0] == 255:  row_data = row_data[1:]
    if row_data[-1][0] == 255: row_data = row_data[0:-1]
    return row_data

def codeword_combo(filename):
    '''Read Codeword Combo from a file'''
    f=open(filename,'r')
    code_combos=f.read()
    f.close()
    
    code_combos=code_combos.split('\n')
    codeword_list=[]
    for i in range(len(code_combos)):
        codes=code_combos[i].split(' ')
        codeword_list.append(codes)    
    codeword_list=np.asarray(codeword_list)
    return codeword_list

def Text_codeword(filename):
    '''Read Text Codeword from a file'''
    f=open(filename,'r')
    code=f.read()
    f.close()
    code=code.split('\n')
    Textcode=[]
    for j in range(31):
        Textcode.append(code[j].split('\t')[1:])
    
    Textcode=pd.DataFrame(Textcode[1:],columns=Textcode[0])
    return Textcode

def find_x_y_codewordlen(gray):
    if gray[0,7]!=gray[0,8]:
        bar_x=1
        bar_y=3
        codewordlen=17
    elif gray[0,15]!=gray[0,16]:
        bar_x=2
        bar_y=6
        codewordlen=17*bar_x
    return bar_x,bar_y,codewordlen

def get_latch_shiftlist():
    '''Get LAtch Shift List'''
    latchlist={'ll':['Lower',0],'as':['Alpha',1],'al':['Alpha',0],'ml':['Mixed',0],'pl':['Punctuation',0],'ps':['Punctuation',1]}
    return latchlist

def Texttype_decode(filename, code, Prev_char=None,Preprev_char=None,Prev_mode=None,Preprev_mode=None):
    '''Decode the Text type codes'''
    S_c=code%30
    F_c=(code-S_c)/30
    Textcode=Text_codeword(filename)
    latchlist=get_latch_shiftlist()
    if Prev_char is None and Textcode['Alpha'][F_c] in latchlist:
        first_char=Textcode['Alpha'][F_c]
        textmode=Textcode['Alpha'][F_c]
        mode=latchlist[textmode]
        second_char=Textcode[mode[0]][S_c]
        pm=mode[0]
        ppm='Alpha'
    elif Prev_char is None and Textcode['Alpha'][F_c] not in latchlist:
        first_char=Textcode['Alpha'][F_c]
        ppm='Alpha'
        if first_char in latchlist:
            mode=latchlist[first_char]
            second_char=Textcode[mode[0]][S_c]
            pm=mode[0]
        else:
            second_char=Textcode['Alpha'][S_c]
            pm='Alpha'
    elif Prev_char is not None and Prev_char in latchlist:
        mode=latchlist[Prev_char]
        first_char=Textcode[mode[0]][F_c]
        ppm=mode[0]
        if mode[1]==0:
            if first_char in latchlist:
                mode = latchlist[first_char]
            second_char=Textcode[mode[0]][S_c]
            pm=mode[0]
        else:
            second_char=Textcode[Preprev_mode][S_c]
            pm=Preprev_mode
    else:
        if Preprev_char in latchlist:
            if latchlist[Preprev_char][1]==1:
                Prev_mode = Preprev_mode
        first_char=Textcode[Prev_mode][F_c]
        ppm=Prev_mode
        if first_char in latchlist:
            mode=latchlist[first_char]
            second_char=Textcode[mode[0]][S_c]
            pm=mode[0]
        else:
            second_char=Textcode[Prev_mode][S_c]
            pm=Prev_mode
        
            
    return first_char,second_char,ppm,pm

def Bytetype_decode(codewords):
    '''Decode the Byte type codes'''
    codewordvalue=codewords[0]
    for c in range(1,len(codewords)):
        codewordvalue=(codewordvalue*900)+codewords[c]
    decoded=[]
    decoded_data=''
    decoded.append(codewordvalue%256)
    decoded_data=chr(int(decoded[-1]))+decoded_data
    for c in range(5):
        codewordvalue=(codewordvalue-decoded[-1])/256
        decoded.append(codewordvalue%256)
        decoded_data=chr(int(decoded[-1]))+decoded_data
    return decoded_data

def Numerictype_decode(codewords):
    '''Decode the Numeric type decodes'''
    codewordvalue=codewords[0]
    for c in range(1,len(codewords)):
        codewordvalue=(codewordvalue*900)+codewords[c]
    codewordvalue=str(codewordvalue)[1:]
    return codewordvalue

def calc_codeword(row):
    '''Calculate codeword'''
    # row=add_quiet_zone(row)
    sets=[]
    setdata = []
    dat=reformat(row)
    dat=filter_quitezone(dat)
    cds=[]
    codeword=[]
    for cc in range(len(dat)):
        
        if len(cds)<8:
            cds.append(dat[cc][1])
            codeword.extend([dat[cc][0]]*dat[cc][1])
        if len(cds)==8:
            codewordlen = len(codeword)
            minwidth=codewordlen/17
            code = []
            for c in cds:
               code.append(round(c/minwidth)) 
            
            sets.append(code)
            setdata.append(codeword)
            codeword=[]
            cds = []
            
    return sets,setdata

def codeword_extraction(binaryimg):
    '''Codeword Extraction'''
    wholewords=[]    
    wholewordrows=[]
    words=[]
    wordrows=[]
    
    for c in range(len(binaryimg)):
        row=binaryimg[c,:]
        per=len(row)*0.2
        zelen,onlen=len(np.where(row==0)[0]),len(np.where(row==255)[0])
        if zelen>per and onlen>per:
            codes,codedata=calc_codeword(row)
            words.append(codes)
            wordrows.append(codedata)
            wholewords.append(codes)
            wholewordrows.append(codedata)
    return wholewords,wholewordrows

def clean_codeword(codeword_list, wholewords):
    '''Clean the Code words'''
    codecnt=[]
    for code in wholewords:
        codecnt.append(len(code))  
    maxcodelen=max(set(codecnt), key = codecnt.count)      
    
    words=[]
    for code in wholewords:
        if len(code)==maxcodelen:
            words.append(code[1:-1])
    i=0
    RowNumbers = []
    Rowpatterns = []
    while i < len(words):   
        newrows=[]
        newrows.append(words[i])
        for k in range(i+1,len(words)):
            row_chk =len([j for j in range(len(words[i])) if words[i][j] == words[k][j]])
            if row_chk>2:
                newrows.append(words[k])
                i = k
        
        if len(newrows)>=3:
            for k in range(1, len(newrows[0]) - 1):
                coldata = []
                for word in newrows:
                    val=('').join([str(d) for d in word[k]])
                    if sum(word[k]) == 17 and val in codeword_list:
                        coldata.append(word[k])
                if len(coldata)==0:
                    coldata = [word[k] for word in newrows]
                    zeroind = np.where(np.asarray(coldata) == 0)
                    if len(zeroind[0]) != 0:
                        for c in range(len(zeroind[1])):
                            coldata[zeroind[0][c]][zeroind[1][c]]=1
                        
                        nonzerocoldata = []
                        for word in coldata:
                            val=('').join([str(d) for d in word])
                            if sum(word) == 17 and val in codeword_list:
                                nonzerocoldata.append(word)
                    else:
                        nonzerocoldata = []
                else:
                    nonzerocoldata = coldata
                
                if len(nonzerocoldata) == 0:
                    RowNumbers.append(0)
                    Rowpatterns.append(0)
                else:
                    val = ('').join([str(d) for d in nonzerocoldata[0]])
                    rowno=int(codeword_list[np.where(codeword_list==val)[0][0],0])
                    RowNumbers.append(rowno)
                    Rowpatterns.append(nonzerocoldata[0])
        else:
            i = i + 1
    
    return RowNumbers, Rowpatterns

def Decode(filename, RowNumbers):
    '''Decode'''
    Decoded_data=''
    latchlist=get_latch_shiftlist()
    
    Prev_char=None
    Preprev_char=None
    Prev_mode=None
    Preprev_mode=None
    codewords=[]
    fin_flg=0
    
    if RowNumbers[1]>=900:
        Pdf_mode = RowNumbers[1]
        start = 2
    else:
        Pdf_mode = 900
        start = 1
    temp_by_flg = 0
    lat_by_flg = 0
    for i in range(start,len(RowNumbers)):
        rowno = RowNumbers[i]
        dat = ''
        # if rowno >= 900:
        #     fin_flg=fin_flg+1
        if Pdf_mode==900 and fin_flg==0:
            # print('TextMode Encoding')
            if rowno in [900,902]:
                break
            if temp_by_flg == 1:
                dat = dat + chr(int(rowno))
                temp_by_flg = 2
                if Prev_char in latchlist:
                    if latchlist[Prev_char][1] == 1:
                        Prev_char = Preprev_char
                # Prev_mode = Preprev_mode
                # Prev_char = Preprev_char
            if rowno == 913:    #temporary shift to byte
                temp_by_flg =1
            
            if lat_by_flg == 1:
                dat = dat + chr(int(rowno))
            if rowno == 901:
                lat_by_flg = 1
                
            if temp_by_flg == 0 and lat_by_flg == 0:
                first_char,second_char,Preprev_mode,Prev_mode=Texttype_decode(filename,rowno,Prev_char,Preprev_char,Prev_mode,Preprev_mode)
                Prev_char=second_char
                Preprev_char=first_char
                
                if first_char not in latchlist:
                    if first_char=='space':
                        dat=dat+' '
                    else:
                        dat=dat+first_char
        
                if second_char not in latchlist:
                    if second_char=='space':
                        dat=dat+' '
                    else:
                        dat=dat+second_char
            if temp_by_flg == 2: temp_by_flg =0
                        
        elif (Pdf_mode==901 or Pdf_mode==924) and fin_flg==0:
            # print('Byte Mode Encoding')
            if rowno in [900,902]:
                break
            if Pdf_mode==901:#Its not multiple of 6
                codewords.append(rowno)
                if len(codewords)%5==0: #Multiple of 6
                    dat=Bytetype_decode(codewords)
                elif len(codewords)>5:
                    dat=chr(int(rowno))
                
            elif Pdf_mode==924:
                codewords.append(rowno)
                if len(codewords)%5==0:
                    dat=Bytetype_decode(codewords)
                elif len(codewords)>5:
                    dat=chr(int(rowno))
            
        elif Pdf_mode==902 and fin_flg==0:
            # print('Numeric mode Encoding')
            codewords.append(rowno)
            if rowno in [900,901,924]:
                break
            if (len(codewords)%15==0 and fin_flg==0) or (len(codewords)<15 and fin_flg==1):
                if fin_flg==1:
                    dat=Numerictype_decode(codewords[0:-1])
                else:
                    dat=Numerictype_decode(codewords)
            
        Decoded_data=Decoded_data+dat
    return Decoded_data
