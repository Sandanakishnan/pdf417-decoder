import os
import cv2
import copy
import shutil
from natsort import natsorted
from barcode_detector import BarcodeDetector


MODEL_PATH = './pdf417_model/PDF417_frozen_inference_graph_Gray_v2.pb'
INPUT_PATH = '/media/Work/Images/TVS ToteID/Text/BaumerBarcodeTest/PDF417/18_03_2021'
OUTPUT_PATH = '/media/Work/Codes/Private/pdf417-decoder/output/images/Method1'

OUTPUT_IMAGE_PATH = os.path.join(OUTPUT_PATH, '0_Input')
DETECTOR_OUTPUT_PATH = os.path.join(OUTPUT_PATH, '1_Detection')
CROP_PATH = os.path.join(OUTPUT_PATH, '2_Crop')

bd = BarcodeDetector(MODEL_PATH, 'GRAY')


def put_rectangle(img, bbox):
    return cv2.rectangle(img, (bbox[1], bbox[0]),
                         (bbox[3], bbox[2]), (0, 255, 0), 5)


def clean_dir(folder_path):
    if os.path.isdir(folder_path):
        shutil.rmtree(folder_path)
    os.makedirs(folder_path)


date = os.path.split(INPUT_PATH)[1]
hour_list = natsorted(os.listdir(INPUT_PATH))

for hour in hour_list:
    hour_path = os.path.join(INPUT_PATH, hour)
    track_list = natsorted(os.listdir(hour_path))

    for track in track_list:
        track_path = os.path.join(hour_path, track)
        image_list = natsorted(os.listdir(track_path))

        input_track_path = os.path.join(OUTPUT_IMAGE_PATH, date, hour, track)
        detector_track_path = os.path.join(DETECTOR_OUTPUT_PATH, date, hour, track)
        crop_track_path = os.path.join(CROP_PATH, date, hour, track)

        clean_dir(input_track_path)
        clean_dir(detector_track_path)
        clean_dir(crop_track_path)

        for image_file in image_list:
            image_file_path = os.path.join(track_path, image_file)
            print(f'image_file_path: {image_file_path}')
            image = cv2.imread(image_file_path, 0)

            image_name, ext = os.path.splitext(image_file)
            print(f'image_name: {image_name}')

            input_file_path = os.path.join(input_track_path, image_name + ext)
            cv2.imwrite(input_file_path, image)

            boxes = bd.predict(image)
            detected_image = copy.deepcopy(image)

            for i, box in enumerate(boxes):
                x1, y1, x2, y2 = list(map(int, box))
                new_image = image[x1:x2, y1:y2]

                put_rectangle(detected_image, box)
                det_file_path = os.path.join(detector_track_path, image_name + '_' + str(i + 1) + ext)
                cv2.imwrite(det_file_path, detected_image)

                crop_file_path = os.path.join(crop_track_path, image_name + '_' +
                                              str(i + 1) + '.bmp')
                cv2.imwrite(crop_file_path, new_image)
