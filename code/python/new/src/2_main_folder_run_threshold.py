import os
import cv2
import shutil
from natsort import natsorted


INPUT_PATH = '/media/Work/Codes/Private/pdf417-decoder/output/images/Method1/2_Crop/18_03_2021'
OUTPUT_PATH = '/media/Work/Codes/Private/pdf417-decoder/output/images/Method1/3_Threshold'


def put_rectangle(img, bbox):
    return cv2.rectangle(img, (bbox[1], bbox[0]),
                         (bbox[3], bbox[2]), (0, 255, 0), 5)


def clean_dir(folder_path):
    if os.path.isdir(folder_path):
        shutil.rmtree(folder_path)
    os.makedirs(folder_path)


date = os.path.split(INPUT_PATH)[1]
hour_list = natsorted(os.listdir(INPUT_PATH))

for hour in hour_list:
    hour_path = os.path.join(INPUT_PATH, hour)
    track_list = natsorted(os.listdir(hour_path))

    for track in track_list:
        track_path = os.path.join(hour_path, track)
        image_list = natsorted(os.listdir(track_path))

        threshold_track_path = os.path.join(OUTPUT_PATH, date, hour, track)

        clean_dir(threshold_track_path)

        for image_file in image_list:
            image_file_path = os.path.join(track_path, image_file)
            print(f'image_file_path: {image_file_path}')
            image = cv2.imread(image_file_path, 0)

            image_name, ext = os.path.splitext(image_file)
            print(image.shape)
            thresh1 = cv2.adaptiveThreshold(image, 255, cv2.ADAPTIVE_THRESH_MEAN_C,
                                            cv2.THRESH_BINARY, 199, 5)

            th_file_path = os.path.join(threshold_track_path, image_name + '.bmp')
            cv2.imwrite(th_file_path, thresh1)
