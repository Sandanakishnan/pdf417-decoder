import os
import cv2
import shutil
import math
import time
from sympy import N, Point, Line
from natsort import natsorted


INPUT_PATH = '/media/Work/Codes/Private/pdf417-decoder/output/images/Method1/4_Canny/18_03_2021'
OUTPUT_PATH = '/media/Work/Codes/Private/pdf417-decoder/output/images/Method1/5_Max_LSD2'


def put_rectangle(img, bbox):
    return cv2.rectangle(img, (bbox[1], bbox[0]),
                         (bbox[3], bbox[2]), (0, 255, 0), 5)


def clean_dir(folder_path):
    if os.path.isdir(folder_path):
        shutil.rmtree(folder_path)
    os.makedirs(folder_path)


date = os.path.split(INPUT_PATH)[1]
hour_list = natsorted(os.listdir(INPUT_PATH))

angle_arr = []
new_angle_arr = []
l1 = Line((0, 0), (0, 10))

for hour in hour_list:
    hour_path = os.path.join(INPUT_PATH, hour)
    track_list = natsorted(os.listdir(hour_path))

    for track in track_list:
        track_path = os.path.join(hour_path, track)
        image_list = natsorted(os.listdir(track_path))

        lsd_track_path = os.path.join(OUTPUT_PATH, date, hour, track)
        clean_dir(lsd_track_path)

        for image_file in image_list:
            image_file_path = os.path.join(track_path, image_file)
            print(f'image_file_path: {image_file_path}')
            image = cv2.imread(image_file_path, 0)
            new_image = cv2.merge((image, image, image))

            image_name, ext = os.path.splitext(image_file)

            start = time.time()
            lsd = cv2.ximgproc.createFastLineDetector()
            dll = lsd.detect(image)
            angless = []
            distance_arr = []
            x0_arr = []
            y0_arr = []
            x1_arr = []
            y1_arr = []

            if dll[0] is not None:
                for line in dll[0]:
                    x0 = int(round(line[0]))
                    y0 = int(round(line[1]))
                    x1 = int(round(line[2]))
                    y1 = int(round(line[3]))

                    x0_arr.append(x0)
                    y0_arr.append(y0)
                    x1_arr.append(x1)
                    y1_arr.append(y1)

                    distance = math.sqrt(((x1 - x0) ** 2) + ((y1 - y0) ** 2))
                    distance_arr.append(distance)

            print(f'Max Distance: {max(distance_arr)}')
            max_distant_idx = distance_arr.index(max(distance_arr))
            print(f'Max Distance ID: {max_distant_idx}')

            max_x0 = x0_arr[max_distant_idx]
            max_y0 = y0_arr[max_distant_idx]
            max_x1 = x1_arr[max_distant_idx]
            max_y1 = y1_arr[max_distant_idx]

            angletan = round(math.degrees(math.atan2((max_y1 - max_y0),
                                                     (max_x1 - max_x0))))

            print(f'Angle1: {angletan}')
            angle_arr.append(angletan)

            lap1 = time.time()
            print(f'Lap1: {lap1 - start}')

            l2 = Line((max_x0, max_y0), (max_x1, max_y1))
            angle = l1.smallest_angle_between(l2)
            angle = round(N(angle), 3)

            print(f'Angle2: {angle}')
            lap2 = time.time()

            print(f'Lap2: {lap2 - lap1}')

            new_angle_arr.append(angle)

            cv2.line(new_image, (max_x0, max_y0), (max_x1, max_y1), 255, 5, cv2.LINE_AA)
            cv2.circle(new_image, (max_x0, max_y0), 10, (0, 0, 255), -1)
            cv2.circle(new_image, (max_x1, max_y1), 10, (0, 0, 255), -1)

            lsd_file_path = os.path.join(lsd_track_path, image_name + ext)
            cv2.imwrite(lsd_file_path, new_image)

print(f'Angles list1: {set(angle_arr)}')
print(f'Angles list2: {set(new_angle_arr)}')