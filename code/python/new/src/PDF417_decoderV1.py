import time
import numpy as np
from binarize import binarisation
from barcode_linefeaturesV1 import GetBarcodeFeatures
from decoderV1 import PDF417Decoder
import re


class Custom_PDF417:
    def __init__(self, search_thresholds=(0.15, 0.2, 0.25, 0.3, 0.35), pixel_var_threshold=6, line_prof_cnt=3):
        self.Decoder = PDF417Decoder()
        self.regex = re.compile('[@_!#$%^&*()<>?/\}{~:.|=+,-]')
        self.search_threshold_arr = search_thresholds
        self.pixel_var_threshold = pixel_var_threshold
        self.req_line_cnt = line_prof_cnt

    def decode(self, img, debug=False):
        result_data = dict(code='failed')
        Decoder = self.Decoder
        decoded_data = ''
        start = time.time()

        # line_profile, _ = GetBarcodeFeatures.extract_rows_dyn_thres(img,
        #                                                             threshold_arr=self.search_threshold_arr,
        #                                                             req_line_cnt=self.req_line_cnt,
        #                                                             var_threshold=self.pixel_var_threshold)
        line_profile, _ = GetBarcodeFeatures.extract_rows_static_thres(img,
                                                                       threshold=0.25,
                                                                       req_line_cnt=3,
                                                                       var_threshold=6)
        int_prof = np.asarray(line_profile)
        print('Lineprofile time : ', time.time() - start)
        start = time.time()

        if len(int_prof):
            try:
                # With global Threshold
                start = time.time()
                binaryimg = binarisation(np.asarray(int_prof), thresh_iter=0)
                print('Binarisation time : ', time.time() - start)

                start = time.time()
                codewords = Decoder.codeword_extraction(binaryimg[0], line_prof_cnt=3)
                print('Codeword_Extraction time : ', time.time() - start)

                start = time.time()
                decoded_data = str(Decoder.decode_codeword(codewords))
                print('Decoder time : ', time.time() - start)

                print(f'decoded_data: {decoded_data}')
                if decoded_data is None:
                    return result_data
                elif decoded_data == '':
                    return result_data
                elif bool(len(decoded_data)) and self.regex.search(decoded_data) == None:
                    result_data = dict(code=decoded_data)
            # else:
            # debug_image = Features.debug_image
            # return dict(image=debug_image)

            except:
                print('Excepted in PDF417 decoder')
        return result_data