import os
import cv2
import shutil

input_path = '/media/Work/Codes/Private/pdf417-decoder/input/images/new_data/rotated_crops/'
output_path = '/media/Work/Codes/Private/pdf417-decoder/input/images/new_data/rotated_nd_brightness_aug_crops/'


def clean_dir(folder_path):
    if os.path.isdir(folder_path):
        shutil.rmtree(folder_path)
    os.makedirs(folder_path)


clean_dir(output_path)
images = os.listdir(input_path)
bright_level = [0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1,
                1.2, 1.4, 1.6, 1.8, 2.0, 2.2, 2.4, 2.6, 2.8, 3.0]

for j in range(len(images)):
    img = cv2.imread(input_path + images[j], 0)
    cnt = 1
    for i in bright_level:
        img_aug = cv2.multiply(img, i)
        cv2.imwrite(output_path + images[j][:-4] + '_B' + str(i) + '.jpg', img_aug)
        # cv2.imwrite(output_path + images[j][:-4] + '_B' + str(cnt) + '.jpg', img_aug)
        cnt += 1
