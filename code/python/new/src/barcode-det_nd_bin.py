import ntpath
import os
import cv2
from barcode_detector import BarcodeDetector
import copy
import numpy as np
import time

# LD_LIBRARY_PATH=/usr/local/cuda/lib64:/usr/local/cuda/extras/CUPTI/lib64:
from function_classV15 import ExtractBarcodeFeatures

MODEL_PATH = '/media/Work/Code/Public_Repo/barcode/new/src/Color_model/Inference_Graph/frozen_inference_graph.pb'
INPUT_PATH = '/media/Work/Materials/Private_Repo/barcode_reader/PDF417_DB/PDF417'
DETECTOR_OUTPUT_PATH = '/media/Work/Materials/Private_Repo/barcode_reader/PDF417_DB/Detection_Output-new'
CROP_PATH = '/media/Work/Materials/Private_Repo/barcode_reader/PDF417_DB/Crop_Output-new'
BIN_OUTPUT_PATH = '/media/Work/Materials/Private_Repo/barcode_reader/PDF417_DB/Bin_Output-new'

bd = BarcodeDetector(MODEL_PATH, 'COLOR')

input_sub_path = os.listdir(INPUT_PATH)
cnt1 = 0
cnt2 = 0


def put_rectangle(img, bbox):
    return cv2.rectangle(img, (bbox[1], bbox[0]),
                         (bbox[3], bbox[2]), (0, 255, 0), 5)

start = time.time()
for i in range(len(input_sub_path)):
    input_path = os.path.join(INPUT_PATH, input_sub_path[i])
    input_files = os.listdir(input_path)

    det_output_path = os.path.join(DETECTOR_OUTPUT_PATH, input_sub_path[i])

    if not os.path.exists(det_output_path):
        os.makedirs(det_output_path)

    bin_output_path = os.path.join(BIN_OUTPUT_PATH, input_sub_path[i])

    if not os.path.exists(bin_output_path):
        os.makedirs(bin_output_path)

    crop_output_path = os.path.join(CROP_PATH, input_sub_path[i])

    if not os.path.exists(crop_output_path):
        os.makedirs(crop_output_path)

    input_files = sorted(input_files)
    for j in range(len(input_files)):
        image = cv2.imread(os.path.join(input_path, input_files[j]))
        boxes = bd.predict(image)
        viz_image = copy.deepcopy(image)
        for box in boxes:
            put_rectangle(viz_image, box)

        op1_img_name = os.path.join(det_output_path, input_files[j])
        cv2.imwrite(op1_img_name, viz_image)
        for k, bbox in enumerate(boxes):
            bbox = boxes[k]
            cnt1 += 1

            x1, y1, x2, y2 = list(map(int, bbox))

            print(input_files[j])
            print('j : ', j, 'bbox : ', bbox)
            new_image = image[x1:x2, y1:y2, :]

            file_name, ext = ntpath.splitext(input_files[j])

            op2_img_name = os.path.join(crop_output_path, file_name+str(k)+ext)
            op3_img_name = os.path.join(bin_output_path, file_name+str(k)+ext)
            cv2.imwrite(op2_img_name, new_image)
            try:
                binary = ExtractBarcodeFeatures(new_image)
                debug_image = binary.debug_image

                cv2.imwrite(op3_img_name, debug_image)
            except:
                cnt2+=1

end = start - time.time()

print('Count1 : ', cnt1)
print('Count2 : ', cnt2)
