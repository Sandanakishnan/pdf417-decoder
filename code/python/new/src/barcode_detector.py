import os
import cv2
import numpy as np
from vap.config import TFModelConfig
from vap.models import TFModel


class BarcodeDetector(TFModel):
    """ PSE Net OCR Text detector """
    def __init__(self, model_path, image_type='GRAY'):
        tfconfig = TFModelConfig(
            model_path=model_path,
            gpu_allow_growth=False,
            gpu_memory_fraction=0.333,
            cuda_visible_device_list='0',
            input_tensors=["import/image_tensor:0"],
            output_tensors=['import/detection_boxes:0', 'import/detection_scores:0',
                            'import/detection_classes:0', 'import/num_detections:0']
        )
        super(BarcodeDetector, self).__init__(config=tfconfig)
        self.image_type = image_type

    def predict(self, image, score_threshold=0.5):

        if self.image_type == 'GRAY':
            h, w = image.shape
            im_resized = np.array(image).reshape(h, w, 1)
            image_expanded = np.expand_dims(im_resized, axis=0)

        elif self.image_type == 'COLOR':
            h, w, _ = image.shape
            image_expanded = np.expand_dims(image, axis=0)

        (boxes, scores, classes, num_detections) = super().predict(image_expanded)

        to_keep = scores[0] > score_threshold
        boxes = boxes[0][to_keep]

        scaler = np.array([h, w, h, w], dtype='float32')
        boxes = (boxes * scaler).astype('int32')

        return boxes

if __name__ == '__main__':

    def put_rectangle(img, bbox):
        return cv2.rectangle(img, (bbox[1], bbox[0]),
                             (bbox[3], bbox[2]), (0, 255, 0), 5)

    model_path = '/media/Work/Code/Public_Repo/barcode/new/src/' \
                 'Graymodel_4000/Inference_Graph/frozen_inference_graph.pb'
    INPUT = '/media/Work/Code/Private_Repo/torrid-barcode-service/output_data/Color/img_00/img_00'
    OUTPUT = '/media/Work/Code/Public_Repo/barcode/new/src/Graymodel_4000/Output'

    image_names = os.listdir(INPUT)
    bcode_detector = BarcodeDetector(model_path=model_path, image_type='GRAY')
    for image_name in image_names:
        image_file = os.path.join(INPUT, image_name)
        image = cv2.imread(image_file, 0)
        bboxes = bcode_detector.predict(image, score_threshold=0.6)
        OUTPUT_FILE = os.path.join(OUTPUT, image_name)

        for box in bboxes:
            put_rectangle(image, box)

        cv2.imwrite(OUTPUT_FILE, image)
        # input_image = cv2.resize(image, (800, 800))
        # cv2.imshow('Output', input_image)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()




