import os
import numpy as np
import cv2
import copy
import math as mt
from math import sin, cos, radians, pi
from scipy.signal import find_peaks
from sympy import Point, Line, Symbol, pi
from skimage import draw
import matplotlib.pyplot as plt
import time
import math
from statistics import median, mode
import pandas as pd
from natsort import natsorted


class GetBarcodeFeatures:
    @staticmethod
    def convert_image(var_image):
        d = np.ndim(var_image)

        if d == 3:
            var_gray_img = cv2.cvtColor(var_image, cv2.COLOR_BGR2GRAY)
        else:
            var_gray_img = var_image

        return var_gray_img

    @staticmethod
    def all_corners(var_gray_img, threshold=0.3):

        corners = cv2.goodFeaturesToTrack(var_gray_img, 1000, threshold, 10)
        var_all_corners = np.array(tuple(map(np.ravel, corners)), dtype=np.int0)

        return var_all_corners

    @staticmethod
    def image_corners(var_gray_img):
        '''Find corner indices of the Image'''

        row, col = var_gray_img.shape
        img_area = row * col
        var_image_corners = [[0, 0],
                             [col - 1, 0],
                             [col - 1, row - 1],
                             [0, row - 1]]

        return var_image_corners

    @staticmethod
    def min_dist(point, points_array):
        '''Find the min distance between reference points with the points'''

        x1, y1 = point
        dist_arr = []
        for i in range(len(points_array)):
            x2, y2 = points_array[i]
            d = mt.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
            dist_arr.append(d)

        return min(dist_arr), np.argmin(dist_arr)

    @staticmethod
    def distances(var_all_corners, var_image_corners):
        '''Find the distances of corners of the Barcode'''

        var_distances = [GetBarcodeFeatures.min_dist(point, var_all_corners) for point in var_image_corners]

        return var_distances

    @staticmethod
    def order_points(points):
        rect = np.zeros((4, 2), dtype="float32")

        s = points.sum(axis=1)
        rect[0] = points[np.argmin(s)]
        rect[2] = points[np.argmax(s)]

        diff = np.diff(points, axis=1)
        rect[1] = points[np.argmin(diff)]
        rect[3] = points[np.argmax(diff)]

        return rect

    @staticmethod
    def barcode_corners(var_all_corners, var_distances):
        '''Get Barcode corner co-ordinates'''

        var_barcode_corners = GetBarcodeFeatures.order_points(np.array([var_all_corners[
                                                         var_distances[i][1]]
                                                     for i in range(4)]))
        return var_barcode_corners

    @staticmethod
    def find_top_border_angle(var_barcode_corners):
        '''Find Angle of Top border'''

        P1 = var_barcode_corners[0][:]
        P2 = var_barcode_corners[1][:]

        top_line_angle = 90 - round(math.degrees(math.atan2((round(P1[0], 2) -
                                                             round(P2[0], 2)),
                                                            (round(P1[1], 2) -
                                                             round(P2[1], 2)))))

        return top_line_angle

    @staticmethod
    def point_angles(var_barcode_corners):
        '''Get Barcode corner co-ordinates'''

        points = list(map(Point, var_barcode_corners))

        var_angle_arr = []
        for i in range(len(points)):
            l1 = Line(points[i], points[i - 1])
            try:
                l2 = Line(points[i], points[i + 1])
            except:
                l2 = Line(points[i], points[-len(points)])

            angle = l1.angle_between(l2)
            var_angle_arr.append(float(angle * (180 / pi)))

        return var_angle_arr

    @staticmethod
    def associated_pts(var_angle_arr):

        var_associat_pts = []
        angle_variance = []

        for i in range(len(var_angle_arr)):
            if i:
                angles = [var_angle_arr[i], var_angle_arr[i - 1]]
                var_associat_pts.append([i, angles])
            else:
                angles = [var_angle_arr[i], var_angle_arr[-1]]
                var_associat_pts.append([i, angles])
            angle_variance.append([i, np.var(angles)])

        return var_associat_pts, angle_variance

    @staticmethod
    def vert_dist(var_barcode_corners, var_all_corners):

        p1, p2 = var_barcode_corners[:2]
        ppcular_dist = []
        for i in range(len(var_all_corners)):
            p3 = var_all_corners[i]
            d = abs(np.cross(p2 - p1, p3 - p1) / np.linalg.norm(p2 - p1))
            ppcular_dist.append(d)
        var_vert_dist = np.array(ppcular_dist)

        return var_vert_dist

    @staticmethod
    def sorted_distances(var_vert_dist):
        var_sorted_dist = np.sort(var_vert_dist)

        return var_sorted_dist

    @staticmethod
    def distance_gradients(var_sorted_dist):
        var_dist_grad = np.gradient(var_sorted_dist)

        return var_dist_grad

    @staticmethod
    def highest_peak(distance_gradients):
        var_highest_peak = np.max(distance_gradients)

        return var_highest_peak

    @staticmethod
    def get_peaks(var_dist_grad, var_sorted_dist, var_highest_peak, height=0.5):

        peak_indices, peaks = find_peaks(var_dist_grad, height=height * var_highest_peak)
        peak_indices = np.append(peak_indices, len(var_sorted_dist))

        var_peak_indices, var_peaks = peak_indices, peaks['peak_heights']

        return var_peak_indices, var_peaks

    @staticmethod
    def search_threshold(converted_image, in_threshold_arr=[0.25, 0.3, 0.35]):
        variance_data_arr = []
        peaks_data_arr = []
        out_threshold_arr = []

        present_flag = False

        for threshold in in_threshold_arr:

            try:
                # print(threshold_arr)
                print(threshold)
                var_all_corners = GetBarcodeFeatures.all_corners(converted_image, threshold=threshold)
                var_image_corners = GetBarcodeFeatures.image_corners(converted_image)
                var_distances = GetBarcodeFeatures.distances(var_all_corners, var_image_corners)
                var_barcode_corners = GetBarcodeFeatures.barcode_corners(var_all_corners, var_distances)
                top_line_angle = GetBarcodeFeatures.find_top_border_angle(var_barcode_corners)
                var_angle_arr = GetBarcodeFeatures.point_angles(var_barcode_corners)
                var_associat_pts, angle_variance = GetBarcodeFeatures.associated_pts(var_angle_arr)
                var_vert_dist = GetBarcodeFeatures.vert_dist(var_barcode_corners, var_all_corners)

                var_sorted_dist = GetBarcodeFeatures.sorted_distances(var_vert_dist)
                var_dist_grad = GetBarcodeFeatures.distance_gradients(var_sorted_dist)
                var_highest_peak = GetBarcodeFeatures.highest_peak(var_dist_grad)

                peak_indices, peaks = GetBarcodeFeatures.get_peaks(var_dist_grad,
                                                                   var_sorted_dist,
                                                                   var_highest_peak,
                                                                   height=0.9)
                variance_data = [np.var(peaks), np.var(np.gradient(peak_indices[:-1]))]
                print(f'threshold: {threshold}')
                print(f'peaks: {peak_indices}')
                print(f'len(peaks): {len(peaks)}')
                print(f'peaks_diff: {np.diff(np.array(sorted(peaks)))}')
                peaks_data_arr.append(peaks)
                variance_data_arr.append(variance_data)
                out_threshold_arr.append(threshold)
                present_flag = True

            except:
                pass

        if present_flag:
            variance_data_arr = np.transpose(variance_data_arr)
            peaks_data_arr = peaks_data_arr

        else:
            variance_data_arr = np.array([])
            peaks_data_arr = np.array([])

        return out_threshold_arr, peaks_data_arr, variance_data_arr

    @staticmethod
    def optim_threshold(threshold_arr, peaks_data, variance_data):
        var_optim_threshold = None
        save_var = {'variance': variance_data, 'peaks': peaks_data}

        good_heights = []
        if len(peaks_data):
            print(f'[max(peak) for peak in peaks_data]: {[max(peak) for peak in peaks_data]}')
            max_peak_list = [max(peak) for peak in peaks_data]

            sizes = []
            for max_peak, peaks_list in zip(max_peak_list, peaks_data):
                cap = 0.7 * max_peak

                print(f'max_peak: {max_peak}')
                print(f'peaks_list: {peaks_list}')
                mask = peaks_list >= cap
                size = len(peaks_list)
                sizes.append(size)

                print(f'mask: {mask}')
                print(f'size: {size}')

                good_height = np.count_nonzero(mask) == size
                print(f'good_height: {good_height}')

                good_heights.append(good_height)

            order = variance_data.argsort()
            ranks = order.argsort()
            total_variance = np.var(ranks, axis=0)
            weighted_ranks = np.sum(ranks, axis=0)
            cum_ranks = np.sum([total_variance, weighted_ranks], axis=0)
            print(f'variance_data: {variance_data}')
            print(f'total_variance: {total_variance}')
            print(f'cum_ranks: {cum_ranks}')

            index = np.argmin(cum_ranks)

            print(f'good_heights: {good_heights}')
            print(f'index: {index}')

            try:
                indices = [i for i, j in enumerate(sizes) if j == mode(sizes)]
            #       index = sizes.index(max(sizes))
                print(f'indices mode: {indices}')
            except:
                indices = [i for i, j in enumerate(sizes) if j == max(sizes)]

            good_indices = [index for index in indices if good_heights[index]]

            if len(good_indices):
                index = int(median(good_indices))
                var_optim_threshold = threshold_arr[index]

        print(f'var_optim_threshold: {var_optim_threshold}')
        return var_optim_threshold

    @staticmethod
    def class_centers(var_peak_indices, var_sorted_dist):
        '''To calculate centroids by averaging distance measures between peaks'''

        centroids = []
        for i in range(len(var_peak_indices)):
            if i == 0:
                min_idx = 0
                max_idx = var_peak_indices[i]
            elif i == len(var_peak_indices) - 1:
                min_idx = var_peak_indices[i - 1]
                max_idx = len(var_sorted_dist)
            else:
                min_idx = var_peak_indices[i - 1]
                max_idx = var_peak_indices[i]

            centroids.append(np.mean(var_sorted_dist[min_idx: max_idx]))

        return centroids

    @staticmethod
    def labels(var_vert_dist, var_sorted_dist, var_peak_indices):
        '''To get the labels of the co-ordinates with centroids as reference'''

        labels = copy.deepcopy(var_vert_dist)
        for i in range(len(var_peak_indices)):
            if i == 0:
                min_idx = 0
                max_idx = var_peak_indices[i]
            elif i == len(var_peak_indices) - 1:
                min_idx = var_peak_indices[i - 1]
                max_idx = len(var_sorted_dist) - 1
            else:
                min_idx = var_peak_indices[i - 1]
                max_idx = var_peak_indices[i]

            if i != len(var_peak_indices) - 1:
                labels[np.where((var_vert_dist < var_sorted_dist[max_idx]) &
                                (var_vert_dist >= var_sorted_dist[min_idx]))] = i
            else:
                labels[np.where((var_vert_dist <= var_sorted_dist[max_idx]) &
                                (var_vert_dist >= var_sorted_dist[min_idx]))] = i

        var_labels = labels.astype(int)

        return var_labels

    @staticmethod
    def center_coordinates(var_labels, var_all_corners):
        '''Find the centroid of the coordinates'''

        var_center_coord = []
        concur_feat_sep = []
        coords = var_all_corners

        for i in range(len(np.unique(var_labels))):
            selected_bin = coords[np.where(var_labels == i)]
            centroid = np.mean(selected_bin, axis=0)
            var_center_coord.append(centroid)
            concur_feat_sep.append(centroid.astype('int'))

        return var_center_coord, concur_feat_sep

    @staticmethod
    def concurrent_feature_sep(var_center_coord, var_barcode_corners):

        start = time.time()
        (p1, p2) = var_barcode_corners[:2]
        top_border = Line(Point(p1), Point(p2))
        mid_point = Point(p1).midpoint(Point(p2))
        ppcular_line = top_border.perpendicular_line(mid_point)
        tap1 = time.time()
        print(f'Time1: {tap1 - start}')
        a, b, c = ppcular_line.coefficients

        collect_feature_sep = []
        for centroid in var_center_coord:
            parallel_line = top_border.parallel_line(centroid)
            feature_sep = ppcular_line.intersection(parallel_line)
            collect_feature_sep.append(feature_sep[0])

        var_concur_feat_sep = collect_feature_sep
        print(f'Time2: {time.time() - tap1}')

        return var_concur_feat_sep

    @staticmethod
    def get_coord_dist(var_concur_feat_sep):
        dist_arr = []
        for p1, p2 in zip(var_concur_feat_sep, var_concur_feat_sep[1:]):
            dist_arr.append(p1.distance(p2))

        return dist_arr

    @staticmethod
    def overlay_corners(image, corners, radius, color, inplace=False):
        if not inplace:
            overlaid_image = copy.deepcopy(image)
        else:
            overlaid_image = image

        corners = tuple(map(tuple, corners))
        for corner in corners:
            cv2.circle(overlaid_image, corner, radius, color, -1)
        return overlaid_image

    @staticmethod
    def overlay_lines(image, start, end, line_thickness, line_color, inplace=False):
        if not inplace:
            overlaid_image = copy.deepcopy(image)
        else:
            overlaid_image = image

        start, end = tuple(map(tuple, start)), tuple(map(tuple, end))
        for start_line, end_line in zip(start, end):
            cv2.line(overlaid_image, start_line, end_line, line_color, line_thickness)

        return overlaid_image

    @staticmethod
    def line_count(int_var_center_coord, req_line_cnt):

        line_count = req_line_cnt
        (p1, p2) = int_var_center_coord[:2]
        (x, y), (s, t) = p1, p2
        d8 = max(abs(x - s), abs(y - t))
        #     print(f'd8: {d8}')

        if line_count > (d8 / 2.2):
            line_count = int(d8 / 2.2)
        else:
            line_count += 1

        #     print(f'line_count: {line_count}')
        var_norm_distance = 1 / line_count
        var_line_count = line_count - 1

        return var_line_count, var_norm_distance

    @staticmethod
    def line_prof(var_image_corners, var_center_coord, top_line_angle):
        line_prof_coor_arr = []
        y_borderpoints = var_image_corners[-1]
        x_borderpoints = var_image_corners[1]

        print(f'y_borderpoints: {y_borderpoints}')
        print(f'x_borderpoints: {x_borderpoints}')

        max_line_length = 20000

        x_angle = math.cos(top_line_angle * pi / 180.0)
        y_angle = math.sin(top_line_angle * pi / 180.0)

        print(f'x_angle: {x_angle}')
        print(f'y_angle: {y_angle}')

        for i in range(len(var_center_coord)):
            P1x, P1y = var_center_coord[i][0], var_center_coord[i][1]
            P1x = int(P1x)
            P1y = int(P1y)

            for j in range(max_line_length):
                P2x = int(round(P1x - j * x_angle))
                P2y = int(round(P1y - j * y_angle))

                if P2x in x_borderpoints or P2y in y_borderpoints:
                    break

            for j in range(max_line_length):
                P3x = int(round(P1x + j * x_angle))
                P3y = int(round(P1y + j * y_angle))

                if P3x in x_borderpoints or P3y in y_borderpoints:
                    break

                line_prof_coor_arr.append([(P3x, P3y), (P2x, P2y)])
        line_prof_coor_np_arr = np.asarray(line_prof_coor_arr)

        return line_prof_coor_np_arr

    @staticmethod
    def row_extraction(var_gray_img, var_image_corners, top_line_angle, var_center_coord, req_line_cnt=3):

        # start_t = time.time()
        Int_prof_points = []
        req_line_cnt = req_line_cnt / 2
        y_borderpoints = var_image_corners[-1]
        x_borderpoints = var_image_corners[1]
        max_line_length = 20000
        x_angle = math.cos(top_line_angle * pi / 180.0)
        y_angle = math.sin(top_line_angle * pi / 180.0)
        int_prof = []
        dup_check = []

        for i in range(len(var_center_coord) - 1):

            start = int(var_center_coord[i][1])
            end = int(var_center_coord[i + 1][1])

            length = (end - start) / 2
            need_row = np.arange(length - math.floor(req_line_cnt), length + math.ceil(req_line_cnt))
            data_arr = []

            for k in need_row:
                P1x, P1y = var_center_coord[i][0], var_center_coord[i][1] + k
                P1x = int(P1x)
                P1y = int(P1y)
                right_line_prof = []
                for j in range(max_line_length):
                    P2x = int(round(P1x - j * x_angle))
                    P2y = int(round(P1y - j * y_angle))
                    if P2x in x_borderpoints or P2y in y_borderpoints:
                        break
                    if len(right_line_prof) >= 1:
                        if [P2y, P2x] != dup_check[-1]:
                            right_line_prof.append(var_gray_img[P2y, P2x])
                    else:
                        right_line_prof.append(var_gray_img[P2y, P2x])
                    dup_check = [[P2y, P2x]]

                left_line_prof = []
                for j in range(max_line_length):
                    P3x = int(round(P1x + j * x_angle))
                    P3y = int(round(P1y + j * y_angle))
                    if P3x in x_borderpoints or P3y in y_borderpoints:
                        break
                    if len(left_line_prof) >= 1:
                        if [P3y, P3x] != dup_check[-1]:
                            left_line_prof.append(var_gray_img[P3y, P3x])
                    else:
                        left_line_prof.append(var_gray_img[P3y, P3x])
                    dup_check = [[P3y, P3x]]
                Int_prof_points.append([(P3x, P3y), (P2x, P2y)])
                left_line_prof.reverse()
                data = left_line_prof + right_line_prof
                data_arr.append(data)

            int_prof.extend(data_arr)

        # print('Int_profile_time : ',time.time()-start_t)
        Int_prof_points = np.asarray(Int_prof_points)

        return int_prof, Int_prof_points

    @staticmethod
    def debug_image(var_image,
                    var_all_corners,
                    var_barcode_corners,
                    concur_feat_sep,
                    Int_prof_points,
                    line_prof_coor_arr):

        debug_img = copy.deepcopy(var_image)

        if not debug_img.ndim == 3:
            debug_img = np.stack((debug_img,) * 3, axis=-1)

        pt1_thickness, pt1_color = 4, (255, 0, 0)
        pt2_thickness, pt2_color = 7, (0, 0, 255)
        pt3_thickness, pt3_color = 7, (0, 0, 255)
        l1_thickness, l1_color = 1, (0, 0, 255)
        l2_thickness, l2_color = 1, (0, 255, 0)

        left_coord_arr = Int_prof_points[:, 0]
        right_coord_arr = Int_prof_points[:, 1]
        debug_img = GetBarcodeFeatures.overlay_corners(debug_img, var_all_corners, pt1_thickness, pt1_color)
        debug_img = GetBarcodeFeatures.overlay_corners(debug_img, var_barcode_corners, pt2_thickness, pt2_color)
        debug_img = GetBarcodeFeatures.overlay_corners(debug_img, concur_feat_sep, pt3_thickness, pt3_color)
        debug_img = GetBarcodeFeatures.overlay_lines(debug_img, line_prof_coor_arr[:, 0], line_prof_coor_arr[:, 1],
                                  l1_thickness, l1_color)
        debug_img = GetBarcodeFeatures.overlay_lines(debug_img, left_coord_arr, right_coord_arr, l2_thickness, l2_color)

        return debug_img

    @staticmethod
    def plot_int_profile(var_int_prof_arr):

        fig_arr = []
        for i in range(len(var_int_prof_arr)):
            fig, ax = plt.subplots()
            for j in range(len(var_int_prof_arr[i])):
                elem_data = var_int_prof_arr[i][j]
                ax.plot(range(elem_data.shape[0]), elem_data, label='Line ' + str(j + 1))
            ax.legend()
            fig_arr.append(fig)

        return fig_arr

    @staticmethod
    def plot_gradient(var_dist_grad):
        '''Visualization of height of each step'''

        x = np.arange(len(var_dist_grad))
        y = var_dist_grad
        fig = plt.figure()
        ax = plt.subplot(111)
        ax.plot(x, y)
        plt.title('Peaks Graph')

        return fig

    @staticmethod
    def extract_rows_static_thres(image, threshold, req_line_cnt, var_threshold=5):
        var_gray_img = GetBarcodeFeatures.convert_image(image)
        var_all_corners = GetBarcodeFeatures.all_corners(var_gray_img, threshold=threshold)
        var_image_corners = GetBarcodeFeatures.image_corners(var_gray_img)
        var_distances = GetBarcodeFeatures.distances(var_all_corners, var_image_corners)
        var_barcode_corners = GetBarcodeFeatures.barcode_corners(var_all_corners, var_distances)
        top_line_angle = GetBarcodeFeatures.find_top_border_angle(var_barcode_corners)
        # var_angle_arr = GetBarcodeFeatures.point_angles(var_barcode_corners)
        # var_associat_pts, angle_variance = GetBarcodeFeatures.associated_pts(var_angle_arr)
        var_vert_dist = GetBarcodeFeatures.vert_dist(var_barcode_corners, var_all_corners)
        print(1)

        var_sorted_dist = GetBarcodeFeatures.sorted_distances(var_vert_dist)
        var_dist_grad = GetBarcodeFeatures.distance_gradients(var_sorted_dist)
        var_highest_peak = GetBarcodeFeatures.highest_peak(var_dist_grad)

        var_peak_indices, var_peaks = GetBarcodeFeatures.get_peaks(var_dist_grad,
                                                                   var_sorted_dist,
                                                                   var_highest_peak,
                                                                   height=0.9)
        centroids = GetBarcodeFeatures.class_centers(var_peak_indices, var_sorted_dist)
        var_labels = GetBarcodeFeatures.labels(var_vert_dist, var_sorted_dist, var_peak_indices)
        var_center_coord, int_var_center_coord = GetBarcodeFeatures.center_coordinates(var_labels, var_all_corners)
        var_concur_feat_sep = GetBarcodeFeatures.concurrent_feature_sep(var_center_coord, var_barcode_corners)
        dist_arr = GetBarcodeFeatures.get_coord_dist(var_concur_feat_sep)
        pixel_diffs = list(map(abs, np.diff(np.array(list(map(int, dist_arr))[1:]))))
        print(f'pixel_diffs: {pixel_diffs}')
        if len(pixel_diffs):
            if all(np.array(pixel_diffs) <= var_threshold):
                # var_line_count, var_norm_distance = GetBarcodeFeatures.line_count(int_var_center_coord, req_line_cnt)
                # line_prof_coor_arr = GetBarcodeFeatures.line_prof(var_image_corners, var_center_coord, top_line_angle)
                int_prof, Int_prof_points = GetBarcodeFeatures.row_extraction(var_gray_img,
                                                           var_image_corners,
                                                           top_line_angle,
                                                           var_center_coord, req_line_cnt)
                return int_prof, Int_prof_points
            else:
                return [], []
        else:
            return [], []
        int_prof, Int_prof_points = GetBarcodeFeatures.row_extraction(var_gray_img,
                                                                      var_image_corners,
                                                                      top_line_angle,
                                                                      var_center_coord, req_line_cnt)
        return None

    @staticmethod
    def extract_rows_dyn_thres(image, req_line_cnt,
                               threshold_arr=[0.2, 0.25, 0.3, 0.35],
                               var_threshold=5):
        var_gray_img = GetBarcodeFeatures.convert_image(image)

        threshold_arr, peaks_data_arr, variance_data_arr = GetBarcodeFeatures.search_threshold(var_gray_img, threshold_arr)
        var_optim_threshold = GetBarcodeFeatures.optim_threshold(threshold_arr, peaks_data_arr, variance_data_arr)

        if var_optim_threshold is not None:
            var_all_corners = GetBarcodeFeatures.all_corners(var_gray_img, threshold=var_optim_threshold)
            var_image_corners = GetBarcodeFeatures.image_corners(var_gray_img)
            var_distances = GetBarcodeFeatures.distances(var_all_corners, var_image_corners)
            var_barcode_corners = GetBarcodeFeatures.barcode_corners(var_all_corners, var_distances)
            top_line_angle = GetBarcodeFeatures.find_top_border_angle(var_barcode_corners)
            # var_angle_arr = GetBarcodeFeatures.point_angles(var_barcode_corners)
            # var_associat_pts, angle_variance = GetBarcodeFeatures.associated_pts(var_angle_arr)
            var_vert_dist = GetBarcodeFeatures.vert_dist(var_barcode_corners, var_all_corners)
            print(1)

            var_sorted_dist = GetBarcodeFeatures.sorted_distances(var_vert_dist)
            var_dist_grad = GetBarcodeFeatures.distance_gradients(var_sorted_dist)
            var_highest_peak = GetBarcodeFeatures.highest_peak(var_dist_grad)

            var_peak_indices, var_peaks = GetBarcodeFeatures.get_peaks(var_dist_grad, var_sorted_dist, var_highest_peak,
                                                                       height=0.9)
            centroids = GetBarcodeFeatures.class_centers(var_peak_indices, var_sorted_dist)
            var_labels = GetBarcodeFeatures.labels(var_vert_dist, var_sorted_dist, var_peak_indices)
            var_center_coord, int_var_center_coord = GetBarcodeFeatures.center_coordinates(var_labels, var_all_corners)
            var_concur_feat_sep = GetBarcodeFeatures.concurrent_feature_sep(var_center_coord, var_barcode_corners)
            dist_arr = GetBarcodeFeatures.get_coord_dist(var_concur_feat_sep)
            pixel_diffs = list(map(abs, np.diff(np.array(sorted(list(map(int, dist_arr))[1:])))))
            print(f'pixel_diffs: {pixel_diffs}')
            if len(pixel_diffs):
                if all(np.array(pixel_diffs) <= var_threshold):
                    # var_line_count, var_norm_distance = GetBarcodeFeatures.line_count(int_var_center_coord, req_line_cnt)
                    # line_prof_coor_arr = GetBarcodeFeatures.line_prof(var_image_corners, var_center_coord, top_line_angle)
                    int_prof, Int_prof_points = GetBarcodeFeatures.row_extraction(var_gray_img,
                                                                                  var_image_corners,
                                                                                  top_line_angle,
                                                                                  var_center_coord, req_line_cnt)
                    return int_prof, Int_prof_points
                else:
                    return [], []
            else:
                return [], []
        else:
            return [], []


if __name__ == '__main__':
    INPUT_PATH = '/home/sandana/Documents/good_crops'
    image_files = natsorted(os.listdir(INPUT_PATH))

    for image_file in image_files:
        INPUT_IMAGE_PATH = os.path.join(INPUT_PATH, image_file)
        print(f'INPUT_IMAGE_PATH: {INPUT_IMAGE_PATH}')
        input_image = cv2.imread(INPUT_IMAGE_PATH, 0)
        start = time.time()
        # int_prof, Int_prof_points = GetBarcodeFeatures.extract_rows_static_thres(input_image,
        #                                                                          threshold=0.35,
        #                                                                          req_line_cnt=3,
        #                                                                          var_threshold=5)
        int_prof, Int_prof_points = GetBarcodeFeatures.extract_rows_dyn_thres(input_image,
                                                                              threshold_arr=[0.2, 0.25, 0.3, 0.35],
                                                                              req_line_cnt=3,
                                                                              var_threshold=6)
        print(f'Total Time: {time.time() - start}')
        if len(int_prof):
            print(f'np.array(int_prof).shape: {np.array(int_prof).shape}')
        else:
            print(None)
