import numpy as np
import time
def binarisation(gray,thresh_iter=0):
    '''Converts a Grayscale image to Binary image'''
    local_thresh=[]
    step_size = 3
    for i in range(0,len(gray),step_size):
        for j in range(0,len(gray[0,:]),step_size):
            imblk=gray[i:i+step_size,j:j+step_size]
            local_thresh.append(imblk.mean())
    
    global_thresh=(sum(local_thresh)/len(local_thresh))
    if thresh_iter == 0:
        ft_thresholds = [global_thresh]
    else:
        ft_thresholds = np.arange(global_thresh - thresh_iter,global_thresh + thresh_iter)
    binaryimg = []
    for ft in range(len(ft_thresholds)):
        binary=np.zeros(gray.shape)+255
        binary[np.where(gray<ft_thresholds[ft])]=0
        binaryimg.append(binary)
    return binaryimg