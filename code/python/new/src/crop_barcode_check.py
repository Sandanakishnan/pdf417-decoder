import os
import cv2
import shutil
import copy
import time
import math as mt
import numpy as np

input_path = '/media/Work/Codes/Private/pdf417-decoder/input/images/new_data/rotated_nd_brightness_aug_crops/'
output_path = '/media/Work/Codes/Private/pdf417-decoder/input/images/new_data/barcode_crops/'


def clean_dir(folder_path):
    if os.path.isdir(folder_path):
        shutil.rmtree(folder_path)
    os.makedirs(folder_path)


class CropBarcode:
    @staticmethod
    def convert_image(var_image):
        d = np.ndim(var_image)

        if d == 3:
            var_gray_img = cv2.cvtColor(var_image, cv2.COLOR_BGR2GRAY)
        else:
            var_gray_img = var_image

        return var_gray_img

    @staticmethod
    def preprocess_image(image):
        thresh1 = cv2.adaptiveThreshold(image, 255, cv2.ADAPTIVE_THRESH_MEAN_C,
                                        cv2.THRESH_BINARY, 199, 5)

        _, contours, hierarchy = cv2.findContours(thresh1, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

        immask = copy.deepcopy(thresh1)
        c_max = max(contours, key=cv2.contourArea)
        cv2.drawContours(immask, [c_max], 0, 255, -1)

        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (200, 200))
        opening = cv2.morphologyEx(immask, cv2.MORPH_OPEN, kernel)

        dest_not = cv2.bitwise_not(opening)
        dest_and = cv2.bitwise_or(thresh1, dest_not, mask=None)

        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
        closing = cv2.morphologyEx(dest_and, cv2.MORPH_CLOSE, kernel)

        kernel = np.ones((5, 5), np.uint8)
        new_mask = cv2.erode(opening, kernel, iterations=6)

        edge_image_closed = cv2.Canny(closing, 100, 200)
        edge_image_closed = cv2.bitwise_and(edge_image_closed, new_mask)

        start = time.time()
        lsd = cv2.createLineSegmentDetector(0)
        dll = lsd.detect(edge_image_closed)
        angless = []
        distance_arr = []

        x0_arr = []
        y0_arr = []
        x1_arr = []
        y1_arr = []

        if dll[0] is not None:
            for line in dll[0]:
                x0 = int(round(line[0][0]))
                y0 = int(round(line[0][1]))
                x1 = int(round(line[0][2]))
                y1 = int(round(line[0][3]))

                x0_arr.append(x0)
                y0_arr.append(y0)
                x1_arr.append(x1)
                y1_arr.append(y1)

                distance = mt.sqrt(((x1 - x0) ** 2) + ((y1 - y0) ** 2))
                distance_arr.append(distance)

        max_distant_idx = distance_arr.index(max(distance_arr))

        max_x0 = x0_arr[max_distant_idx]
        max_y0 = y0_arr[max_distant_idx]
        max_x1 = x1_arr[max_distant_idx]
        max_y1 = y1_arr[max_distant_idx]

        angletan = mt.degrees(mt.atan2((max_y1 - max_y0),
                                       (max_x1 - max_x0)))
        rotation_angle = 90 + angletan
        rows, cols = closing.shape

        img_center = (rows, cols)
        M = cv2.getRotationMatrix2D(img_center, rotation_angle, 1)

        if cols <= rows:
            new_cols, new_rows = 7 * cols, 4 * rows
        else:
            new_cols, new_rows = 3 * cols, 4 * rows

        rotated_image = cv2.warpAffine(closing, M, (new_cols, new_rows), borderValue=(255,))

        start = time.time()
        append_pixels = 300
        append_rows = new_rows + 2 * append_pixels
        append_cols = new_cols + 2 * append_pixels

        appended_image = np.ones((append_rows, append_cols), dtype=np.uint8) * 255

        appended_image[append_pixels:append_rows - append_pixels,
        append_pixels:append_cols - append_pixels] = rotated_image

        kernel = np.ones((1, 30))
        # kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(200, 200))
        opening_test = cv2.morphologyEx(appended_image, cv2.MORPH_OPEN, kernel)

        # Find the contours
        _, contours, hierarchy = cv2.findContours(opening_test, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        # For each contour approximate the curve and
        # detect the shapes.
        sorted_contours = sorted(contours, key=cv2.contourArea, reverse=True)
        # for cnt in sorted_contours:
            # print(cv2.contourArea(cnt))
        barcode_contour = np.array(sorted(contours, key=cv2.contourArea, reverse=True)[1])

        barcode_contour = barcode_contour.reshape(-1, 2)
        # epsilon = 0.01*cv2.arcLength(barcode_contour, True)
        # approx = cv2.approxPolyDP(barcode_contour, epsilon, True)
        # print(approx)

        # approx = np.reshape(np.array(approx), (4, 2))
        # x_arr, y_arr = approx[:, 0], approx[:, 1]

        x_arr, y_arr = barcode_contour[:, 0], barcode_contour[:, 1]

        row_start, col_start = min(y_arr), min(x_arr)
        row_end, col_end = max(y_arr), max(x_arr)

        barcode_crop = appended_image[row_start:row_end, col_start:col_end]

        print(f'Time Duration: {time.time() - start}')
        return barcode_crop


if __name__ == '__main__':
    clean_dir(output_path)
    image_files = os.listdir(input_path)

    for image_file in image_files:
        try:
            image_name = os.path.join(input_path, image_file)
            print(f'Input: {image_name}')

            image = cv2.imread(image_name, 0)

            output_image_name = os.path.join(output_path, image_file)

            barcode_image = CropBarcode.preprocess_image(image)
            cv2.imwrite(output_image_name, barcode_image)
        except Exception:
            pass