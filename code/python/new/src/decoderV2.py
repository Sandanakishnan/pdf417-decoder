import numpy as np
import pandas as pd
import itertools
from PIL import Image
import time
import os, sys, traceback
from Classes_EC.Error_Correction import ErrorCorrection
from math import ceil, floor


class PDF417Decoder():
    def __init__(self):
        self.codeword_list = np.load('PDF417codes/codeword_list.npy')
        self.Textcode = self.Text_codeword('PDF417codes/pdf417_text_code.txt')
        self.latchlist = self.get_latch_shiftlist()

    def Text_codeword(self, filename):
        '''Read Text Codeword from a file'''
        f = open(filename, 'r')
        code = f.read()
        f.close()
        code = code.split('\n')
        Textcode = []
        for j in range(31):
            Textcode.append(code[j].split('\t')[1:])

        Textcode = pd.DataFrame(Textcode[1:], columns=Textcode[0])
        return Textcode

    def add_quiet_zone(self, im):
        # box = (15, 15, im.size[0] + 15, im.size[1] + 15)
        # img = Image.new('L', (im.size[0] + 30, im.size[1] + 30), 'white')
        # img.paste(im, box)
        img =  [255]*5+list(im)+[255]*5
        return img

    def split_module(self, pdf_row):
        '''Group  row-wise binary pixels into classes'''
        pdf_row = self.add_quiet_zone(pdf_row)
        return [(i[0], len(list(i[1]))) for i in itertools.groupby(pdf_row)]

    def filter_quitezone(self, row_data):
        '''Filter Quite Zone from Left and Right of Image'''
        if row_data[0][0] == 255:  row_data = row_data[1:]
        if row_data[-1][0] == 255: row_data = row_data[0:-1]
        return row_data

    def get_latch_shiftlist(self):
        '''Get LAtch Shift List'''
        latchlist = {'ll': ['Lower', 0], 'as': ['Alpha', 1], 'al': ['Alpha', 0], 'ml': ['Mixed', 0],
                     'pl': ['Punctuation', 0], 'ps': ['Punctuation', 1]}
        return latchlist

    def reformat_orig(bin_data, Inp_data):
        '''Group the row-wise Intensity pixels into classes'''
        grouped = []
        start = 0
        for i in range(len(bin_data)):
            cnt = bin_data[i][1]
            grouped.append(Inp_data[start: start + cnt])
            start = start + cnt
        return grouped

    def filter_quitezone_orig(row_data, orig):
        '''Filter Quite Zone from Left and Right of Image'''
        if row_data[0][0] == 255:  orig = orig[1:]
        if row_data[-1][0] == 255: orig = orig[0:-1]
        return orig

    def split_column(self, pdf_row):
        '''Calculate codeword'''
        pdf_col = []
        codeword_list = self.codeword_list
        for i in range(len(pdf_row)):
            modules = self.split_module(pdf_row[i, :])
            modules = self.filter_quitezone(modules)
            if modules[0][1]*1.2 < modules[1][1]:
                self.invertflg += 1
                modules.reverse()
            column = []
            codeword = []
            codewords = []
            for j in range(len(modules)):
                if len(column) < 8:
                    column.append(modules[j][1])
                    codeword.extend([modules[j][0]] * modules[j][1])
                if len(column) == 8:
                    codewordlen = len(codeword)
                    mmv = codewordlen / 17  # Minimum width of module
                    code = [[round(k / mmv), floor(k / mmv), ceil(k / mmv)] for k in column]
                    code = np.asarray(code)
                    code[np.where(code == 0)] = 1
                    tot_width_code = sum(code)
                    correct_code = np.where(tot_width_code == 17)[0]
                    if len(correct_code) <= 0:
                        codestring = '0'
                        codewords.append(codestring)
                    else:
                        codestring = ('').join([str(d) for d in code[:, correct_code[0]]])
                        if codestring in codeword_list:
                            codewords.append(codestring)
                        else:
                            codewords.append('0')
                    codeword = []
                    column = []
            pdf_col.append(codewords)
        return pdf_col

    def clean_codeword(self, wholecodewords):
        '''Clean the Code words'''
        col_len = self.no_of_col
        row_len = self.num_rows
        codeword_list = self.codeword_list
        words = wholecodewords
        i = 0
        PDF_coefs = []
        PDF_code_pattern = []
        for i in range(0, len(words), row_len):
            rows = words[i:i + row_len]
            newrows = [v for v in rows if len(v) == col_len]
            for k in range(2, col_len - 2):
                if len(newrows) > 0:
                    nonzerocol = [v[k] for v in newrows if v[k] != '0']

                    if len(nonzerocol) == 0:
                        PDF_coefs.append(0)
                        PDF_code_pattern.append('0')
                    else:
                        rowno = int(codeword_list[np.where(codeword_list == nonzerocol[0])[0][0], 0])
                        PDF_coefs.append(rowno)
                        PDF_code_pattern.append(nonzerocol[0])

        return PDF_coefs, PDF_code_pattern

    def codeword_extraction(self, binaryimg, line_prof_cnt=3):
        '''Codeword Extraction'''
        wholecodewords = []
        self.invertflg = 0
        self.num_rows = line_prof_cnt
        for i in range(0, len(binaryimg), line_prof_cnt):
            pdf_row = binaryimg[i:i + 3, :]
            pdf_col = self.split_column(pdf_row)
            wholecodewords.extend(pdf_col)

        no_of_col_all = []
        for code in wholecodewords:
            no_of_col_all.append(len(code))
        no_of_col = max(set(no_of_col_all), key=no_of_col_all.count)
        
        padded = []
        
        for code in wholecodewords:
            if len(code)<no_of_col:
                pad=no_of_col-len(code)
                code.extend(['0']*pad)
            padded.append(code)
        
        wholecodewords=padded
        self.no_of_col = no_of_col
        self.wholecodewords = wholecodewords
        PDF_coefs, PDF_code_pattern = self.clean_codeword(wholecodewords)
        if self.invertflg >= 8:
            PDF_coefs.reverse()
            PDF_code_pattern.reverse()
        self.codewords = PDF_coefs
        self.PDF_code_pattern = PDF_code_pattern
        return self.codewords

    def coefs_error_correction(self, coefs):
        EC = ErrorCorrection()
        error_coef_len = len(coefs) - coefs[0]
        try:
            error_corrected_coefs = EC.Testcodewords(coefs.copy(), error_coef_len)
            if error_corrected_coefs == 0:
                return coefs[0:-error_coef_len]
            elif error_corrected_coefs == -1 and len(coefs) < 4:
                print('Not Enough Codewords')
                return None
            elif error_corrected_coefs == -1 and len(coefs) > 4:
                print('Maybe Decode wrongly')
                return coefs[0:-error_coef_len]
            elif len(error_corrected_coefs) == len(coefs):
                return error_corrected_coefs[0:-error_coef_len]
        except Exception as exp:
            tb = sys.exc_info()[-1]
            stk = traceback.extract_tb(tb, 1)
            print(stk)
            print('Error : ', exp)
            return None

    def find_x_y_codewordlen(gray):
        if gray[0, 7] != gray[0, 8]:
            bar_x = 1
            bar_y = 3
            codewordlen = 17
        elif gray[0, 15] != gray[0, 16]:
            bar_x = 2
            bar_y = 6
            codewordlen = 17 * bar_x
        return bar_x, bar_y, codewordlen

    def Texttype_decode(self, coeff, Prev_char=None, Preprev_char=None, Prev_mode=None, Preprev_mode=None):
        '''Decode Text type codes'''
        S_c = coeff % 30
        F_c = (coeff - S_c) / 30
        Textcode = self.Textcode
        latchlist = self.latchlist
        if Prev_char is None and Textcode['Alpha'][F_c] in latchlist:
            first_char = Textcode['Alpha'][F_c]
            textmode = Textcode['Alpha'][F_c]
            mode = latchlist[textmode]
            second_char = Textcode[mode[0]][S_c]
            pm = mode[0]
            ppm = 'Alpha'
        elif Prev_char is None and Textcode['Alpha'][F_c] not in latchlist:
            first_char = Textcode['Alpha'][F_c]
            ppm = 'Alpha'
            if first_char in latchlist:
                mode = latchlist[first_char]
                second_char = Textcode[mode[0]][S_c]
                pm = mode[0]
            else:
                second_char = Textcode['Alpha'][S_c]
                pm = 'Alpha'
        elif Prev_char is not None and Prev_char in latchlist:
            mode = latchlist[Prev_char]
            first_char = Textcode[mode[0]][F_c]
            ppm = mode[0]
            if mode[1] == 0:
                if first_char in latchlist:
                    mode = latchlist[first_char]
                second_char = Textcode[mode[0]][S_c]
                pm = mode[0]
            else:
                second_char = Textcode[Preprev_mode][S_c]
                pm = Preprev_mode
        else:
            if Preprev_char in latchlist:
                if latchlist[Preprev_char][1] == 1:
                    Prev_mode = Preprev_mode
            first_char = Textcode[Prev_mode][F_c]
            ppm = Prev_mode
            if first_char in latchlist:
                mode = latchlist[first_char]
                second_char = Textcode[mode[0]][S_c]
                pm = mode[0]
            else:
                second_char = Textcode[Prev_mode][S_c]
                pm = Prev_mode

        return first_char, second_char, ppm, pm

    def Bytetype_decode(self, codewords):
        '''Decode the Byte type codes'''
        codewordvalue = codewords[0]
        for c in range(1, len(codewords)):
            codewordvalue = (codewordvalue * 900) + codewords[c]
        decoded = []
        decoded_data = ''
        decoded.append(codewordvalue % 256)
        decoded_data = chr(int(decoded[-1])) + decoded_data
        for c in range(5):
            codewordvalue = (codewordvalue - decoded[-1]) / 256
            decoded.append(codewordvalue % 256)
            decoded_data = chr(int(decoded[-1])) + decoded_data
        return decoded_data

    def Numerictype_decode(self, codewords):
        '''Decode the Numeric type decodes'''
        codewordvalue = codewords[0]
        for c in range(1, len(codewords)):
            codewordvalue = (codewordvalue * 900) + codewords[c]
        codewordvalue = str(codewordvalue)[1:]
        return codewordvalue

    def decode_codeword(self, Codewords):
        '''Decode'''
        start = time.time()
        PDF_coefs = self.coefs_error_correction(Codewords.copy())
        print('Error correction time : ', time.time() - start)

        Decoded_data = ''
        try:
            # start_time = time.time()
            if PDF_coefs is not None:
                latchlist = self.latchlist

                Prev_char = None
                Preprev_char = None
                Prev_mode = None
                Preprev_mode = None
                codewords = []
                fin_flg = 0

                if PDF_coefs[1] >= 900:
                    Pdf_mode = PDF_coefs[1]
                    start = 2
                else:
                    Pdf_mode = 900
                    start = 1
                temp_by_flg = 0
                lat_by_flg = 0
                for i in range(start, len(PDF_coefs)):
                    coef = PDF_coefs[i]
                    dat = ''

                    if Pdf_mode == 900 and fin_flg == 0:
                        # print('TextMode Encoding')
                        if coef in [900, 902]:
                            break
                        if temp_by_flg == 1:
                            dat = dat + chr(int(coef))
                            temp_by_flg = 2
                            if Prev_char in latchlist:
                                if latchlist[Prev_char][1] == 1:
                                    Prev_char = Preprev_char
                        if coef == 913:  # temporary shift to byte
                            temp_by_flg = 1

                        if lat_by_flg == 1:
                            dat = dat + chr(int(coef))
                        if coef == 901:
                            lat_by_flg = 1

                        if temp_by_flg == 0 and lat_by_flg == 0:
                            # print('codeword     ',coef)
                            first_char, second_char, Preprev_mode, Prev_mode = self.Texttype_decode(coef, Prev_char,
                                                                                                    Preprev_char,
                                                                                                    Prev_mode,
                                                                                                    Preprev_mode)
                            Prev_char = second_char
                            Preprev_char = first_char

                            if first_char not in latchlist:
                                if first_char == 'space':
                                    dat = dat + ' '
                                else:
                                    dat = dat + first_char

                            if second_char not in latchlist:
                                if second_char == 'space':
                                    dat = dat + ' '
                                else:
                                    dat = dat + second_char
                        if temp_by_flg == 2: temp_by_flg = 0

                    elif (Pdf_mode == 901 or Pdf_mode == 924) and fin_flg == 0:
                        # print('Byte Mode Encoding')
                        if coef in [900, 902]:
                            break
                        if Pdf_mode == 901:  # Its not multiple of 6
                            codewords.append(coef)
                            if len(codewords) % 5 == 0:  # Multiple of 6
                                dat = self.Bytetype_decode(codewords)
                            elif len(codewords) > 5:
                                dat = chr(int(coef))

                        elif Pdf_mode == 924:
                            codewords.append(coef)
                            if len(codewords) % 5 == 0:
                                dat = self.Bytetype_decode(codewords)
                            elif len(codewords) > 5:
                                dat = chr(int(coef))

                    elif Pdf_mode == 902 and fin_flg == 0:
                        # print('Numeric mode Encoding')
                        codewords.append(coef)
                        if coef in [900, 901, 924]:
                            break
                        if (len(codewords) % 15 == 0 and fin_flg == 0) or (len(codewords) < 15 and fin_flg == 1):
                            if fin_flg == 1:
                                dat = self.Numerictype_decode(codewords[0:-1])
                            else:
                                dat = self.Numerictype_decode(codewords)

                    Decoded_data = Decoded_data + dat
            # print('decode time : ',time.time()-start_time)
            return Decoded_data
        except Exception as exp:
            print(exp)
            return Decoded_data
