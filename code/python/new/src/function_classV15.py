import numpy as np
import cv2
import copy
import math as mt
from scipy.signal import find_peaks
from sympy import Point, Line, Symbol, pi
from skimage import draw
import matplotlib.pyplot as plt
import pickle


class ExtractBarcodeFeatures:
    def __init__(self, image):

        self.var_image = image

    def preset_variables(self, ref_dict):
        for key in ref_dict.keys():
            if key not in self.__dict__.keys():
                exec(ref_dict[key])

    def clear_variables(self, ref_list):
        for key in ref_list:
            self.__dict__.pop(key, None)

    @property
    def convert_image(self):
        d = np.ndim(self.var_image)
        if d == 3:
            self.var_gray_img = cv2.cvtColor(self.var_image, cv2.COLOR_BGR2GRAY)
        else:
            self.var_gray_img = self.var_image
        return self.var_gray_img

    def all_corners(self, threshold=0.3):
        ref_dict = {'var_gray_img': 'self.convert_image'}

        self.preset_variables(ref_dict=ref_dict)
        print(self.var_gray_img.shape, threshold)
        corners = cv2.goodFeaturesToTrack(self.var_gray_img, 1000, threshold, 10)
        self.var_all_corners = np.array(tuple(map(np.ravel, corners)), dtype=np.int0)
        return self.var_all_corners

    @property
    def image_corners(self):
        '''Find corner indices of the Image'''
        ref_dict = {'var_gray_img': 'self.convert_image'}
        self.preset_variables(ref_dict=ref_dict)

        row, col = self.var_gray_img.shape
        self.img_area = row * col
        self.var_image_corners = [[0, 0],
                                  [col - 1, 0],
                                  [col - 1, row - 1],
                                  [0, row - 1]]

        self.right_border = Line(self.var_image_corners[1], self.var_image_corners[2])
        self.left_border = Line(self.var_image_corners[0], self.var_image_corners[3])

        return self.var_image_corners

    def min_dist(self, point, points_array):
        '''Find the min distance between reference points with the points'''
        x1, y1 = point
        dist_arr = []
        for i in range(len(points_array)):
            x2, y2 = points_array[i]
            d = mt.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
            dist_arr.append(d)

        # dist_arr = [Point(point).distance(Point(p2)) for p2 in points_array]

        return min(dist_arr), np.argmin(dist_arr)

    @property
    def distances(self):
        '''Find the distances of corners of the Barcode'''
        ref_dict = {'var_all_corners': 'self.all_corners()',
                    'var_image_corners': 'self.image_corners'}
        self.preset_variables(ref_dict=ref_dict)

        self.var_distances = [self.min_dist(point, self.var_all_corners) for point in self.var_image_corners]
        return self.var_distances

    @property
    def barcode_corners(self):
        '''Get Barcode corner co-ordinates'''
        ref_dict = {'var_all_corners': 'self.all_corners()',
                    'var_distances': 'self.distances'}
        self.preset_variables(ref_dict=ref_dict)

        self.var_barcode_corners = self.order_points(np.array([self.var_all_corners[
                                                                   self.var_distances[i][1]]
                                                               for i in range(4)]))
        return self.var_barcode_corners

    @property
    def point_angles(self):
        '''Get Barcode corner co-ordinates'''
        ref_dict = {'var_barcode_corners': 'self.barcode_corners'}

        self.preset_variables(ref_dict=ref_dict)
        points = list(map(Point, self.var_barcode_corners))

        self.var_angle_arr = []
        for i in range(len(points)):
            l1 = Line(points[i], points[i - 1])
            try:
                l2 = Line(points[i], points[i + 1])
            except:
                l2 = Line(points[i], points[-len(points)])

            angle = l1.angle_between(l2)
            self.var_angle_arr.append(float(angle * (180 / pi)))
        return self.var_angle_arr

    @property
    def associated_pts(self):
        ref_dict = {'var_angle_arr': 'self.point_angles'}

        self.preset_variables(ref_dict=ref_dict)

        self.var_associat_pts = []
        self.angle_variance = []
        self.angle_diff = []
        for i in range(len(self.var_angle_arr)):
            if i:
                angles = [self.var_angle_arr[i], self.var_angle_arr[i - 1]]
                self.var_associat_pts.append([i, angles])
            else:
                angles = [self.var_angle_arr[i], self.var_angle_arr[-1]]
                self.var_associat_pts.append([i, angles])
            self.angle_variance.append([i, np.var(angles)])
            self.angle_diff.append
        return self.var_associat_pts, self.angle_variance

    @property
    def associat_var(self):
        ref_dict = {'var_angle_arr': 'self.point_angles'}

        self.preset_variables(ref_dict=ref_dict)

    def order_points(self, points):
        rect = np.zeros((4, 2), dtype="float32")

        s = points.sum(axis=1)
        rect[0] = points[np.argmin(s)]
        rect[2] = points[np.argmax(s)]

        diff = np.diff(points, axis=1)
        rect[1] = points[np.argmin(diff)]
        rect[3] = points[np.argmax(diff)]

        return rect

    @property
    def vert_dist(self):
        ref_dict = {'var_all_corners': 'self.all_corners()',
                    'var_barcode_corners': 'self.barcode_corners'}
        self.preset_variables(ref_dict=ref_dict)

        p1, p2 = self.var_barcode_corners[:2]
        ppcular_dist = []
        for i in range(len(self.var_all_corners)):
            p3 = self.var_all_corners[i]
            d = abs(np.cross(p2 - p1, p3 - p1) / np.linalg.norm(p2 - p1))
            ppcular_dist.append(d)
        self.var_vert_dist = np.array(ppcular_dist)
        return self.var_vert_dist

    @property
    def sorted_distances(self):
        ref_dict = {'var_vert_dist': 'self.vert_dist'}
        self.preset_variables(ref_dict=ref_dict)

        self.var_sorted_dist = np.sort(self.var_vert_dist)
        return self.var_sorted_dist

    @property
    def distance_gradients(self):
        ref_dict = {'var_sorted_dist': 'self.sorted_distances'}
        self.preset_variables(ref_dict=ref_dict)

        self.var_dist_grad = np.gradient(self.var_sorted_dist)
        return self.var_dist_grad

    @property
    def highest_peak(self):
        ref_dict = {'var_dist_grad': 'self.distance_gradients'}
        self.preset_variables(ref_dict=ref_dict)
        self.var_highest_peak = np.max(self.distance_gradients)
        return self.var_highest_peak

    def get_peaks(self, height=0.7):
        ref_dict = {'var_dist_grad': 'self.distance_gradients',
                    'var_highest_peak': 'self.highest_peak',
                    'var_sorted_dist': 'sorted_distances'}
        self.preset_variables(ref_dict=ref_dict)

        peak_indices, peaks = find_peaks(self.var_dist_grad,
                                         height=height * self.var_highest_peak)
        peak_indices = np.append(peak_indices, len(self.var_sorted_dist))

        self.var_peak_indices, self.var_peaks = peak_indices, peaks['peak_heights']
        return self.var_peak_indices, self.var_peaks

    def search_threshold(self, threshold_arr=[0.1, 0.15,
                                              0.2, 0.25,
                                              0.3, 0.35,
                                              0.4, 0.45
                                              # 0.5, 0.55,
                                              # 0.6, 0.65,
                                              # 0.7, 0.75
                                              ]):

        ref_list = ['var_all_corners', 'var_distances', 'var_barcode_corners',
                    'var_vert_dist', 'var_sorted_dist', 'var_dist_grad',
                    'var_highest_peak', 'peak_indices', 'peaks']

        variance_data_arr = []
        peaks_data_arr = []
        self.threshold_arr = []
        present_flag = False
        for threshold in threshold_arr:

            try:
                self.clear_variables(ref_list=ref_list)
                self.all_corners(threshold=threshold)
                peak_indices, peaks = self.get_peaks(height=0.4)
                variance_data = [np.var(peaks), np.var(np.gradient(peak_indices[:-1]))]
                peaks_data_arr.append(peaks)
                variance_data_arr.append(variance_data)
                self.threshold_arr.append(threshold)
                present_flag = True
            except:
                pass

        if present_flag:
            self.variance_data_arr = np.transpose(variance_data_arr)
            self.peaks_data_arr = peaks_data_arr
            return self.threshold_arr

    @property
    def optim_threshold(self):
        ref_dict = {'variance_data_arr': 'self.search_threshold()'}

        self.preset_variables(ref_dict=ref_dict)
        variance_data, peaks_data = self.variance_data_arr, self.peaks_data_arr
        save_var = {'variance': variance_data, 'peaks': peaks_data}

        maximum = max([max(peak) for peak in peaks_data])
        cap = 0.8 * maximum
        print(f'maximum: {maximum}')
        masks = [peak >= cap for peak in peaks_data]
        sizes = [np.size(peak, axis=0) for peak in peaks_data]

        good_heights = [np.count_nonzero(mask) == size for mask, size in zip(masks, sizes)]
        order = variance_data.argsort()
        ranks = order.argsort()
        total_variance = np.var(ranks, axis=0)
        weighted_ranks = np.sum(ranks, axis=0)
        cum_ranks = np.sum([total_variance, weighted_ranks], axis=0)
        index = np.argmin(cum_ranks)

        print(f'good_heights: {good_heights}')
        print(f'index: {index}')

        if good_heights[index]:
            self.var_optim_threshold = self.threshold_arr[index]
            return self.var_optim_threshold
        else:
            return None

    @property
    def class_centers(self):
        '''To calculate centroids by averaging distance measures between peaks'''
        ref_dict = {'var_sorted_dist': 'self.sorted_distances',
                    'var_peak_indices': 'self.get_peaks()'}
        self.preset_variables(ref_dict=ref_dict)

        centroids = []
        peak_indices, _ = self.get_peaks()
        for i in range(len(self.var_peak_indices)):
            if i == 0:
                min_idx = 0
                max_idx = self.var_peak_indices[i]
            elif i == len(self.var_peak_indices) - 1:
                min_idx = self.var_peak_indices[i - 1]
                max_idx = len(self.var_sorted_dist)
            else:
                min_idx = self.var_peak_indices[i - 1]
                max_idx = self.var_peak_indices[i]

            centroids.append(np.mean(self.var_sorted_dist[min_idx: max_idx]))

        return centroids

    @property
    def labels(self):
        '''To get the labels of the co-ordinates with centroids as reference'''
        ref_dict = {'var_vert_dist': 'self.vert_dist',
                    'var_sorted_dist': 'self.sorted_distances',
                    'var_peak_indices': 'self.get_peaks()'}
        self.preset_variables(ref_dict=ref_dict)

        labels = copy.deepcopy(self.var_vert_dist)
        for i in range(len(self.var_peak_indices)):
            if i == 0:
                min_idx = 0
                max_idx = self.var_peak_indices[i]
            elif i == len(self.var_peak_indices) - 1:
                min_idx = self.var_peak_indices[i - 1]
                max_idx = len(self.var_sorted_dist) - 1
            else:
                min_idx = self.var_peak_indices[i - 1]
                max_idx = self.var_peak_indices[i]

            if i != len(self.var_peak_indices) - 1:
                labels[np.where((self.var_vert_dist < self.var_sorted_dist[max_idx]) &
                                (self.var_vert_dist >= self.var_sorted_dist[min_idx]))] = i
            else:
                labels[np.where((self.var_vert_dist <= self.var_sorted_dist[max_idx]) &
                                (self.var_vert_dist >= self.var_sorted_dist[min_idx]))] = i

        self.var_labels = labels.astype(int)
        return self.var_labels

    @property
    def center_coordinates(self):
        '''Find the centroid of the coordinates'''
        ref_dict = {'var_all_corners': 'self.all_corners(self.optim_threshold)',
                    'var_labels': 'self.labels'}
        self.preset_variables(ref_dict=ref_dict)

        centroid_arr = []
        coords = self.var_all_corners
        for i in range(len(np.unique(self.var_labels))):
            selected_bin = coords[np.where(self.var_labels == i)]
            centroid = np.mean(selected_bin, axis=0)
            centroid_arr.append(centroid)
        self.var_center_coord = centroid_arr
        return self.var_center_coord

    def round(self, data):
        return np.round(data, 3)

    @property
    def concurrent_feature_sep(self):
        ref_dict = {'var_center_coord': 'self.center_coordinates',
                    'var_barcode_corners': 'self.barcode_corners'}
        self.preset_variables(ref_dict=ref_dict)

        (p1, p2) = self.var_barcode_corners[:2]
        self.top_border = Line(Point(p1), Point(p2))
        mid_point = Point(p1).midpoint(Point(p2))
        self.ppcular_line = self.top_border.perpendicular_line(mid_point)

        collect_feature_sep = []
        for centroid in self.var_center_coord:
            parallel_line = self.top_border.parallel_line(centroid)
            feature_sep = self.ppcular_line.intersection(parallel_line)
            collect_feature_sep.append(feature_sep[0])

        self.var_concur_feat_sep = collect_feature_sep
        return self.var_concur_feat_sep

    def line_count(self, req_line_cnt):
        ref_dict = {'var_concur_feat_sep': 'self.concurrent_feature_sep'}

        self.preset_variables(ref_dict=ref_dict)
        line_count = req_line_cnt
        (p1, p2) = self.var_concur_feat_sep[:2]
        (x, y), (s, t) = p1, p2
        d8 = max(abs(x - s), abs(y - t))

        if line_count > (d8 / 2.2):
            line_count = int(d8 / 2.2)
        else:
            line_count += 1
        norm_distance = 1 / line_count
        self.var_line_count = line_count - 1
        self.var_norm_distance = norm_distance
        return self.var_line_count, self.var_norm_distance

    def int_profile(self, req_line_cnt=3):
        ref_dict = {'var_concur_feat_sep': 'self.concurrent_feature_sep',
                    'var_image_corners': 'self.image_corners'}

        self.preset_variables(ref_dict=ref_dict)
        self.line_count(req_line_cnt=req_line_cnt)

        collect_feature_sep = self.var_concur_feat_sep

        self.start_pt = []
        self.end_pt = []

        int_prof_arr = []
        for i in range(len(collect_feature_sep) - 1):

            t = Symbol('t', real=True)
            param_point = Line(collect_feature_sep[i],
                               collect_feature_sep[i + 1]).arbitrary_point('t')
            data_arr = []
            for j in range(self.var_line_count):
                ref_point = param_point.subs(t, self.var_norm_distance * (j + 1))
                horiz_line = self.top_border.parallel_line(ref_point)

                left_intersect = horiz_line.intersection(self.left_border)
                right_intersect = horiz_line.intersection(self.right_border)

                left_coord = tuple(map(int, left_intersect[0]))
                right_coord = tuple(map(int, right_intersect[0]))

                # Get Line Profile
                r_start, c_start = left_coord
                r_end, c_end = right_coord

                self.start_pt.append(left_coord)
                self.end_pt.append(right_coord)

                line = np.transpose(np.array(draw.line(r_start, c_start, r_end, c_end)))
                data = self.var_gray_img[line[:, 1], line[:, 0]]
                data_arr.append(data)
            int_prof_arr.append(data_arr)

        self.var_int_prof_arr = int_prof_arr
        return self.var_int_prof_arr, self.var_line_count

    def overlay_corners(self, image, corners, radius, color, inplace=False):
        if not inplace:
            overlaid_image = copy.deepcopy(image)
        else:
            overlaid_image = image

        corners = tuple(map(tuple, corners))
        for corner in corners:
            cv2.circle(overlaid_image, corner, radius, color, -1)
        return overlaid_image

    def overlay_lines(self, image, start, end, line_thickness, line_color, inplace=False):
        if not inplace:
            overlaid_image = copy.deepcopy(image)
        else:
            overlaid_image = image

        start, end = tuple(map(tuple, start)), tuple(map(tuple, end))
        for start_line, end_line in zip(start, end):
            cv2.line(overlaid_image, start_line, end_line, line_color, line_thickness)
        return overlaid_image

    @property
    def debug_image(self):
        ref_dict = {'var_all_corners': 'self.all_corners(self.optim_threshold)',
                    'start_pt': 'self.int_profile()',
                    'var_concur_feat_sep': 'self.concurrent_feature_sep',
                    'var_image_corners': 'self.image_corners'}

        self.preset_variables(ref_dict=ref_dict)
        debug_img = copy.deepcopy(self.var_image)

        if not debug_img.ndim == 3:
            debug_img = np.stack((debug_img,) * 3, axis=-1)

        pt1_thickness, pt1_color = 4, (255, 0, 0)
        pt2_thickness, pt2_color = 7, (0, 0, 255)
        pt3_thickness, pt3_color = 7, (0, 0, 255)
        l1_thickness, l1_color = 2, (0, 0, 255)
        l2_thickness, l2_color = 1, (0, 255, 0)
        feature_sep = self.var_concur_feat_sep

        left_coord_arr = []
        right_coord_arr = []
        for sep_point in feature_sep:
            horiz_line = self.top_border.parallel_line(sep_point)

            left_intersect = horiz_line.intersection(self.left_border)
            right_intersect = horiz_line.intersection(self.right_border)

            left_coord_arr.append(map(int, left_intersect[0]))
            right_coord_arr.append(map(int, right_intersect[0]))

        debug_img = self.overlay_corners(debug_img, self.var_all_corners, pt1_thickness, pt1_color)
        debug_img = self.overlay_corners(debug_img, self.var_barcode_corners, pt2_thickness, pt2_color)
        debug_img = self.overlay_corners(debug_img, self.var_concur_feat_sep, pt3_thickness, pt3_color)
        debug_img = self.overlay_lines(debug_img, left_coord_arr, right_coord_arr, l1_thickness, l1_color)
        debug_img = self.overlay_lines(debug_img, self.start_pt, self.end_pt, l2_thickness, l2_color)
        return debug_img

    @property
    def plot_int_profile(self):
        ref_dict = {'var_int_prof_arr': 'self.int_profile()'}

        data = self.var_int_prof_arr
        fig_arr = []
        for i in range(len(data)):
            fig, ax = plt.subplots()
            for j in range(len(data[i])):
                elem_data = data[i][j]
                ax.plot(range(elem_data.shape[0]), elem_data, label='Line ' + str(j + 1))
            ax.legend()
            fig_arr.append(fig)
        return fig_arr

    @property
    def plot_gradient(self):
        '''Visualization of height of each step'''
        ref_dict = {'var_dist_grad': 'self.distance_gradients'}

        self.preset_variables(ref_dict=ref_dict)
        x = np.arange(len(self.var_dist_grad))
        y = self.var_dist_grad
        fig = plt.figure()
        ax = plt.subplot(111)
        ax.plot(x, y)
        plt.title('Peaks Graph')
        return fig
