import cv2
import time

INPUT_IMAGE_PATH = '/media/Work/Codes/Private/pdf417-decoder/output/images/Method1/4_Canny/18_03_2021/11.00.00/18_03_2021_11.49.28.553_000/00001_1.bmp'

# Read gray image
img = cv2.imread(INPUT_IMAGE_PATH, 0)

# Create default parametrization LSD
lsd = cv2.createLineSegmentDetector(0)

start = time.time()
# Detect lines in the image
lines = lsd.detect(img)[0]  # Position 0 of the returned tuple are the detected lines
print(f'Time Duration: {time.time() - start}')

# Draw detected lines in the image
drawn_img = lsd.drawSegments(img, lines)

# Show image
cv2.imshow("LSD", drawn_img)
cv2.waitKey(0)
