import cv2
import time

INPUT_IMAGE_PATH = '/media/Work/Codes/Private/pdf417-decoder/output/images/Method1/4_Canny/18_03_2021/11.00.00/18_03_2021_11.49.28.553_000/00001_1.bmp'
INPUT_IMAGE_PATH = '/media/Work/Codes/Private/pdf417-decoder/output/images/Method1/4_Canny/18_03_2021/11.00.00/18_03_2021_11.49.28.553_000/00001_1.bmp'


img = cv2.imread(INPUT_IMAGE_PATH, 0)

fld = cv2.ximgproc.createFastLineDetector()

start = time.time()
lines = fld.detect(img)
print(f'lines: {lines}')

print(f'Total Time: {time.time() - start}')

result_img = fld.drawSegments(img, lines)

cv2.imshow("Result", result_img)
cv2.waitKey(0)
