import os
import cv2
import csv
import time
import copy
import shutil
import numpy as np
from natsort import natsorted
from barcode_detector import BarcodeDetector
from PDF417_decoderV2 import Custom_PDF417

# LD_LIBRARY_PATH=/usr/local/cuda/lib64:/usr/local/cuda/extras/CUPTI/lib64:

MODEL_PATH = './pdf417_model/PDF417_frozen_inference_graph_Gray_v2.pb'
# INPUT_PATH = '/media/Work/Images/TVS ToteID/TVS-PPK/PPK/15-02-2021/cleaned_data/PDF417/IP_Camera/PDF417/15-02-2021'
INPUT_PATH = '/media/Work/Images/TVS ToteID/Text/BaumerBarcodeTest/PDF417/18_03_2021'
OUTPUT_PATH = '/media/Work/Codes/Private/pdf417-decoder/output/images'

OUTPUT_IMAGE_PATH = os.path.join(OUTPUT_PATH, 'Input')
DETECTOR_OUTPUT_PATH = os.path.join(OUTPUT_PATH, 'Detection')
CROP_PATH = os.path.join(OUTPUT_PATH, 'Crop')
BIN_OUTPUT_PATH = os.path.join(OUTPUT_PATH, 'Binary')

bd = BarcodeDetector(MODEL_PATH, 'GRAY')


def put_rectangle(img, bbox):
    return cv2.rectangle(img, (bbox[1], bbox[0]),
                         (bbox[3], bbox[2]), (0, 255, 0), 5)


def clean_dir(folder_path):
    if os.path.isdir(folder_path):
        shutil.rmtree(folder_path)
    os.makedirs(folder_path)


date = os.path.split(INPUT_PATH)[1]
hour_list = natsorted(os.listdir(INPUT_PATH))

# custom_pdf417_decoder = Custom_PDF417(search_thresholds=(0.15, 0.2, 0.25, 0.3, 0.35),
custom_pdf417_decoder = Custom_PDF417(search_thresholds=[0.25],
                                      pixel_var_threshold=12,
                                      line_prof_cnt=3)

for hour in hour_list:
    hour_path = os.path.join(INPUT_PATH, hour)
    track_list = natsorted(os.listdir(hour_path))

    for track in track_list:
        track_path = os.path.join(hour_path, track)
        image_list = natsorted(os.listdir(track_path))

        detector_track_path = os.path.join(DETECTOR_OUTPUT_PATH, date, hour, track)
        crop_track_path = os.path.join(CROP_PATH, date, hour, track)
        bin_track_path = os.path.join(BIN_OUTPUT_PATH, date, hour, track)

        clean_dir(detector_track_path)
        clean_dir(crop_track_path)
        clean_dir(bin_track_path)

        for image_file in image_list:
            try:
                image_file_path = os.path.join(track_path, image_file)
                print(f'image_file_path: {image_file_path}')
                image = cv2.imread(image_file_path, 0)

                print(image.shape)

                boxes = bd.predict(image)
                detected_image = copy.deepcopy(image)

                for i, box in enumerate(boxes):
                    x1, y1, x2, y2 = list(map(int, box))
                    new_image = image[x1:x2, y1:y2]

                    image_name, ext = os.path.splitext(image_file)
                    print(f'image_name: {image_name}')

                    # crop_file_path = os.path.join(input_track_path, image_name + '_' + str(i+1) + '.bmp')
                    # cv2.imwrite(crop_file_path, new_image)

                    put_rectangle(detected_image, box)
                    det_file_path = os.path.join(detector_track_path, image_name + '_' + str(i+1) + ext)
                    cv2.imwrite(det_file_path, detected_image)
                    # width, height = image.shape
                    #
                    # print(CROP_PATH)
                    print(new_image.shape)

                    # try:
                    bin_file_path = os.path.join(bin_track_path, image_name + '_' + str(i+1) + ext)

                    start = time.time()
                    decoded_data = custom_pdf417_decoder.decode(new_image, debug=False)
                    print(f'Total Time: {time.time() - start}')

                    if len(decoded_data):
                        decoded_data = decoded_data['code']
                        if decoded_data != 'failed':
                            print(f'decoded_data: passed')
                            print(f'decoded_data: {decoded_data}')

                        else:
                            print(f'decoded_data: {decoded_data}')
                        crop_file_path = os.path.join(crop_track_path, image_name + '_' +
                                                      str(i + 1) + '_' + decoded_data + '.bmp')
                        cv2.imwrite(crop_file_path, new_image)
                    else:
                        print(f'decoded_data: None')
                    # if decoded_data!= None:
                    #     print(f'decoded_data: {decoded_data}')
                    #     print(f'bin_file_path: {bin_file_path}')
                    #     bin_file_path = os.path.join(bin_track_path, image_name + '_' + decoded_data + ext)
                    # else:
                    #     print(f'decoded_data: None')
                    #     bin_file_path = os.path.join(bin_track_path, image_name + '_' + str(i+1) + ext)
            except:
                print('failed')