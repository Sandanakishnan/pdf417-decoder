import cv2
import time
import numpy as np
from decoderV3 import PDF417Decoder
from barcode_linefeaturesV4 import GetBarcodeFeatures
import re
import shutil
import pandas as pd
import os

threshold = 0.2
req_line_cnt = 3
height = 0.5


def clean_dir(folder_path):
    if os.path.isdir(folder_path):
        shutil.rmtree(folder_path)
    os.makedirs(folder_path)


class custom_PDF417():
    def __init__(self):
        self.decoder = PDF417Decoder()
        self.regex = re.compile('[@_!#$%^&*()<>?/\}{~:;.|=+,-]')
        # self.lsd = cv2.createLineSegmentDetector(0)

    def decode(self, img, debug=False):
        start = time.time()
        result_data = dict(code='failed')
        decoded_data = ''

        line_profile, \
        line_profile_points, \
        preprocessed_image = GetBarcodeFeatures.extract_rows_static_thres(img,
                                                                          threshold,
                                                                          height,
                                                                          req_line_cnt,
                                                                          extra_pixels=5,
                                                                          var_threshold=5)
        if debug:
            debug_image = GetBarcodeFeatures.debug_image_from_scratch(img,
                                                                      corner_threshold=threshold,
                                                                      height=0.5)
        else:
            debug_image = None
        lap1 = time.time()
        print('Line profile Extraction time : ', lap1 - start)
        line_profile_length = len(line_profile)

        print('line_profile_length: ', len(line_profile))

        int_prof = np.asarray(line_profile)

        if len(int_prof):
            try:
                # With global Threshold
                codewords = self.decoder.codeword_extraction(int_prof, line_prof_cnt=req_line_cnt)
                lap2 = time.time()
                print('Codeword Extraction time : ', lap2 - lap1)

                decoded_data = str(self.decoder.decode_codeword(codewords))
                lap3 = time.time()
                print('decoder time : ', lap3 - lap2)

                if decoded_data is None:
                    return result_data, preprocessed_image, debug_image, line_profile_length
                elif decoded_data == '':
                    return result_data, preprocessed_image, debug_image, line_profile_length
                elif bool(len(decoded_data)) and self.regex.search(decoded_data) == None:
                    result_data = dict(code=decoded_data)

            except:
                print('Excepted in PDF417 decoder')
        return result_data, preprocessed_image, debug_image, line_profile_length


INPUT_PATH = '/media/Work/Codes/Private/pdf417-decoder/input/images/new_data/rotated_nd_brightness_aug_crops/'
OUTPUT_THRESHOLD_PATH = '/media/Work/Codes/Private/pdf417-decoder/output/images/rotated_nd_brightness_aug_thresh/'
OUTPUT_DEBUG_PATH = '/media/Work/Codes/Private/pdf417-decoder/output/images/rotated_nd_brightness_aug_debug/'
OUTPUT_CSV_PATH = '/media/Work/Codes/Private/pdf417-decoder/output/images/rotated_nd_brightness_aug.csv'

clean_dir(OUTPUT_THRESHOLD_PATH)
clean_dir(OUTPUT_DEBUG_PATH)
images = os.listdir(INPUT_PATH)
pdf417 = custom_PDF417()
result = []
decode_count = 0
fail_count = 0
wrong_decode_count = 0
except_cnt = 0
for i in range(len(images)):
    print(images[i])
    start = time.time()
    crop_image = cv2.imread(INPUT_PATH + images[i], 0)

    try:
        result_data, preprocessed_image, debug_image, lineprof_len = pdf417.decode(crop_image, debug=True)

        cv2.imwrite(OUTPUT_THRESHOLD_PATH + images[i][:-4] + '_threh_lp_' + str(lineprof_len) + '.jpg',
                    preprocessed_image)
        if debug_image is not None:
            cv2.imwrite(OUTPUT_DEBUG_PATH +
                        images[i][:-4] +
                        '_debug_lp_' +
                        str(lineprof_len) +
                        '.jpg', debug_image)

        end = time.time()
        gt = images[i].split('_')[0]
        if result_data['code'] == 'failed':
            fail_count += 1
        elif result_data['code'] in gt:
            decode_count += 1
        else:
            wrong_decode_count += 1

        if result_data['code'] not in 'failed':
            print(f'Time Duration: {end - start}')
            print(result_data)
        result.append([images[i], result_data['code']])
    except:
        except_cnt += 1

df = pd.DataFrame(result)

df.to_csv(OUTPUT_CSV_PATH)

print('Wrong decode: ', wrong_decode_count)
print('Correctly decoded: ', decode_count)
print('failed: ', fail_count)
print('exception: ', except_cnt)
