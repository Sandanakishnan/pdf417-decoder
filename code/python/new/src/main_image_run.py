import cv2
import time
from barcode_detector import BarcodeDetector
import copy
from natsort import natsorted
from barcode_detector import BarcodeDetector
from PDF417_decoderV1 import custom_PDF417

# LD_LIBRARY_PATH=/usr/local/cuda/lib64:/usr/local/cuda/extras/CUPTI/lib64:

MODEL_PATH = './pdf417_model/PDF417_frozen_inference_graph_Gray_v1.pb'
# INPUT_PATH = 'in/IP_TVS_2020_04_21_00014.png'
INPUT_PATH = '/media/TrackDiagnostics/17-02-2021/18.00.00/18.59.59.967_3bbfe06a-7124-11eb-8ab2-7bd193772022/' \
             'InputStream/32.jpg'
# INPUT_PATH = 'in/IP_TVS_2020_04_21_00009_00016.png'
DETECTOR_OUTPUT_PATH = 'out/Detection_Output.png'
CROP_PATH = 'out/Crop_Output.png'
BIN_OUTPUT_PATH = 'out/Bin_Output.png'

bd = BarcodeDetector(MODEL_PATH, 'GRAY')
image = cv2.imread(INPUT_PATH, 0)

custom_pdf417_decoder = custom_PDF417()
print(image.shape)

boxes = bd.predict(image)
detected_image = copy.deepcopy(image)
#
#
print(f'boxes: {boxes}')

def put_rectangle(img, bbox):
    return cv2.rectangle(img, (bbox[1], bbox[0]),
                         (bbox[3], bbox[2]), (0, 255, 0), 5)
#
#
for box in boxes:
    x1, y1, x2, y2 = list(map(int, box))
    new_image = image[x1:x2, y1:y2]
    cv2.imwrite(CROP_PATH, new_image)
    put_rectangle(detected_image, box)

# new_image = image

cv2.imwrite(CROP_PATH, new_image)
cv2.imwrite(DETECTOR_OUTPUT_PATH, detected_image)

start = time.time()
decoded_data = custom_pdf417_decoder.decode(new_image, debug=True)
print(f'Total Time: {time.time() - start}')

print(f'decoded_data: {decoded_data}')
# cv2.imwrite(BIN_OUTPUT_PATH, debug_image)