import time
import numpy as np
import os
import tensorflow as tf
import cv2
import copy

from object_detection.utils import label_map_util
from object_detection.utils import visualization_utils as vis_util

class BarcodeDetector:

    def __init__(self, BASE_PATH, NUM_CLASSES):
        # print('Entered initialize')
        MODEL_NAME = 'Inference_Graph'
        PATH_TO_FROZEN_GRAPH = os.path.join(BASE_PATH, MODEL_NAME, 'frozen_inference_graph.pb')
        PATH_TO_LABELS = os.path.join(BASE_PATH, 'annotations', 'label_map.pbtxt')

        self.detection_graph = tf.Graph()
        with self.detection_graph.as_default():
          od_graph_def = tf.compat.v1.GraphDef()
          with tf.io.gfile.GFile(PATH_TO_FROZEN_GRAPH, 'rb') as fid:
            serialized_graph = fid.read()
            od_graph_def.ParseFromString(serialized_graph)
            tf.import_graph_def(od_graph_def, name='')

        label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
        categories = label_map_util.convert_label_map_to_categories(label_map,
                                                                    max_num_classes=NUM_CLASSES,
                                                                    use_display_name=True)
        self.category_index = label_map_util.create_category_index(categories)

    def preset_variables(self, ref_dict):
        # print('Entered preset_variables')

        for key in ref_dict:
            if key not in self.__dict__:
                exec(ref_dict[key])

    @property
    def declare_tensors(self):
        # print('Entered declare_tensors')
        self.image_tensor = self.detection_graph.get_tensor_by_name('image_tensor:0')
        self.detection_boxes = self.detection_graph.get_tensor_by_name('detection_boxes:0')
        self.detection_scores = self.detection_graph.get_tensor_by_name('detection_scores:0')
        self.detection_classes = self.detection_graph.get_tensor_by_name('detection_classes:0')
        self.num_detections = self.detection_graph.get_tensor_by_name('num_detections:0')

        session_config = tf.compat.v1.ConfigProto(gpu_options=tf.compat.v1.GPUOptions(allow_growth=True,
                                                                                      per_process_gpu_memory_fraction=0.333))
        self.sess = tf.compat.v1.Session(graph=self.detection_graph, config=session_config)

        return self.image_tensor, self.detection_boxes, self.detection_scores, self.detection_classes, self.num_detections

    def predict(self, image, custom_flg=None):
        "set custom_flg as 1 for predict gray scale images"
        # print('Entered predict')
        ref_dict = dict(image_tensor='self.declare_tensors')

        self.preset_variables(ref_dict=ref_dict)
        self.custom_flg = custom_flg
        if self.custom_flg ==1:
            (im_width, im_height) = image.size
            image_np_expanded = np.array(image).reshape((1,im_height, im_width, 1)).astype(np.uint8)
            image_np_expanded = image_np_expanded
        else:
            image_np_expanded = np.expand_dims(image, axis=0)
        self.image = image
    
        options = tf.compat.v1.RunOptions(trace_level=tf.compat.v1.RunOptions.FULL_TRACE)
        run_metadata = tf.compat.v1.RunMetadata()

        print(image_np_expanded.shape)

        (self.boxes, self.scores, self.classes, self.num) = self.sess.run(
            [self.detection_boxes, self.detection_scores, self.detection_classes, self.num_detections],
            feed_dict={self.image_tensor: image_np_expanded},
            options=options, run_metadata=run_metadata)

        return self.boxes, self.scores, self.classes, self.num

    @property
    def visualize(self):
        # print('Entered visualize')
        if self.custom_flg  == 1:
            (im_width, im_height) = self.image.size  
            image = np.array(copy.deepcopy(self.image))
            rgbimg=np.zeros([im_height,im_width,3])
            rgbimg[:,:,0]=image
            rgbimg[:,:,1]=image
            rgbimg[:,:,2]=image
            
            self.viz_image = copy.deepcopy(rgbimg)
            vis_util.visualize_boxes_and_labels_on_image_array(
                self.viz_image,
                np.squeeze(self.boxes),
                np.squeeze(self.classes).astype(np.int32),
                np.squeeze(self.scores),
                self.category_index,
                use_normalized_coordinates=True,
                line_thickness=8)
        else:
            self.viz_image = np.array(copy.deepcopy(self.image))
            vis_util.visualize_boxes_and_labels_on_image_array(
                self.viz_image,
                np.squeeze(self.boxes),
                np.squeeze(self.classes).astype(np.int32),
                np.squeeze(self.scores),
                self.category_index,
                use_normalized_coordinates=True,
                line_thickness=8)
        return self.viz_image

# BASE_PATH = '/media/Work/Code/Private_Repo/mobilenet-detector'
# NUM_CLASSES = 1
#
# barcode_detector = BarcodeDetector(BASE_PATH=BASE_PATH, NUM_CLASSES=NUM_CLASSES)
#
# image_path = '/home/sandana/Desktop/test'
# image_files = os.listdir(image_path)
#
# for k in range(len(image_files)):
#
#     image = cv2.imread(os.path.join(image_path, image_files[k]))
#
#     # Actual detection.
#     start_time = time.time()
#     boxes, scores, classes, num = barcode_detector.predict(image)
#     print('Iteration %d: %.3f sec' % (k, time.time() - start_time))
#     viz_image = barcode_detector.visualize
#
#     boxes = boxes[0]
#     scores = scores.ravel()
#     indices = np.argwhere(scores>0.9).ravel()
#
#     width, height, _ = image.shape
#     for i in range(len(indices)):
#         print(boxes[i])
#
#         bbox = [boxes[i][0] * width, boxes[i][1] * height, boxes[i][2] * width, boxes[i][3] * height]
#
#         x1, y1, x2, y2 = list(map(int, bbox))
#         print(x1, y1, x2, y2)
#
#         new_image = image[x1:x2, y1:y2, :]
#
#         cv2.imwrite(os.path.join('/home/sandana/Desktop/temp5', str(k) + '_' + str(i) + '.png'), new_image)
#     cv2.imwrite(os.path.join('/home/sandana/Desktop/temp4', str(k)+'.png'), viz_image)
