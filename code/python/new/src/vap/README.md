# Visual Analytics Platform - VAP

## What's this?

We aim to make this a repository consisting of highly reusable components that will aid in
rapid development of Computer Vision applications.

So devs can focus less on housekeeping and more on pixels!

## To use in your project

1. First make a git based repository for your project 
    
    `mkdir -p cool-project && cd cool-project && git init`
    
    Visit RecodeVision's [bitbucket](https://bitbucket.org/%7B73b4b5d9-be34-44ce-abf6-ece12119af15%7D/) and create a
    project + repo and get it's remote url, then
    
    `git remote add origin git@bitbucket.org:aeyeonerecode/cool-project.git`
    
    `git push -u origin master`
 
2. Add this repository as a submodule in your project

    Inside root directory of the repository, let's say you organize code inside src
    
    `mkdir src` then add submodule inside `cool-project/src/vap`
    
    `git submodule add git@bitbucket.org:aeyeonerecode/vap.git src/vap`
    
    This will add a new `.gitmodules` file as well as a pointer to the submodule. 
    Add them both and commit them
    
    `git add .gitmodules src/vap`
    
    `git commit -m "added vap submodule"`
    
    `git push`
    
    Done!

3. Import and start using!

## Example Usage

An example usage of VideoFeed

```python
from vap.sources import VideoFeed    
for each_frame in VideoFeed(some-opencv-compatible-cam-source):
    do_awesome_stuff(each_frame)
```
   
## To learn more

    Check documentation in `vap/docs` folder
    
## Cheatsheet

* To update to latest vap 

    ```bash
    cd src/vap
    git pull
    cd ..
    git add vap
    git commit -m "updated vap"
    git push
    ```

* To switch to correct vap version after a branch switch/checkout

    ```bash
    git submodule update
    ```