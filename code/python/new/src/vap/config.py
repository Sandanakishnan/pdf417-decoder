"""
Classes that define configuration that can be passed to instantiate other classes like TFModel
"""

import json
import os
import shutil

import vap.utils.classes
from . import base
from . import exceptions
from . import utils

class CNNEstimatorConfig(base.CNNEstimatorConfigBase):
    """ NN Framework Configruation """
    def __init__(self, model_path=None, gpu_memory_fraction=None, cuda_visible_device_list=None):
        super(CNNEstimatorConfig, self).__init__()

        if model_path is None:
            raise exceptions.ConfigurationError("model_path is not set")

        if gpu_memory_fraction is None:
            raise exceptions.ConfigurationError("gpu_memory_fraction is not set")

        if cuda_visible_device_list is None:
            raise exceptions.ConfigurationError("cuda_visible_device_list is not set")

        self._model_path = model_path
        self._gpu_memory_fraction = gpu_memory_fraction
        self._cuda_visible_device_list = cuda_visible_device_list

    @property
    def model_path(self):
        return self._model_path

    @property
    def gpu_memory_fraction(self):
        return self._gpu_memory_fraction

    @property
    def cuda_visible_device_list(self):
        return self._cuda_visible_device_list


class TFModelConfig(CNNEstimatorConfig):
    """ TF Model Configuration """
    def __init__(self, model_path=None, gpu_memory_fraction=None, gpu_allow_growth=True, cuda_visible_device_list=None, input_tensors=[], output_tensors=[], import_name='import'):
        """
        Config object needed to initialize a TFModel instance

        Parameters
        ----------
        model_path : str
            Path to the frozen graph - .pb
        gpu_memory_fraction : float
            Percentage in range of 0-1 of available GPU memory that should be allocated to serve this model
        gpu_allow_growth : bool
            Expands model to take up as much available space in GPU
        cuda_visible_device_list : str
            List of device IDs that can be used to allocate the model
        input_tensors : List [ str ]
            List of string tensor names corresponding to input of the NN
            Ex. `["import/image_tensor:0"]`
        output_tensors : List [ str ]
            List of string tensor names corresponding to output of the NN.
            Ex. `["import/boxes:0", "import/scores:0", "import/num_boxes:0"]`
        import_name : Optional str
            A string prefixed to the tensor names loaded from the frozen graph. Default is `import`
        """
        
        super(TFModelConfig, self).__init__(model_path, gpu_memory_fraction, cuda_visible_device_list)

        # if len(input_tensors) != 1:
        #     raise exceptions.ConfigurationError("There can be only one input tensor")

        if len(output_tensors) < 1:
            raise exceptions.ConfigurationError("There must be more than one output tensor")
            
        self._input_tensors = input_tensors
        self._output_tensors = output_tensors
        self._import_name = import_name
        self._gpu_allow_growth = gpu_allow_growth

    @property
    def input_tensors(self):
        return self._input_tensors

    @property
    def output_tensors(self):
        return self._output_tensors

    @property
    def import_name(self):
        return self._import_name

    @property
    def gpu_allow_growth(self):
        return self._gpu_allow_growth


class CVModelConfig(CNNEstimatorConfig):
    """ TF Model Configuration """
    def __init__(self, model_path=None, proto_path=None, gpu_memory_fraction=None, cuda_visible_device_list=None):
        """
        Config object needed to initialize a CVModel instance

        Parameters
        ----------
        model_path : str
            Path to the cv caffe model file
        proto_path : str
            Path to the cv prototxt model file
        """
        
        super(CVModelConfig, self).__init__(model_path, gpu_memory_fraction, cuda_visible_device_list)
        self._proto_path = proto_path

    @property
    def proto_path(self):
        return self._proto_path



class AppConfig(base.VAPBase):
    """"""
    def __init__(self, json_path=None):
        """
        JSON-based configuration reader that reads a json file and makes the key-value pairs available in dot-notation

        Parameters
        ----------
        json_path : str
            Path to json file

        Usage
        ------
        ```
        config = AppConfig('config.json')
        ## {"detector": {"threshold":0.8}}
        threshold = config.detector.threshold

        ```
        """

        super(AppConfig, self).__init__()

        self.json_path = json_path

        if os.path.exists(json_path):
            with open(json_path) as f:
                self.__dict__ = json.loads(f.read(), object_hook=vap.utils.classes.AttrDict.from_dict)
        else:
            self.__dict__ = {}

    
    def save(self, path=None):
        if path is None:
            path = self.json_path
        if os.path.exists(path):
            backup_path = path[:-4] + "bk.json"
            shutil.copy(path, backup_path)

        with open(path, "w") as jsf:
            jsf.write(json.dumps(self.__dict__, indent=4))