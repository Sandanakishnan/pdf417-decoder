"""
Exception classes that are raised by some VAP Components
"""

from . import base

class ConfigurationError(base.ExceptionBase):
    """ Raised due to invalid configuration """
    pass 

class InputFeedError(base.ExceptionBase):
    """ Raised due to input read error """
    pass

class CameraFeedError(InputFeedError):
    """ Raised due to error in reading camera feed """
    pass
