"""
`Kamerai-Process` - Wrapper class and functions for easier Multiprocessing Management and Health Monitoring support at Prodcution
"""
import json
import logging
import multiprocessing as mp
import os
import queue
import signal
import socket
import sys
import threading
import time
import cv2
from contextlib import closing
from multiprocessing.managers import SyncManager
from wsgiref import simple_server

import requests

HSTATUS_KEY_PID = "pid"
HSTATUS_KEY_UNIQUE_NAME = "uniqueName"
HSTATUS_KEY_PORT = "port"

if __name__ == '__main__':
    import bottle
else:
    from . import bottle

lg = logging.getLogger(__name__)


def run_and_wait(service):
    service.start()
    try:
        while service.is_alive():
            time.sleep(1)
    except KeyboardInterrupt:
        pass
    finally:
        service.join()


def get_free_port():
    with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as s:
        s.bind(('', 0))
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        return s.getsockname()[1]


class MultiprocessingProxyManager(SyncManager):
    """ Singleton Multiprocessing SyncManager derived class for conveniently creating Qs """
    __instance = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if MultiprocessingProxyManager.__instance != None:
            raise Exception("This class is a singleton!")
        else:
            MultiprocessingProxyManager.__instance = self

        self.start()

    def start(self, *args, **kwargs):
        def mgr_sig_handler(signal, frame):
            pass  # don't do anything

        # initilizer for SyncManager
        def mgr_init():
            signal.signal(signal.SIGINT, mgr_sig_handler)
            lg.debug("Initialized MultiprocessingProxyManager")

        super().start(mgr_init, *args, **kwargs)

    @staticmethod
    def getInstance():
        """ Static access method. """
        if MultiprocessingProxyManager.__instance == None:
            MultiprocessingProxyManager()
        return MultiprocessingProxyManager.__instance

    def __del__(self):
        if self.__instance is not None:
            self.__instance.stop()


class StoppableWSGIRefServer(bottle.ServerAdapter):

    def run(self, handler):
        if self.quiet:
            class QuietHandler(simple_server.WSGIRequestHandler):
                def log_request(*args, **kw): pass

            self.options['handler_class'] = QuietHandler
        self.server = simple_server.make_server(
            self.host, self.port, handler, **self.options)
        self.server.serve_forever()

    def stop(self):
        # self.server.server_close() <--- alternative but causes bad fd exception
        self.server.shutdown()


class KaiProcess(mp.Process):
    """
    KaiProcess is derived from multiprocessing.Process

    Process objects represent activity that is run in a separate process

    KaiProcess provides out-of-the-box support for

    * Health Monitoring with `self.monitored_status_params` method
    * Process Management with Graceful exit
    * Methods to manage subprocesses `self.subproc_handles` and `self.q_handles`

    """
    def __init__(
            self,
            health_status_port=None,
            force_terminate_on_stop=False,
            stop_subprocs_on_stop=False,
            join_subprocs_on_sigstop=True,
            clear_qs_on_stop=True,
            display_health_status_port=False,
            enable_video_on_status_port=False,
            *args, **kwargs
    ):
        """

        Parameters
        ----------
        health_status_port: int [Auto]
            port number at which subprocess health will be exposed.

            If not set, a free port will be requested from OS.

        force_terminate_on_stop: bool [False]
            Certain process run lengthy computations and need to be
            interrupted immediately rather than wait for main loop to
            perform shutdown_event check. Set this to True in such cases.
        stop_subprocs_on_stop: bool [False]
            If True, when stop is called, subprocesses present in self.sub_proc_handles
            will be sent the stop() command.

            Typical user sent SIGINT is received by
            all subprocesses and hence they will stop themselves. However, when
            `stop()` is called programatically from a different parent, it will
            need to be propagated to children.

            Default is False.
        join_subprocs_on_sigstop: bool [True]
            If `True`, process will wait for subprocesses in self.sub_proc_handles to
            close before closing itself. This value can be `False` when children
            are initialized with `daemon=True` argument

            *Warning*: Setting this to False could lead to orphaned child (zombie)
            processes unless they were initialized with `daemon=True`
        clear_qs_on_stop: bool [True]
            For clean exit, any queues that transfer data in/out of the
            process need to be flushed.

            If `True` it will clear all queues
            appended in the `self.q_handles` list.
        display_health_status_port: bool [False]
            Logs health status port. Useful if no port was specified.

        enable_video_on_status_port: bool [False]
            Registers and enables `/video` route that serves images in the preview_buffer to a browser using mjpg.
            If `True`, derived process must call `self.push_to_preview_buffer(cv2frame)` to send current frame for preview
            If `False`, the `/video` route is not registered

        kwargs

        Attributes
        ----------
        health_status_port: int
            port number at which subprocess health will be exposed
        shutdown_event: multiprocessing.Event
            An event that is SET when a kill/interrupt signal is received; Can be used to return from run() method
        q_handles: List[Queue]
            A list of handles to a multiprocessing manager Q object. Queue handles in this list are used to clear resp
            ective queues for graceful exit when a kill/interrupt signal is received.
        subproc_handles: List[KaiProcess]
            Subprocess Handles. A list of instances of KaiProcess or derived class(es) that implements

            * `start()`
            * `stop()`
            * `stop_subprocs()`
            * `join_subprocs()`
            * `clear_qs()`

            Adding subprocesses to this list enables automatic management of children with minimal effort. See
            `subproc*` initializer params for more info.

            **Warning**: Subprocesses that are added to this list (or in general) must be initialized inside `self.run()` method
            and not inside `__init__`.  Otherwise they will inherit the wrong parent!
        """
        super().__init__(*args, **kwargs)
        self.health_status_port = get_free_port(
        ) if health_status_port is None else health_status_port
        self.subprocess_health_status_ports = []
        self.status_params = {
            "status": "OK",
            "class": self.__class__.__name__,
        }
        self.shutdown_event = mp.Event()
        self.host = "0.0.0.0"
        if self.health_status_port > 0:
            self.health_status_server = StoppableWSGIRefServer(
                host=self.host, port=self.health_status_port)
        self.health_status_thread = None
        self.q_handles = []
        self.subproc_handles = []
        self.force_terminate_on_stop = force_terminate_on_stop
        self.clear_qs_on_stop = clear_qs_on_stop
        self.stop_subprocs_on_stop = stop_subprocs_on_stop
        self.join_subprocs_on_stop = join_subprocs_on_sigstop
        self.display_health_status_port = display_health_status_port
        self.enable_video_on_status_port = enable_video_on_status_port
        self.preview_frame = None

    def monitored_status_params(self) -> dict:
        """ retuns a dictionary with `key:value-` that can be monitored """
        raise NotImplementedError("Please define a method `monitored_status_params()` in class %s "
                                  "and return parameters that indicate health status of an "
                                  "instance of the class as a dict"
                                  %
                                  self.__class__.__name__)

    def unique_name(self) -> str:
        """ returns a unique string name to identify process instance """
        raise NotImplementedError("Please define a method `unique_name()` in class %s and return a unique name "
                                  "to identify an instance of it as string" %
                                  self.__class__.__name__)

    def signal_handler(self, signum, stack_frame):
        """ handles all signals. Use `signum == signal.SIG*` to implement custom signal handlers """

        if signum == signal.SIGINT:
            # lg.debug("Caught SIGINT; Exiting")
            self.__sig_stop()
            return

        raise SystemExit

    def _updated_status_params(self):
        return {**self.status_params, **(self.monitored_status_params()), **(self._get_subprocess_status())}

    def _get_subprocess_status(self):
        sub_status = {}
        for each_port in self.subprocess_health_status_ports:
            if each_port == -1:
                continue
            status = requests.get("http://%s:%s/" % (self.host, each_port))
            if status.status_code == 200:
                status = status.json()
            else:
                status = {"error": status.status_code}
            key = status[HSTATUS_KEY_UNIQUE_NAME]
            sub_status.update({key: status})

        return {"children": sub_status}

    def health_status_handler_additional_routes(self):
        """ define additional `@bottle` routes here. routes will be served at the `health_status_port` """
        pass

    def _health_status_handler(self):
        @bottle.get('/')
        def index():
            status = self._updated_status_params()
            return json.dumps(status)

        if self.enable_video_on_status_port:
            @bottle.get('/video')
            def feed():
                bottle.response.content_type='multipart/x-mixed-replace; boundary=frame'
                return iter(self._live_video_preview_frame_gen())

        self.health_status_handler_additional_routes()

        if self.display_health_status_port:
            lg.info("Service Check on port %d" % self.health_status_port)
        bottle.run(host=self.host, debug=False, quiet=True, server=self.health_status_server)

    def _init_sig_handlers(self):
        # register the signals to be caught
        signal.signal(signal.SIGHUP, self.signal_handler)
        signal.signal(signal.SIGINT, self.signal_handler)
        signal.signal(signal.SIGQUIT, self.signal_handler)
        signal.signal(signal.SIGILL, self.signal_handler)
        signal.signal(signal.SIGTRAP, self.signal_handler)
        signal.signal(signal.SIGABRT, self.signal_handler)
        signal.signal(signal.SIGBUS, self.signal_handler)
        signal.signal(signal.SIGFPE, self.signal_handler)
        # signal.signal(signal.SIGKILL, self.signal_handler)
        signal.signal(signal.SIGUSR1, self.signal_handler)
        signal.signal(signal.SIGSEGV, self.signal_handler)
        signal.signal(signal.SIGUSR2, self.signal_handler)
        signal.signal(signal.SIGPIPE, self.signal_handler)
        signal.signal(signal.SIGALRM, self.signal_handler)
        signal.signal(signal.SIGTERM, self.signal_handler)

    def run_and_wait_for_subprocs(self):
        """ starts `self.subproc_handles`, waits for `shutdown_event`, stops subprocs, waits for them to exit """
        self.run_subprocs()
        self.wait_for_shutdown_event()
        self.stop_subprocs()
        self.join_subprocs()

    def run_in_and_wait_for_subthread(self, callable, *args, **kwargs):
        threading.Thread(target=callable, args=args, kwargs=kwargs, daemon=True).start()
        self.wait_for_shutdown_event()

    def run_subprocs(self):
        """ runs processes in sub_proc_handles queue and waits for them to exit """
        for handler in self.subproc_handles:
            handler.start()
            self.subprocess_health_status_ports.append(handler.health_status_port)

    def wait_for_shutdown_event(self):
        """ blocks till int/kill signal or stop() call - whatever sets shutdown_event flag """
        while not self.shutdown_event.is_set():
            time.sleep(1)

        lg.info("## SHUTTING DOWN ##")

    def stop_subprocs(self):
        """ calls `stop()` on each subproc in `self.subproc_handles` """
        [handler.stop() for handler in self.subproc_handles]

    def join_subprocs(self):
        """ calls `join()` on each subproc in `self.subproc_handles`. if subproc is a daemon, joins with timeout of 2 seconds """
        # lg.debug("Waiting for subprocesses to stop")
        for handler in self.subproc_handles:
            lg.debug("Waiting for subproc: %s" % handler.unique_name())
            if handler.daemon:
                timeout = 2
            else:
                timeout = None
            handler.join(timeout)
        # lg.debug("done joining")

    def _live_video_preview_frame_gen(self):
        while not self.shutdown_event.is_set():
            try:
                frame_b = cv2.imencode('.jpg', self._get_from_preview_buffer())[1].tobytes()
                yield (b'--frame\r\n'
                       b'Content-Type: image/jpeg\r\n\r\n' + frame_b + b'\r\n')
            except Exception as e:
                lg.error(e)
                time.sleep(1)

    def push_to_preview_buffer(self, frame):
        """ Push frame to preview buffer that will be shown in the web video preview if enabled """
        self.preview_frame = frame

    def _get_from_preview_buffer(self):
        return self.preview_frame

    def run(self):
        if type(self.unique_name()) != str:
            raise TypeError("unique_name() should return an str object")

        self.status_params.update({
            HSTATUS_KEY_PID: os.getpid(),
            HSTATUS_KEY_UNIQUE_NAME: self.unique_name(),
            HSTATUS_KEY_PORT: self.health_status_port
        })

        if self.health_status_port > 0:
            self.health_status_thread = threading.Thread(
                target=self._health_status_handler, args=(), daemon=True)
            self.health_status_thread.start()
        self._init_sig_handlers()

    def stop(self):
        self.shutdown_event.set()
        os.kill(self.pid, signal.SIGINT)

    def __sig_stop(self):
        lg.debug("Shutdown Signal Received by %s:%s" %
                 (self.__class__.__name__, self.unique_name()))
        if self.shutdown_event.is_set():
            # parent called stop() programmatically
            if self.stop_subprocs_on_stop:
                lg.debug("Stopping subprocs")
                [h.stop() for h in self.subproc_handles]
        else:
            self.shutdown_event.set()
        if self.clear_qs_on_stop:
            self.clear_qs()
        try:
            self.health_status_server.stop()
        except AttributeError:
            pass

        if self.join_subprocs_on_stop:
            if len(self.subproc_handles) > 0:
                self.join_subprocs()
        if self.force_terminate_on_stop:
            lg.debug("Terminating Now")
            sys.exit()
        else:
            lg.debug("Shutdown Event set, wait to kill main thread...")

    def clear_qs(self):
        """ flushes each in que in `self.q_handles`"""
        for q_handle in self.q_handles:
            try:
                while True:
                    q_handle.get_nowait()
            except queue.Empty:
                continue
            except FileNotFoundError:
                continue
            except Exception as e:
                continue
        # lg.debug("finished clearing qs")
