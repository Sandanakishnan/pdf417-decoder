import flask


class WSGIBaseAPI(object):

    def __init__(self, websocket_enabled=False):
        """ Base API Class that implements a get_blueprint() method to return flask.Blueprint object """
        self.websocket_enabled = websocket_enabled

    def get_blueprint(self, *args, **kwargs) -> flask.Blueprint:
        """ return flask.Blueprint object with appropriate routes """
        raise NotImplementedError(
            "Please implement get_blueprint() static method and return a flask.Blueprint object with appropriate routes")
