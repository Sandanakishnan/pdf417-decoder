import base64
import logging

import cv2
import numpy as np
from flask import Blueprint, Response, render_template, request
from flask_socketio import emit 

from .base import WSGIBaseAPI


lg = logging.getLogger(__name__)


class FeedSettingsAPI(WSGIBaseAPI):

    def __init__(self, app_config, title="Settings", polycount=4, preview_callback=None):
        """ An API that serves a web ui for selecting ROI/Calibration points etc. from each camera feed

        Parameters
        ----------
        app_config: vap.config.AppConfig
            application configuration (used to list cameras)
        title : str
            title of the page
        polycount : int
            number of vertices of the polygon that the user can select
        preview_callback : callable(cv2image, data=list[{x:int,y:int}]) -> cv2image
            a callback function that accepts two arguments.

            1. The cv2image on which the user selected the polygon

            2. Polygon points that the user selected are passed as a list in data.
            Each list item is an dict with x & y as keys.

            The callable MUST return an opencv image which will be shown as preview to the user.
        """
        super().__init__(websocket_enabled=True)
        self.app_config = app_config
        self.title = title
        self.polycount = polycount
        self.preview_callback = preview_callback

        self.camfeeds = {}
        skip = 200
        for camid in self.app_config.inputs.keys():
            try:
                cap = cv2.VideoCapture(self.app_config.inputs[camid].src)
                ok, frame = cap.read()
                if not ok:
                    self.camfeeds[camid] = Response(
                        "Unable to open camera source", 500)
                    continue
                for x in range(skip):
                    ok, frame = cap.read()

                cap.release()
                r, jpg = cv2.imencode('.jpg', frame)
                self.camfeeds[camid] = Response(b'--frame\r\n' b'Content-Type: image/jpeg\r\n\r\n' + jpg.tobytes(
                ) + b'\r\n\r\n', mimetype='multipart/x-mixed-replace; boundary=frame')
            except:
                self.camfeeds[camid] = Response("Cam read error", 500)

    def _from_base64(self, base64_data):
        b64 = base64.b64decode(base64_data)
        nparr = np.fromstring(b64, np.uint8)
        return cv2.imdecode(nparr, cv2.IMREAD_ANYCOLOR)

    def get_blueprint(self, websocket_handle, apiroot='feedsetting'):

        title = self.title
        polycount = self.polycount
        preview_callback = self.preview_callback

        feedsettings_api = Blueprint(
            apiroot, __name__, template_folder="templates/feed_settings")
        feedsettings_api_socket = websocket_handle
        expose = feedsettings_api_socket.on

        camids = list(self.app_config.inputs.keys())

        @feedsettings_api.route("/")
        def index():
            return render_template("polySelector.html", pointmapper=True, camids=camids, apiroot=apiroot, title=title, polycount=polycount)

        @feedsettings_api.route('/camfeed/<camid>')
        def camfeed(camid):
            skip = request.args.get("skip", default=200)
            if camid in self.app_config.inputs.keys():
                return self.camfeeds[camid]
            else:
                return Response("camera not defined", 404)

        @expose('getMappedPoints', namespace=apiroot)
        def get_flattened_points(data):
            camid = data['camid']
            persp_points = self.app_config.inputs[camid].calibration.perspective_points
            flat_points = self.app_config.inputs[camid].calibration.flattened_points
            flat_points_dict = [{"x": x, "y": y} for x, y in flat_points]
            persp_points_dict = [{"x": x, "y": y} for x, y in persp_points]
            emit('refreshMappedPoints', {
                 'mappedPoints': flat_points_dict, 'perspectivePoints': persp_points_dict})

        @expose('updatePoints', namespace=apiroot)
        def update_points(data):
            # camid = data['camid']
            img64 = data['img']
            img = self._from_base64(img64)
            # src = self.app_config.inputs[camid].src
            prev_img = preview_callback(img, data)
            jpg = base64.b64encode(cv2.imencode('.jpg', prev_img)[1]).decode()
            emit('refresh', {'img': jpg})

        @expose('savePoints', namespace=apiroot)
        def save_points(data):
            try:
                persp_points = data['pts']
                flat_points = data['flatpts']
                camid = data['camid']

                persp_points = [[pt['x'], pt['y']] for pt in persp_points]
                flat_points = [[pt['x'], pt['y']] for pt in flat_points]

                self.app_config.inputs[camid].calibration.perspective_points = persp_points
                self.app_config.inputs[camid].calibration.flattened_points = flat_points

                self.app_config.save('testconfig.json')

                emit('savePointsCompleted', {'status': 'ok'})
            except Exception as e:
                emit('savePointsCompleted', {'status': 'failed'})

        return feedsettings_api
