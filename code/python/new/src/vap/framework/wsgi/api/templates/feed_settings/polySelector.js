
//globals
var maxCoordinatesListLength = {{ polycount }};
var perspCoordinatesListPtr = 0;
var perspCoordinatesList = Array.apply(null, Array({{ maxCoordinatesListLength }})).map(function () { return { x: 0, y: 0 } }); //[{x:0,y:0},{x:1,y:0},{x:1,y:1},{x:0,y:1}];
var mappedCoordinatesList = Array.apply(null, Array({{ maxCoordinatesListLength }})).map(function () { return { x: 0, y: 0 } });
var isDone = false;


// document.ready
document.addEventListener('DOMContentLoaded', function () {
    var elems = document.querySelectorAll('select');
    var instances = M.FormSelect.init(elems, {});


    selector = elems[0];
    selector.addEventListener('change', function () {
        var camid = selector.options[selector.selectedIndex].text
        var imgsrc = "{{apiroot}}/camfeed/" + camid;
        document.getElementById("initImage").setAttribute('src', imgsrc);
        document.getElementById("canvas").style.backgroundImage = 'url(' + imgsrc + ')';
        document.getElementById("calibImg").setAttribute('src', imgsrc)
        initPreviewCanvas();
        window.selCamId = camid;
        socket.emit('getMappedPoints', { 'camid': window.selCamId });
    });

    

});

//load all images
var imgs = document.images,
    len = imgs.length,
    counter = 0;

[].forEach.call( imgs, function( img ) {
    if(img.complete)
      incrementCounter();
    else
      img.addEventListener( 'load', incrementCounter, false );
} );

function incrementCounter() {
    counter++;
    if ( counter === len ) {
        console.log( 'All images loaded!' );
        initInputCanvasVariables();
        initInputCanvasSize();
        initPreviewCanvasSize();
        updateCoordinatesDisplay();
        recalcInputCanvasOffset();
        window.selCamId = "{{camids.0}}";
        initPreviewCanvas();
    }
}

// socket stuff
var socket = io.connect('http://' + document.domain + ':' + location.port + '{{apiroot}}');
// verify our websocket connection is established
socket.on('connect', function () {
    console.log('Websocket connected!');
    socket.emit('getMappedPoints', { 'camid': window.selCamId });
});

socket.on('refreshMappedPoints', function (msg) {
    mappedCoordinatesList = msg['mappedPoints'];
    perspCoordinatesList = msg['perspectivePoints'];
    updateCoordinatesDisplay();
    drawPolygonInputCanvas();
});

socket.on('refresh', function (msg) {
    b64image = 'data:image/png;base64, ' + msg['img'];
    $("#calibImg").attr('src', b64image);
    gkhead.src = b64image;
    redrawPreviewCanvas();
    $('.progress').css("visibility", "hidden");

})

socket.on('savePointsCompleted', function (msg) {
    if (msg['status'] == 'ok') {
        status = "save was successul"
    } else {
        status = "save failed! check log for details"
    }
    M.toast({ html: status });
})




var inputCanvas, inputCanvasCtx, offsetX, offsetY, cw, ch;

function initInputCanvasVariables(){
    inputCanvas = document.getElementById("canvas");
    inputCanvasCtx = inputCanvas.getContext("2d");
    inputCanvasCtx.setTransform(1, 0, 0, 1, 0, 0);
    
    cw = inputCanvas.width;
    ch = inputCanvas.height;

    recalcInputCanvasOffset();

    resetInputCanvasCtx();
}


function recalcInputCanvasOffset() {
    var BB = inputCanvas.getBoundingClientRect();
    offsetX = BB.left;
    offsetY = BB.top;
}
window.onscroll = function (e) { recalcInputCanvasOffset(); }


function initPreviewCanvasSize() {
    cid = '#preview-canvas';
    imid = '#calibImg';
    var img = $(imid);
    img.css('display', '');
    width = img.width();
    height = img.height();
    console.log(width, height);
    var canvas = $(cid);
    canvas.css('width', '');
    canvas.css('height', '');
    canvas.css('display', '');
    canvas[0].width = width;
    canvas[0].height = height;
    img.css('display', 'none');
}

function initInputCanvasSize() {
    cid = '#canvas';
    imid = '#initImage';
    var img = $(imid);
    img.css('display', '');
    width = img.width();
    height = img.height();
    console.log(width, height);
    var canvas = $(cid);
    canvas.css('width', '');
    canvas.css('height', '');
    canvas.css('display', '');
    canvas.css('background-image', 'url("{{ apiroot }}/camfeed/{{camids.0}}"');
    canvas[0].width = width;
    canvas[0].height = height;
    img.css('display', 'none');
}

function resetInputCanvasCtx() {
    inputCanvasCtx.lineWidth = 2;
    inputCanvasCtx.strokeStyle = 'yellow';
    inputCanvasCtx.font = "30px Arial";
}

function genCalibrationPreview() {
    perspCoordinatesList_ = perspCoordinatesList.map(c => { return { x: c.x / cw, y: c.y / ch } });
    newCanvas = document.createElement("canvas");
    srcImage = document.getElementById('initImage');
    var ctx = newCanvas.getContext("2d");
    newCanvas.width = inputCanvas.width;
    newCanvas.height = inputCanvas.height;
    ctx.drawImage(srcImage, 0, 0, newCanvas.width, newCanvas.height);
    ctx.drawImage(inputCanvas, 0, 0, newCanvas.width, newCanvas.height);
    camimg = newCanvas.toDataURL("image/jpeg").split(",")[1].trim();
    socket.emit('updatePoints', { 'img': camimg, 'camid': window.selCamId, 'pts': perspCoordinatesList_, 'flatpts': mappedCoordinatesList });
    $('.progress').css("visibility", "visible");
    // document.removeChild(newCanvas);
}

function saveCalibrationPoints() {
    perspCoordinatesList_ = perspCoordinatesList.map(c => { return { x: c.x, y: c.y } });
    socket.emit('savePoints', { 'camid': window.selCamId, 'pts': perspCoordinatesList_, 'flatpts': mappedCoordinatesList });
}

function updateCoordinatesListOnUserMouseInput(pts) {
    perspCoordinatesList[perspCoordinatesListPtr] = pts
    updateCoordinatesDisplay();
}

function updateMappedCoordinatesListOnUserKbInput(e) {
    i = this.getAttribute("pt");
    xy = this.getAttribute("xy");
    mappedCoordinatesList[i][xy] = parseInt(this.value);
}

function updateCoordinatesDisplay() {
    {%if pointmapper == false %}

    var list = document.getElementById('polycoordinates_ul');
    list.innerHTML = "";
    for (var i = 0; i < perspCoordinatesList.length; i++) {
        // Create the list item:
        var item = document.createElement('li');
        item.className = "collection-item";
        // Set its contents:
        item.appendChild(document.createTextNode("Pt " + i.toString() + ": " +
            "{x:" + perspCoordinatesList[i].x + ", y:" + perspCoordinatesList[i].y + "}"));

        // Add it to the list:
        list.appendChild(item);
    }

    {% else %}
    var tbody = document.getElementById('polycoordinates_tbody');
    tbody.innerHTML = "";
    for (var i = 0; i < perspCoordinatesList.length; i++) {
        var row = document.createElement('tr');
        var ptcol = document.createElement('td');
        ptcol.appendChild(document.createTextNode("Pt " + (i + 1).toString()));
        var polycol = document.createElement('td');
        polycol.appendChild(document.createTextNode("{x:" + perspCoordinatesList[i].x + ", y:" + perspCoordinatesList[i].y + "}"));
        var rectcol = document.createElement('td');
        rectcolrowx = document.createElement('div');
        rectcolrowx.className = "col s3"
        rectcolrowy = document.createElement('div');
        rectcolrowy.className = "col s3"

        txtx = document.createTextNode("x:");
        txty = document.createTextNode("y:");
        rectcolvalx = document.createElement('input');
        rectcolvalx.type = 'text';
        rectcolvalx.id = 'mapped_pt_x_' + i.toString();
        rectcolvalx.setAttribute("value", mappedCoordinatesList[i].x);
        rectcolvalx.setAttribute("pt", i);
        rectcolvalx.setAttribute("xy", "x");
        rectcolvalx.addEventListener("change", updateMappedCoordinatesListOnUserKbInput);

        rectcolvaly = document.createElement('input');
        rectcolvaly.type = 'text';
        rectcolvaly.id = 'mapped_pt_y_' + i.toString();
        rectcolvaly.setAttribute("value", mappedCoordinatesList[i].y);
        rectcolvaly.setAttribute("pt", i);
        rectcolvaly.setAttribute("xy", "y");
        rectcolvaly.addEventListener("change", updateMappedCoordinatesListOnUserKbInput);

        rectcolrowx.appendChild(txtx);
        rectcolrowx.appendChild(rectcolvalx);
        rectcolrowy.appendChild(txty);
        rectcolrowy.appendChild(rectcolvaly);
        rectcol.appendChild(rectcolrowx);
        rectcol.appendChild(rectcolrowy);


        row.appendChild(ptcol);
        row.appendChild(polycol);
        row.appendChild(rectcol);

        tbody.appendChild(row);
    }

    {% endif %}
};

$('#done').click(function () {
    isDone = true;
});


document.addEventListener("keydown", keyDown, false);
$("#canvas").mousedown(function (e) { handleMouseDown(e); });
$("#canvas").mousemove(function (e) { handleMouseMove(e); });

function handleMouseMove(e) {
    // tell the browser we're handling this event
    e.preventDefault();
    e.stopPropagation();
    mouseX = parseInt(e.clientX - offsetX);
    mouseY = parseInt(e.clientY - offsetY);
};

function keyDown(e) {
    key = -1;
    if (e.keyCode >= 48 && e.keyCode <= 57) {
        key = e.keyCode - 48;
    }
    if (e.keyCode >= 96 && e.keyCode <= 105) {
        key = e.keyCode - 96;
    }
    if ((key > 0) && (key <= 4)) {
        perspCoordinatesListPtr = key - 1;
        drawPolygonInputCanvas();

    }
}

function handleMouseDown(e) {
    if (isDone || perspCoordinatesList.length > 10) { return; }

    // tell the browser we're handling this event
    e.preventDefault();
    e.stopPropagation();

    mouseX = parseInt(e.clientX - offsetX);
    mouseY = parseInt(e.clientY - offsetY);
    updateCoordinatesListOnUserMouseInput({ x: mouseX, y: mouseY });
    drawPolygonInputCanvas();
}

function putTextInputCanvas(is_sel, txt, pt) {
    x = pt.x;
    y = pt.y;
    ctx = inputCanvasCtx;
    ctx.strokeStyle = 'black';
    ctx.lineWidth = 2;
    ctx.strokeText(txt, x, y);
    if (is_sel) {
        ctx.fillStyle = "red";
    } else {
        ctx.fillStyle = "green";
    }
    ctx.fillText(txt, pt.x, pt.y);
    resetInputCanvasCtx();

}

function drawPolygonInputCanvas() {
    labels = ["1", "2", "3", "4"]
    inputCanvasCtx.clearRect(0, 0, cw, ch);
    inputCanvasCtx.beginPath();
    inputCanvasCtx.moveTo(perspCoordinatesList[0].x, perspCoordinatesList[0].y);
    index = 0;
    putTextInputCanvas(perspCoordinatesListPtr == index, labels[index], perspCoordinatesList[index]);

    for (index = 1; index < perspCoordinatesList.length; index++) {
        inputCanvasCtx.lineTo(perspCoordinatesList[index].x, perspCoordinatesList[index].y);
        putTextInputCanvas(perspCoordinatesListPtr == index, labels[index], perspCoordinatesList[index]);
    }
    inputCanvasCtx.closePath();
    inputCanvasCtx.stroke();
}


var gkhead;
// gkhead.src = '{{ apiroot }}/camfeed/{{camids.0}}';

function redrawPreviewCanvas() {
    var canvas = document.getElementById('preview-canvas');
    var ctx = canvas.getContext('2d');

    // Clear the entire canvas
    var p1 = ctx.transformedPoint(0, 0);
    var p2 = ctx.transformedPoint(canvas.width, canvas.height);
    ctx.clearRect(p1.x, p1.y, p2.x - p1.x, p2.y - p1.y);

    ctx.save();
    ctx.setTransform(1, 0, 0, 1, 0, 0);
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.restore();

    ctx.drawImage(gkhead, 0, 0);

}

function initPreviewCanvas() {
    gkhead = new Image;
    var canvas = document.getElementById('preview-canvas');

    window.onload = function () {

        var ctx = canvas.getContext('2d');
        trackTransforms(ctx);

        redrawPreviewCanvas();

        var lastX = canvas.width / 2, lastY = canvas.height / 2;

        var dragStart, dragged;

        canvas.addEventListener('mousedown', function (evt) {
            document.body.style.mozUserSelect = document.body.style.webkitUserSelect = document.body.style.userSelect = 'none';
            lastX = evt.offsetX || (evt.pageX - canvas.offsetLeft);
            lastY = evt.offsetY || (evt.pageY - canvas.offsetTop);
            dragStart = ctx.transformedPoint(lastX, lastY);
            dragged = false;
        }, false);

        canvas.addEventListener('mousemove', function (evt) {
            lastX = evt.offsetX || (evt.pageX - canvas.offsetLeft);
            lastY = evt.offsetY || (evt.pageY - canvas.offsetTop);
            dragged = true;
            if (dragStart) {
                var pt = ctx.transformedPoint(lastX, lastY);
                ctx.translate(pt.x - dragStart.x, pt.y - dragStart.y);
                redrawPreviewCanvas();
            }
        }, false);

        canvas.addEventListener('mouseup', function (evt) {
            dragStart = null;
            if (!dragged) zoom(evt.shiftKey ? -1 : 1);
        }, false);

        var scaleFactor = 1.1;

        var zoom = function (clicks) {
            var pt = ctx.transformedPoint(lastX, lastY);
            ctx.translate(pt.x, pt.y);
            var factor = Math.pow(scaleFactor, clicks);
            ctx.scale(factor, factor);
            ctx.translate(-pt.x, -pt.y);
            redrawPreviewCanvas();
        }

        var handleScroll = function (evt) {
            var delta = evt.wheelDelta ? evt.wheelDelta / 40 : evt.detail ? -evt.detail : 0;
            if (delta) zoom(delta);
            return evt.preventDefault() && false;
        };

        canvas.addEventListener('DOMMouseScroll', handleScroll, false);
        canvas.addEventListener('mousewheel', handleScroll, false);
    };

    gkhead.src = '{{ apiroot }}/camfeed/{{camids.0}}';

    // Adds ctx.getTransform() - returns an SVGMatrix
    // Adds ctx.transformedPoint(x,y) - returns an SVGPoint
    function trackTransforms(ctx) {
        var svg = document.createElementNS("http://www.w3.org/2000/svg", 'svg');
        var xform = svg.createSVGMatrix();
        ctx.getTransform = function () { return xform; };

        var savedTransforms = [];
        var save = ctx.save;
        ctx.save = function () {
            savedTransforms.push(xform.translate(0, 0));
            return save.call(ctx);
        };

        var restore = ctx.restore;
        ctx.restore = function () {
            xform = savedTransforms.pop();
            return restore.call(ctx);
        };

        var scale = ctx.scale;
        ctx.scale = function (sx, sy) {
            xform = xform.scaleNonUniform(sx, sy);
            return scale.call(ctx, sx, sy);
        };

        var rotate = ctx.rotate;
        ctx.rotate = function (radians) {
            xform = xform.rotate(radians * 180 / Math.PI);
            return rotate.call(ctx, radians);
        };

        var translate = ctx.translate;
        ctx.translate = function (dx, dy) {
            xform = xform.translate(dx, dy);
            return translate.call(ctx, dx, dy);
        };

        var transform = ctx.transform;
        ctx.transform = function (a, b, c, d, e, f) {
            var m2 = svg.createSVGMatrix();
            m2.a = a; m2.b = b; m2.c = c; m2.d = d; m2.e = e; m2.f = f;
            xform = xform.multiply(m2);
            return transform.call(ctx, a, b, c, d, e, f);
        };

        var setTransform = ctx.setTransform;
        ctx.setTransform = function (a, b, c, d, e, f) {
            xform.a = a;
            xform.b = b;
            xform.c = c;
            xform.d = d;
            xform.e = e;
            xform.f = f;
            return setTransform.call(ctx, a, b, c, d, e, f);
        };

        var pt = svg.createSVGPoint();
        ctx.transformedPoint = function (x, y) {
            pt.x = x; pt.y = y;
            return pt.matrixTransform(xform.inverse());
        }
    }
}
