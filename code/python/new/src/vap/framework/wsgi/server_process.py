
import logging
import threading
import time

from vap.framework.kai_process import KaiProcess, run_and_wait

from .server import WSGIApiServer
from .api.multimedia import MultiMediaAPI

logging.getLogger('socketio').setLevel(logging.ERROR)


class WSGIApiServerProcess(KaiProcess):

    def __init__(
        self,
        host="localhost",
        port=9090,
        blueprints={
            '': MultiMediaAPI(
                image_feed=True
                ),
            }
        ):
        """ A Flask Server that runs in a separate process
        
        Parameters
        ----------
        host: str 
            hostname (default: localhost)
        port: int
            port number
        blueprints: dict
            a dictionary of path:flask.Blueprint instances.  Check wsgi.server documentation for details
        """
        super().__init__(
            health_status_port=-1
        )

        self.ws = WSGIApiServer(
            host=host,
            port=port,
            blueprints=blueprints
        )

    def unique_name(self):
        return "WebServer"

    def monitored_status_params(self):
        return {"status": "OK"}

    def run(self):
        super().run()
        threading.Thread(target=self.ws.run, daemon=True).start()

        while not self.shutdown_event.is_set():
            time.sleep(1)


if __name__ == "__main__":
    wsp = WSGIApiServerProcess()
    run_and_wait(wsp)
