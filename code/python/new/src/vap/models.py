"""
Template Classes for instantiating and serving exported CNN models
"""

import cv2
import tensorflow as tf 
import numpy as np
from abc import abstractmethod
from sklearn import neighbors, svm
import time
import pickle
from sklearn.cluster import KMeans

from . import base
from . import config
from . import predictions

class CNNEstimator(base.CNNEstimatorBase):
    """ CNN Estimator """
    def __init__(self, config):
        super(CNNEstimator, self).__init__(config)
        self._config = config

    @abstractmethod
    def predict(self):
        pass

class BBoxEstimatorBase(base.CNNEstimatorBase):
    """ Bounding Box Model class that returns bounding boxes """
    def __init__(self):
        super(BBoxEstimatorBase, self).__init__()

class MaskEstimatorBase(base.CNNEstimatorBase):
    """ Mask Detection Model class that returns binary masks """
    def __init__(self):
        super(MaskEstimatorBase, self).__init__()

class ClassificationEstimatorBase(base.CNNEstimatorBase):
    """ Classifier Model class that returns classfication outputs """
    def __init__(self):
        super(ClassificationEstimatorBase, self).__init__()

class DenseEstimatorBase(base.CNNEstimatorBase):
    """ Dense Model class that returns dense layer outputs (eg. landmarks) """
    def __init__(self):
        super(DenseEstimatorBase, self).__init__()

class TFModel(CNNEstimator):
    """ Tensorflow DL Model """
    def __init__(self, config: config.TFModelConfig):
        """
        Tensorflow Deep learning model loader

        Parameters
        ----------
        config: vap.config.TFModelConfig

        """
        super(TFModel, self).__init__(config)

        self._config = config

        with tf.gfile.GFile(self._config.model_path, 'rb') as f:
            self.graph_def = tf.GraphDef()
            self.graph_def.ParseFromString(f.read())

        self.graph = tf.Graph()
        with self.graph.as_default():
            tf.import_graph_def(self.graph_def, name=config.import_name)

        self._input = [self.graph.get_tensor_by_name(tensor_name) for tensor_name in self._config.input_tensors]
        self._output_ops = [self.graph.get_tensor_by_name(tensor_name) for tensor_name in self._config.output_tensors]

        self.gpu_options = tf.GPUOptions(
            allow_growth=self._config.gpu_allow_growth,
            per_process_gpu_memory_fraction=self._config.gpu_memory_fraction,
            visible_device_list=self._config.cuda_visible_device_list
        )
        self._config_proto = tf.ConfigProto(gpu_options=self.gpu_options, log_device_placement=False)
        self.sess = tf.Session(graph=self.graph, config=self._config_proto)

    @property
    def input_tensor(self):
        """ returns tf tensor object corresponding to input """
        return self._input
    
    @property
    def output_tensors(self):
        """ returns list of tf tensor objects corresponding to output """
        return self._output_ops

    def predict(self, *args):
        """
        Performs inference by feeding input and returns output

        Parameters
        ----------
        args : tuple
            Tuple corresponding to each input-tensor defined in model configuration

        Returns
        -------
        Raw output of one forward pass of the TF Graph

        """
        feed_dict = {}
        for inp, val in zip(self._input, args):
            feed_dict[inp] = val
        outputs = self.sess.run(
            self._output_ops, feed_dict=feed_dict
        )
        return outputs

class CVModel(CNNEstimator):
    """ OpenCV DL Model """
    def __init__(self, config: config.CVModelConfig):
        """
        OpenCV Deep learning model loader

        Parameters
        ----------
        config: vap.config.CVModelConfig

        """
        super(CVModel, self).__init__(config)

        self._config = config
        self.net = cv2.dnn.readNetFromCaffe(self._config._proto_path, self._config.model_path)
        

    @property
    def config(self):
        return self._config
    

    def predict(self, blob, scale=1., score_threshold=0.5):
        """
        Performs inference by feeding input and returns output

        Parameters
        ----------
        args : tuple
            Tuple corresponding to each input-tensor defined in model configuration

        Returns
        -------
        Raw output of one forward pass of the TF Graph

        """
        
        self.net.setInput(blob)
        outputs = self.net.forward()

        return outputs




class CFModel(CNNEstimator):
    """ Caffe2 Framework Model """
    def __init__(self, config):
        raise NotImplementedError()


class TFMultiClassBBoxDetector(TFModel):
    """ Tensorflow Multi Class Object Detector Base Class """
    def __init__(self, config: config.TFModelConfig, classnames_dict: dict):
        """
        WIP

        Parameters
        ----------
        config : vap.config.TFModelConfig
        classnames_dict : dict
            Dictionary of classnames
        """
        super(TFMultiClassBBoxDetector, self).__init__(config)
        self._config = config 
        self._classnames_dict = classnames_dict    

    @property
    def classname(self):
        return self._classnames_dict

    @property
    def config(self):
        return self._config


    def predict(self, input, scale=1., score_threshold=0.6):
        h, w, _ = input.shape
        sh = int(h*scale)
        sw = int(w*scale)
        image = cv2.resize(input, (sw, sh))
        image = np.expand_dims(image, 0)

        boxes, scores, classes, num_boxes = super().predict(image)
        num_boxes = int(num_boxes[0])
        boxes = boxes[0][:num_boxes]
        classes = classes[0][:num_boxes]
        scores = scores[0][:num_boxes]

        to_keep = scores > score_threshold
        boxes = boxes[to_keep]
        scores = scores[to_keep]
        classes = classes[to_keep]

        scaler = np.array([h, w, h, w], dtype='float32')
        boxes = boxes * scaler

        return [predictions.BBoxPrediction.from_TFModel(box, self.classname[classid], score, h, w) for (box, classid, score) in zip(boxes, classes, scores)]


class TFSingleClassBBoxDetector(TFModel):
    """ Tensorflow Single Class Object Detector Class"""
    def __init__(self, config: config.TFModelConfig, classname: str):
        """

        Parameters
        ----------
        config : vap.config.TFModelConfig
            TFModel configuration
        classname : str
            String specifying classname of the detected object
        """
        super(TFSingleClassBBoxDetector, self).__init__(config)
        self._config = config
        self._classname = classname 

    @property
    def classname(self):
        return self._classname

    @property
    def config(self):
        return self._config
        
    def predict(self, input, scale=1., score_threshold=0.6, normalized=True):
        """
        Perform inference on input image

        Parameters
        ----------
        input : ndarray
            numpy array representing image
        scale : float
            factor by which to resize image before inference
        score_threshold : float
            discard detections with confidence less than this
        normalized : boolean
            returned `vap.predictions.classes.BBoxPrediction` will have `vap.predictions.primitives.BBox` with normalized coordinates

        Returns
        -------
        List [ vap.predictions.classes.BBoxPrediction ]
        """
        h, w, _ = input.shape
        sh = int(h*scale) 
        sw = int(w*scale)
        image = cv2.resize(input, (sw, sh))
        image = np.expand_dims(image, 0)

        boxes, scores, num_boxes = super().predict(image)
        num_boxes = num_boxes[0]
        boxes = boxes[0][:num_boxes]
        scores = scores[0][:num_boxes]

        to_keep = scores > score_threshold
        boxes = boxes[to_keep]
        scores = scores[to_keep]

        if not normalized:
            scaler = np.array([h, w, h, w], dtype='float32')
            boxes = boxes * scaler

        return [predictions.BBoxPrediction.from_TFModel(box, self.classname, score, h, w) for (box, score) in zip(boxes, scores)]

class CVSingleClassBBoxDetector(CVModel):
    """ Tensorflow Single Class Object Detector Class"""
    def __init__(self, config: config.CVModelConfig, classname: str):
        """

        Parameters
        ----------
        config : vap.config.CVModelConfig
            TFModel configuration
        classname : str
            String specifying classname of the detected object
        """
        super(CVSingleClassBBoxDetector, self).__init__(config)
        self._config = config
        self._classname = classname 

    @property
    def classname(self):
        return self._classname

    @property
    def config(self):
        return self._config
        
    def predict(self, input, scale=1., score_threshold=0.6, normalized=True):
        """
        Perform inference on input image

        Parameters
        ----------
        input : ndarray
            numpy array representing image
        s_size : tuple
            size of the scaled down image before inference
        score_threshold : float
            discard detections with confidence less than this
        normalized : boolean
            returned `vap.predictions.classes.BBoxPrediction` will have `vap.predictions.primitives.BBox` with normalized coordinates

        Returns
        -------
        List [ vap.predictions.classes.BBoxPrediction ]
        """
        h, w, _ = input.shape
        sh = int(h*scale) 
        sw = int(w*scale)
        image = cv2.resize(input, (sw, sh))
        blob = cv2.dnn.blobFromImage(image, 1.0, (sh, sw), (104.0, 177.0, 123.0))

        outputs = super().predict(blob)
        scores = [outputs[0, 0, i, 2] for i in range(len(outputs))]
        boxes = [outputs[0, 0, i, 3:7] * np.array([w, h, w, h]) for i in range(len(outputs))]
        boxes = [box.astype('int') for box in boxes]

        boxes, scores = np.array(boxes), np.array(scores)

        num_boxes = len(boxes)

        to_keep = scores > score_threshold
        boxes = boxes[to_keep]
        scores = scores[to_keep]

        return [predictions.BBoxPrediction.from_TFModel(box, self.classname, score, h, w) for (box, score) in zip(boxes, scores)]

class TFFaceEmbeddingsExtractor(TFModel):
    """ Tensorflow Face Embeddings Extractor :AD:"""
    def __init__(self, config: config.TFModelConfig):
        super(TFFaceEmbeddingsExtractor, self).__init__(config)
        
        self._config = config
        
    def predict(self, image, face_describer_tensor_shape = (112, 112)):
        """ returns encoding for given image_path """
        image = cv2.resize(image, face_describer_tensor_shape)
        image = np.expand_dims(image, 0)
        embeddings = super().predict(image, 0.1)

        return embeddings

        


class MLEstimator(base.MLEstimatorBase):
    """ ML Estimator :AD:"""
    def __init__(self):
        super(MLEstimator, self).__init__(base.MLEstimatorConfigBase())
#        self._config = config

class SKLearnModel(MLEstimator):
    """ SciKit Learn Base Class :AD: """
    def __init__(self):
        super(SKLearnModel, self).__init__()
#        self._config = config
    
class KnnModel(SKLearnModel):
    """ SciKit Learn Base Class :AD: """
    def __init__(self, n_neighbors = 2, algorithm='ball_tree', weights='distance'):
        super(KnnModel, self).__init__()
        self.n_neighbors = n_neighbors
        self.algorithm = algorithm
        self.weights = weights
        self.knn_clf = neighbors.KNeighborsClassifier(n_neighbors=self.n_neighbors, algorithm=self.algorithm, weights=self.weights)
    
    def train(self, X, y):
        """ Training a KNN model :AD: """
        self.knn_clf.fit(X,y)
        return self.knn_clf
    
    def save_model(self, model_name = None, class_number = 999):
        """Model Name = model_name-class_number-time_stamp.clf :AD:"""
        if model_name is None:
            model_name = "knnmodel"+"-"+str(class_number)+"-"+time.strftime("%Y%m%d-%H%M%S", time.localtime())+".clf"
#            print("Model Name is not defined: Model is saving with name:", model_name)
        else:
            model_name = model_name+"-"+str(class_number)+"-"+time.strftime("%Y%m%d-%H%M")+".clf"
#            print("Model is saving with name:", model_name)

        with open(model_name, 'wb') as f:
            pickle.dump(self.knn_clf, f)
        return model_name
       
#        print("Model Saved Sucessfully")

class SVMModel(SKLearnModel):
    """ SciKit Learn Base Class :AD: """
    def __init__(self, gamma=0.001, C=100.):
        super(SVMModel, self).__init__()
        self.gamma = gamma
        self.C = C
        self.svm_clf = svm.SVC(gamma=self.gamma, C=self.C)
    
    def train(self, X, y):
        """ Training a KNN model :AD: """
        self.svm_clf.fit(X,y)
        return self.svm_clf
    
    def save_model(self, model_name = None, class_number = 999):
        """Model Name = model_name-class_number-time_stamp.clf :AD:"""
        if model_name is None:
            model_name = "svmmodel"+"-"+str(class_number)+"-"+time.strftime("%Y%m%d-%H%M%S")+".clf"
        else:
            model_name = model_name+"-"+str(class_number)+"-"+time.strftime("%Y%m%d-%H%M%S")+".clf"
        with open(model_name, 'wb') as f:
            pickle.dump(self.svm_clf, f)
        
class KMeansModel(SKLearnModel):
    """ SciKit Learn Base Class :AD: """
    def __init__(self, n_clusters=None):
        super(KMeansModel, self).__init__()
        if n_clusters is None:
            raise ValueError("n_clusters value is undefined")
        else:
            self.n_clusters = n_clusters
        self.kmeans_clu = KMeans(n_clusters = n_clusters, max_iter=300, tol=1e-4, n_init=10, precompute_distances='auto')
    
    def train(self, X):
        """ Training a KNN model :AD: """
        self.kmeans_clu.fit(X)
        return self.kmeans_clu
    
    def save_model(self, model_name = None, class_number = 999):
        """Model Name = model_name-class_number-time_stamp.clf :AD:"""
        if model_name is None:
            model_name = "kmeansmodel"+"-"+str(class_number)+"-"+time.strftime("%Y%m%d-%H%M%S")+".clf"
        else:
            model_name = model_name+"-"+str(class_number)+"-"+time.strftime("%Y%m%d-%H%M%S")+".clf"
        with open(model_name, 'wb') as f:
            pickle.dump(self.kmeans_clu, f)
           
        