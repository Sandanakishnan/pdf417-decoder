"""
DeepSORT Algorithm based tracker; Check `core` for documentation

Usage
-----

```
from vap.modules.deepsort_tracker import DeepsortTracker

def track_deletion_callback(track)
    lg.info('deleted Trk-%s'%track.track_id)

def track_confirm_callback(track)
    lg.info('confirmed Trk-%s'%track.track_id)

tracker = DeepSortTracker(
            "path-to-mars-small128.pb",
            track_deletion_callback=track_deletion_callback,
            track_confirmation_tick_callback=track_confirm_callback,
            metric="cosine",
            max_match_distance=0.2,
            max_age=10
        )

detections = my-detector.detect(image)
tracked_detections = tracker.update(image, detections)

for d in tracked_detections:
    print(d.info.track.track_id)

```

"""
from . core import *

