import numpy as np
from typing import List
from collections import deque
import itertools
import cv2

from ... import base
from . import nn_matching
# from . import track
from . import kalman_filter
from . import linear_assignment
from . import iou_matching
from ...predictions import BBoxPrediction
from ...models import TFModel
from ...config import TFModelConfig
from ...utils.image_utils import ImageUtil


class DeepSortTrackerModuleBase(base.VAPModuleBase):
    """ Ultimate base class for all DeepSortTracker modules """
    pass


class sliceable_deque(deque):
    def __getitem__(self, index):
        if isinstance(index, slice):
            if index.start < 0:
                idx = max(0, self.__len__() - abs(index.start))
            else:
                idx = index.start

            return type(self)(itertools.islice(self, idx,
                                               index.stop, index.step))
        return deque.__getitem__(self, index)


class TrackState(DeepSortTrackerModuleBase):
    """
    Enumeration type for the single target track state. Newly created tracks are
    classified as `tentative` until enough evidence has been collected. Then,
    the track state is changed to `confirmed`. Tracks that are no longer alive
    are classified as `deleted` to mark them for removal from the set of active
    tracks.

    """

    Tentative = 1
    Confirmed = 2
    Deleted = 3


class Track(DeepSortTrackerModuleBase):
    """
    A single target track with state space `(x, y, a, h)` and associated
    velocities, where `(x, y)` is the center of the bounding box, `a` is the
    aspect ratio and `h` is the height.

    Parameters
    ----------
    mean : ndarray
        Mean vector of the initial state distribution.
    covariance : ndarray
        Covariance matrix of the initial state distribution.
    track_id : int
        A unique track identifier.
    n_init : int
        Number of consecutive detections before the track is confirmed. The
        track state is set to `Deleted` if a miss occurs within the first
        `n_init` frames.
    max_age : int
        The maximum number of consecutive misses before the track state is
        set to `Deleted`.
    feature : Optional[ndarray]
        Feature vector of the detection this track originates from. If not None,
        this feature is added to the `features` cache.

    Attributes
    ----------
    mean : ndarray
        Mean vector of the initial state distribution.
    covariance : ndarray
        Covariance matrix of the initial state distribution.
    track_id : int
        A unique track identifier.
    hits : int
        Total number of measurement updates.
    age : int
        Total number of frames since first occurance.
    time_since_update : int
        Total number of frames since last measurement update.
    state : TrackState
        The current track state.
    features : List[ndarray]
        A cache of features. On each measurement update, the associated feature
        vector is added to this list.

    """

    def __init__(self, mean, covariance, track_id, n_init, max_age,
                 feature=None, hist_len=20):
        self.mean = mean
        self.covariance = covariance
        self.track_id = track_id
        self.hits = 1
        self.age = 1
        self.confirmed_age = 0
        self.time_since_update = 0

        self.state = TrackState.Tentative
        self.features = []
        self.mean_history = sliceable_deque(maxlen=hist_len)

        if feature is not None:
            self.features.append(feature)

        self._n_init = n_init
        self._max_age = max_age

    @property
    def is_stationary(self):
        prev_c = [0, 0]
        distances = np.array([])
        for h in self.mean_history:
            curr_c = h[:2]
            dist = abs(np.linalg.norm(prev_c - curr_c))
            prev_c = curr_c
            np.append(distances, dist, axis=-1)

        mean_distance = np.mean(distances)
        # print(mean_distance)
        return mean_distance < 10

    def to_tlwh(self):
        """Get current position in bounding box format `(top left x, top left y,
        width, height)`.

        Returns
        -------
        ndarray
            The bounding box.

        """
        ret = self.mean[:4].copy()
        ret[2] *= ret[3]
        ret[:2] -= ret[2:] / 2
        return ret

    def to_tlbr(self):
        """Get current position in bounding box format `(min x, miny, max x,
        max y)`.

        Returns
        -------
        ndarray
            The bounding box.

        """
        ret = self.to_tlwh()
        ret[2:] = ret[:2] + ret[2:]
        return ret

    def predict(self, kf):
        """Propagate the state distribution to the current time step using a
        Kalman filter prediction step.

        Parameters
        ----------
        kf : kalman_filter.KalmanFilter
            The Kalman filter.

        """
        self.mean, self.covariance = kf.predict(self.mean, self.covariance)
        self.age += 1
        if self.state == TrackState.Confirmed:
            self.confirmed_age += 1
        self.time_since_update += 1

    def update(self, kf, detection):
        """Perform Kalman filter measurement update step and update the feature
        cache.

        Parameters
        ----------
        kf : kalman_filter.KalmanFilter
            The Kalman filter.
        detection : Detection
            The associated detection.

        """
        self.mean, self.covariance = kf.update(
            self.mean, self.covariance, detection.bbox_asis.xyah)
        self.features.append(detection.info.feature)

        self.hits += 1
        self.time_since_update = 0
        self.mean_history.append(self.mean)

        if self.state == TrackState.Tentative and self.hits >= self._n_init:
            self.state = TrackState.Confirmed

    def mark_missed(self):
        """Mark this track as missed (no association at the current time step).
        """
        if self.state == TrackState.Tentative:
            self.state = TrackState.Deleted
        elif self.time_since_update > self._max_age:
            self.state = TrackState.Deleted

    def mark_deleted(self):
        """Mark this track as deleted"""
        self.state = TrackState.Deleted

    def is_tentative(self):
        """Returns True if this track is tentative (unconfirmed).
        """
        return self.state == TrackState.Tentative

    def is_confirmed(self):
        """Returns True if this track is confirmed."""
        return self.state == TrackState.Confirmed

    def is_deleted(self):
        """Returns True if this track is dead and should be deleted."""
        return self.state == TrackState.Deleted


class Tracker(DeepSortTrackerModuleBase):
    """
    This is the multi-target tracker.

    Parameters
    ----------
    metric : nn_matching.NearestNeighborDistanceMetric
        A distance metric for measurement-to-track association.
    max_age : int
        Maximum number of missed misses before a track is deleted.
    n_init : int
        Number of consecutive detections before the track is confirmed. The
        track state is set to `Deleted` if a miss occurs within the first
        `n_init` frames.

    Attributes
    ----------
    metric : nn_matching.NearestNeighborDistanceMetric
        The distance metric used for measurement to track association.
    max_age : int
        Maximum number of missed misses before a track is deleted.
    n_init : int
        Number of frames that a track remains in initialization phase.
    kf : kalman_filter.KalmanFilter
        A Kalman filter to filter target trajectories in image space.
    tracks : List[Track]
        The list of active tracks at the current time step.

    """

    def __init__(self, metric, max_iou_distance=0.7, max_age=30, n_init=3,
                 track_deletion_callback=None,
                 track_confirmation_callback=None,
                 start_id=1,
                 **kwargs
                 ):
        self.metric = metric
        self.max_iou_distance = max_iou_distance
        self.max_age = max_age
        self.n_init = n_init
        self.track_deletion_callback = track_deletion_callback
        self.track_confirmation_callback = track_confirmation_callback

        self.kf = kalman_filter.KalmanFilter()
        self.tracks = []
        self._next_id = start_id

    def predict(self):
        """Propagate track state distributions one time step forward.

        This function should be called once every time step, before `update`.
        """
        for track in self.tracks:
            track.predict(self.kf)

    def clear(self):
        """Clear all tracks"""
        for track in self.tracks:
            track.mark_deleted()

    def update(self, detections: List[BBoxPrediction]):
        """Perform measurement update and track management.
        Parameters
        ----------
        detections : List[BBoxPrediction]
            A list of detections at the current time step.
        """
        # Run matching cascade.
        matches, unmatched_tracks, unmatched_detections = \
            self._match(detections)

        # Update track set.
        # print("update track set")
        tracked_detections = []
        for track_idx, detection_idx in matches:
            self.tracks[track_idx].update(
                self.kf, detections[detection_idx])
            detections[detection_idx].info.track = self.tracks[track_idx]
            tracked_detections.append(detections[detection_idx])

        for track_idx in unmatched_tracks:
            self.tracks[track_idx].mark_missed()
        for detection_idx in unmatched_detections:
            self._initiate_track(detections[detection_idx])

        # print("prep partial fit")
        active_targets, features, targets = [], [], []
        for track in self.tracks:
            # print(track, "del:", track.is_deleted(), "confirmed:", track.is_confirmed())
            if track.is_deleted():
                if self.track_deletion_callback:
                    # print("deleting!")
                    self.track_deletion_callback(track)
                # print("deleted")
                continue

            if track.is_confirmed():
                # print("track confirmed")
                if self.track_confirmation_callback:
                    self.track_confirmation_callback(track)
                    # print("track confirmed callback")

                active_targets.append(track.track_id)
                features += track.features
                targets += [track.track_id for _ in track.features]
                track.features = []

        # print("partial fit")
        self.metric.partial_fit(np.asarray(features), np.asarray(targets), active_targets)
        # # print("removing deleted tracks")
        self.tracks = [t for t in self.tracks if not t.is_deleted()]

        #
        # self.deleted_tracks = [t for t in self.tracks if t.is_deleted()]
        #
        # # Update distance metric.
        # active_targets = [t.track_id for t in self.tracks if t.is_confirmed()]
        # features, targets = [], []
        # for track in self.tracks:
        #     if not track.is_confirmed():
        #         continue
        #     features += track.features
        #     targets += [track.track_id for _ in track.features]
        #     track.features = []
        # self.metric.partial_fit(
        #     np.asarray(features), np.asarray(targets), active_targets)

        return tracked_detections

    def _match(self, detections: List[BBoxPrediction]):

        def gated_metric(tracks, dets, track_indices, detection_indices):
            features = np.array([dets[i].info.feature for i in detection_indices])
            targets = np.array([tracks[i].track_id for i in track_indices])
            cost_matrix = self.metric.distance(features, targets)
            cost_matrix = linear_assignment.gate_cost_matrix(
                self.kf, cost_matrix, tracks, dets, track_indices,
                detection_indices)

            return cost_matrix

        # Split track set into confirmed and unconfirmed tracks.
        confirmed_tracks = [
            i for i, t in enumerate(self.tracks) if t.is_confirmed()]
        unconfirmed_tracks = [
            i for i, t in enumerate(self.tracks) if not t.is_confirmed()]

        # Associate confirmed tracks using appearance features.
        matches_a, unmatched_tracks_a, unmatched_detections = \
            linear_assignment.matching_cascade(
                gated_metric, self.metric.matching_threshold, self.max_age,
                self.tracks, detections, confirmed_tracks)

        # Associate remaining tracks together with unconfirmed tracks using IOU.
        iou_track_candidates = unconfirmed_tracks + [
            k for k in unmatched_tracks_a if
            self.tracks[k].time_since_update == 1]
        unmatched_tracks_a = [
            k for k in unmatched_tracks_a if
            self.tracks[k].time_since_update != 1]
        matches_b, unmatched_tracks_b, unmatched_detections = \
            linear_assignment.min_cost_matching(
                iou_matching.iou_cost, self.max_iou_distance, self.tracks,
                detections, iou_track_candidates, unmatched_detections)

        matches = matches_a + matches_b
        unmatched_tracks = list(set(unmatched_tracks_a + unmatched_tracks_b))
        return matches, unmatched_tracks, unmatched_detections

    def _initiate_track(self, detection: BBoxPrediction):
        mean, covariance = self.kf.initiate(detection.bbox_asis.xyah)
        self.tracks.append(Track(
            mean, covariance, self._next_id, self.n_init, self.max_age,
            detection.info.feature))
        self._next_id += 1


class DeepSortSimilarityEncoder(TFModel):
    """ DeepSort Similarity Encoder """

    def __init__(self, model_path):
        super(DeepSortSimilarityEncoder, self).__init__(
            TFModelConfig(
                model_path=model_path,
                gpu_memory_fraction=0.1,
                cuda_visible_device_list='0',
                input_tensors=["net/images:0"],
                output_tensors=["net/features:0"],
                import_name='net'
            )
        )
        self.feature_dim = self.output_tensors[0].get_shape().as_list()[-1]
        self.image_shape = self.input_tensor[0].get_shape().as_list()[1:]

    def _run_in_batches(self, f, data_dict, out, batch_size):
        data_len = len(out)
        num_batches = int(data_len / batch_size)

        s, e = 0, 0
        for i in range(num_batches):
            s, e = i * batch_size, (i + 1) * batch_size
            batch_data_dict = {k: v[s:e] for k, v in data_dict.items()}
            out[s:e] = f(batch_data_dict)
        if e < len(out):
            batch_data_dict = {k: v[e:] for k, v in data_dict.items()}
            out[e:] = f(batch_data_dict)

    def predict(self, image, boxes: List[BBoxPrediction]):
        """ returns encoding for given image_path """
        image_patches = []
        for box in boxes:
            patch = ImageUtil.extract_image_patch(image, box.bbox_normalized, self.image_shape[:2])
            if patch is None:
                print("WARNING: Failed to extract image patch: %s." % str(box))
                patch = np.random.uniform(
                    0., 255., self.image_shape).astype(np.uint8)
            image_patches.append(patch)
        image_patches = np.asarray(image_patches)
        batch_size = 1
        out = np.zeros((len(image_patches), self.feature_dim), np.float32)
        self._run_in_batches(
            lambda x: self.sess.run(self.output_tensors[0], feed_dict=x),
            {self.input_tensor[0]: image_patches}, out, batch_size)
        return out


class DeepSortTracker(DeepSortTrackerModuleBase):
    """ Tracker based on DeepSORT Algorithm """

    def __init__(
            self,
            encoder_model_path,
            track_deletion_callback=None,
            track_confirmation_callback=None,
            start_id=1,
            metric="cosine",
            max_match_distance=0.3,
            nn_budget=None,
            max_age=30
    ):
        """

        Parameters
        ----------
        encoder_model_path : str
            Path to embeddings model frozen graph .pb file.  Used to calculate deep-features in DeepSORT Algo
        track_deletion_callback: function
            A callback function with signature `def del_callback(track)` that is called every time a track is deleted
        track_confirmation_callback: function
            A callback function with signature `def conf_callback(track)` that is called every time a track is confirmed stable
        start_id: int
            Track Ids are assigned starting from this number
        metric: str
            `cosine` or `euclidean`
        max_cosine_distance: float
            The matching threshold. Samples with larger distance are considered an
            invalid match.
        max_euclidean_distance: float
            The matching threshold. Samples with larger distance are considered an
            invalid match.
        budget : int
            If not None, fix samples per class for tracking to at most this number. Removes
            the oldest samples when the budget is reached.
        max_age: int
            Maximum number of misses before a track is deleted.
        """
        super(DeepSortTracker, self).__init__()

        # Definition of the parameters
        if metric == "cosine":
            metric = nn_matching.NearestNeighborDistanceMetric("cosine", max_match_distance, nn_budget)
        elif metric == 'euclidean':
            metric = nn_matching.NearestNeighborDistanceMetric("euclidean", max_match_distance, nn_budget)
        else:
            raise ValueError("Invalid value for arg metric; Must be 'cosine' or 'euclidean'")

        self.tracker = Tracker(
            metric=metric,
            track_deletion_callback=track_deletion_callback,
            track_confirmation_callback=track_confirmation_callback,
            start_id=start_id
        )

        self.encoder = DeepSortSimilarityEncoder(encoder_model_path)

    def update(self, cvimage, detections: List[BBoxPrediction]):
        """
        Updates tracking state with newer detections

        Parameters
        ----------
        cvimage : ndarray
            The OpenCV image on which detection was performed
        detections : List [ vap.predictions.classes.BBoxPrediction ]
            List of detections

        Returns
        -------
        List [ `BBoxPrediction` ] with each detections `info` attribute set to a `Track` object
        """
        # print("encoder predict")
        features = self.encoder.predict(cvimage, detections)
        # print("cropping")
        for d, f in zip(detections, features):
            d.info.feature = f

        # print("cropped done")
        self.tracker.predict()
        # print("tracker.predict returned")
        tracked_detections = self.tracker.update(detections)
        # print("tracker.update returned")

        return tracked_detections

    def clear(self):
        """ clear all tracks """
        return self.tracker.clear()

    @property
    def tracks(self):
        return self.tracker.tracks
