"""
Standard CNN Output definitions - to enable writing model-agnostic convenience functions
"""
import vap.utils.classes
from vap.predictions.primitives import BBox, RBBox
from vap import base
from vap import utils


class BBoxPrediction(base.CNNEstimatorOutputBase):
    """ Bounding Box Predictions """
    def __init__(self, box, clas, score, ht, wd, info=None):
        super(BBoxPrediction, self).__init__()
        self._box = box
        self._cls = clas 
        self._score = score
        self._ht = ht
        self._wd = wd
        self.info = vap.utils.classes.AttrDict() if info is None else info
        self.info.captions = {}

    @classmethod
    def from_TFModel(cls, box, clas, score, ht, wd):
        """
        Init BBoxPrediction from a TFModel output

        Parameters
        ----------
        box : tuple
             (ymin, xmin, ymax, xmax)
        clas : str
            classname string
        score : float
            prediction confidence
        ht : int
            height of the image used for inference
        wd : int
            width of the image used for inference

        Returns
        -------
        New instance of BBoxPrediction
        """
        return cls(
            box=BBox.from_tfbox(box),
            clas=clas,
            score=score,
            ht=ht,
            wd=wd
        )
        
    @classmethod
    def from_xywh(cls, xmin, ymin, w, h, ht, wd, clas='object', score=1.):
        """
        Init BBoxPrediction from x,y,width,ht
        Parameters
        ----------
        xmin : int
            top-left corner x
        ymin : int
            top-left corner y
        w : int
            width of box
        h : int
            ht of box
        ht : int
            height of image used for inference
        wd : int
            width of image used for inference
        clas : str
            classname
        score : float
            prediction confidence

        Returns
        -------
        New instance of BBoxPrediction
        """
        return cls(
            box=BBox(xmin, ymin, xmin + w, ymin + h),
            clas=clas,
            score=score,
            ht=ht,
            wd=wd
        )

    @classmethod
    def from_cv2boxpoints(cls, boxpoints, ht, wd, clas='object', score=1.):
        """
        Init BBoxPrediction from OpenCV BoxPoints
        Parameters
        ----------
        boxpoints : cv2BoxPoints
            WIP
        ht : int
            height of inference image
        wd : int
            width of inference image
        clas : str
            classname
        score : float
            prediction confidence

        Returns
        -------
        New instance of BBoxPrediction
        """
        pbottomright, pbottomleft, ptopleft, ptopright = boxpoints.tolist()
        return cls(
            box=RBBox(tuple(ptopleft), tuple(ptopright), tuple(pbottomright), tuple(pbottomleft)),
            clas=clas,
            score=score,
            ht=ht,
            wd=wd
        )

        
    @property
    def bbox(self):
        """ returns BBox object """
        return self._box

    @bbox.setter
    def bbox(self, bbox):
        self._box = bbox

    @property
    def bbox_normalized(self):
        """ returns normalized BBox """
        if not self.bbox.is_normalized:
            return BBox(self._box.xmin / self._wd, self._box.ymin / self._ht, self._box.xmax / self._wd, self._box.ymax / self._ht)
        else:
            return self.bbox

    @property
    def bbox_asis(self):
        """ returns underive BBox with absolute values """
        if not self.bbox.is_normalized:
            return self.bbox
        else:
            return self.bbox.scaled(self._ht, self._wd)

    @property
    def classname(self):
        """ returns classname """
        return self._cls 
    
    @property
    def score(self):
        """ returns prediction confidence """
        return self._score
        
    def __repr__(self):
        return "BBox Prediction<class:{clas} score:{score} xmin:{xmin} xmax:{xmax} ymin:{ymin} ymax:{ymax}>".format(clas=self._cls, 
        score=self._score, xmin=self.bbox.xmin, xmax=self.bbox.xmax, ymin=self.bbox.ymin, ymax=self.bbox.ymax) 

    def __copy__(self):
        newone = type(self)(self._box, self._cls, self._score, self._ht, self._wd, self.info)
        return newone
