"""
Classes with iterator interface for convenient image input handling

Example:
    ```
    from vap.sources import RealtimeVideoFeed

    for frame in RealtimeVideoFeed("rtsp://my-camera-url/"):
        process(frame)
    ```

"""
import cv2
import numpy as np
from glob import glob
import os
from typing import List
import skvideo.io
from collections import deque
from threading import Thread
import time

from . import base
from . import exceptions


class VideoFeed(base.SrcFeedBase):
    """ Plain Video Feed class wrapping OpenCV's video capture with an iterator interface"""

    def __init__(self, src=0):
        """

        Parameters
        ----------
        src : str or int
            OpenCV Compatible video source

        Example
        ---------
        ```
        for frame in VideoFeed(0):
            process(frame)
        ```
        """
        self.src = src
        if isinstance(self.src, int) or "http" in self.src:
            self.type = 'cv'
            self.cap = cv2.VideoCapture(src)
            ok, _ = self.cap.read()
        else:
            self.type = 'sk'
            self.cap = skvideo.io.vreader(src)
            try:
                self.cap.__next__()[:, :, ::-1]
                ok = True
            except Exception as e:
                self.type = 'cv'
                self.cap = cv2.VideoCapture(src)
                ok, _ = self.cap.read()
                if not ok:
                    raise exceptions.CameraFeedError(e)

        if not ok:
            raise exceptions.CameraFeedError("unable to open video source %s" % src)

    def __iter__(self):
        return self

    def __next__(self):
        if self.type == 'cv':
            if self.cap.isOpened():
                ok, frame = self.cap.read()
                if not ok:
                    self.cap.release()
                    raise StopIteration
                return frame
            else:
                raise StopIteration
        elif self.type == 'sk':
            return self.cap.__next__()[:, :, ::-1]
        else:
            raise StopIteration


class DummyVideoFeed(base.SrcFeedBase):
    """A Video Feed class that can act as a placeholder - returns blank frame with frame-size printed at the center"""

    def __init__(self, size=(480, 640, 3)):
        """
        Parameters
        ----------
        size : tuple
            H, W, Channels - specify frame size

        """
        self.size = size
        self.src = np.zeros(size, dtype=np.uint8)

        h, w, c = size
        mh, mw = int(h / 2), int(w / 2)

        text = "%dx%d" % (w, h)
        font = cv2.FONT_HERSHEY_COMPLEX
        font_scale = 2.0
        font_color = (255, 255, 255)
        font_thickness = 1
        txsize, baseline = cv2.getTextSize(text, font, font_scale, font_thickness)

        txw, txh = txsize

        otxh = int(mh + txh / 2)
        otxw = int(mw - txw / 2)

        cv2.putText(self.src, text, (otxw, otxh), font, font_scale, font_color, font_thickness)

    def __iter__(self):
        return self

    def __next__(self):
        time.sleep(0.01)
        return self.src.copy()


class RealtimeVideoFeed(base.SrcFeedBase):
    """ A Video Feed that doesn't buffer frames.  Hence, if application is slower than source, it skips frames.
    This removes uncontrolled lag encountered using OpenCV VideoCapture.
    It can also emulate different FPS source if reading a stored feed.
     """

    def __init__(self, src=0, tick=1, buff_size=1, max_fps=None, props={}):
        """

        Parameters
        ----------
        src : str
            OpenCV compative video source
        tick : int
            Unit of time; Default is 1 second
        buff_size : int
            internal frame buffer size; Default is 1
        max_fps : int
            FPS at which the source sends frames
            Can be used to emulate a realtime camera while reading a recorded video and analyze application
            performance and speed. Default is None for which it will read frames from source as soon as they are available
        props : dict
            OpenCV VideoSrc Flags that need to be passed as key value pairs
            Ex: props = { cv2.CAP_PROP_FRAME_WIDTH: 320 }

        Example
        ---------
        ```
        # open file, emulate 25fps camera, skip first 1000 frames
        for frame in RealtimeVideoFeed("test.mp4", max_fps=25, props={cv2.CAP_PROP_POS_FRAMES, 1000}):
            process(frame)
        ```
        """
        self.src = src
        self.tick = tick
        self.tick_count = 0
        self.buffer = deque(maxlen=buff_size)
        if max_fps is not None:
            self.tick_sleep_time = 1 / max_fps
        else:
            self.tick_sleep_time = 0
        self.die = False

        if isinstance(self.src, int) or "http" in self.src or "rtsp" in self.src:
            self.type = 'cv'
            self.cap = cv2.VideoCapture(src)
            self.cap.set(cv2.CAP_PROP_BUFFERSIZE, buff_size)
            for k, v in props.items():
                self.cap.set(k, v)
            ok, _ = self.cap.read()
        else:
            self.type = 'sk'
            self.cap = skvideo.io.vreader(src)
            try:
                self.cap.__next__()[:, :, ::-1]
                ok = True
            except Exception as e:
                self.type = 'cv'
                self.cap = cv2.VideoCapture(src)
                ok, _ = self.cap.read()
                if not ok:
                    raise exceptions.CameraFeedError(e)

        if not ok:
            raise exceptions.CameraFeedError("unable to open video source %s" % src)

        self.ticker = Thread(target=self._tick_thread, daemon=True)
        self.ticker.start()

    def _tick_thread(self):
        while not self.die:
            try:
                next_frame = self._get_next_frame()
                self.buffer.append(next_frame)
                self.tick_count += 1
                time.sleep(self.tick_sleep_time)
            except StopIteration:
                self.buffer.append(None)
                break

    def __iter__(self):
        return self

    def __next__(self):
        limit = 1000
        while not self.buffer:  # if buffer is empty
            limit -= 1
            time.sleep(0.001)
            if limit < 0:
                raise StopIteration
        nxt = self.buffer.popleft()
        if nxt is None:
            raise StopIteration
        self.tick_count -= 1
        return nxt

    def _get_next_frame(self):
        if self.type == 'cv':
            if self.cap.isOpened():
                ok, frame = self.cap.read()  # [self.cap.read() for t in range(self.tick)][-1]
                if not ok:
                    raise StopIteration
                return frame
            else:
                raise StopIteration
        elif self.type == 'sk':
            return self.cap.__next__()[:, :, ::-1]  # [self.cap.__next__()[:,:,::-1] for t in range(self.tick)][-1]
        else:
            raise StopIteration


class FolderFeed(base.SrcFeedBase):
    """ Returns Iterator to all images in a folder """

    def __init__(self, path: str, exts: List[str] = ['jpg', 'jpeg', 'png' , 'bmp'], sort_key=None, imread_flag=cv2.IMREAD_UNCHANGED):
        """

        Parameters
        ----------
        path : str
            path to directory with images
        exts : list[str]
            list of extensions - default is [jpg, jpeg, png]
        sort_key : function
            a sorting key function/lambda expression that is passed to python sort function to order images.
            the sort_key function can be used to convert image path into a proxy value that can be used for sorting
            Refer https://developers.google.com/edu/python/sorting
        imread_flag : cv2 Flag
            a cv2.IMREAD_XXXX flag that will be passed to cv2.imread call - default is cv2.IMREAD_UNCHANGED
            Refer https://www.tutorialspoint.com/opencv/opencv_imread_xxx_flag.htm

        Example
        -------
        ```
        for each_image in FolderFeed("project/images/", exts=["bmp","png"],sort_key=lambda x: os.path.basename(x)):
            process(each_image)
        ```
        """
        if not os.path.exists(path):
            raise exceptions.InputFeedError("specified path doesn't exist %s" % path)

        self.paths = []
        for ext in exts:
            self.paths += glob(os.path.join(path, "*.%s" % ext))

        if len(self.paths) < 1:
            raise exceptions.InputFeedError("no images in directory %s" % path)

        self.index = 0

        self.paths.sort(key=sort_key)

        self.imread_flag = imread_flag

    def __iter__(self):
        return self

    def __next__(self):
        try:
            self.index += 1
            path = self.paths[self.index - 1]
            return path, cv2.imread(path, self.imread_flag)
        except:
            raise StopIteration
