""" CNN Prediction manipulation functions """

from typing import List
import cv2

import vap.predictions
from vap import base, predictions


class PredictionUtil(base.UtilBase):
    """ Base Class for all prediction manipulation """
    def __init__(self):
        super(PredictionUtil, self).__init__()


    @staticmethod
    def expand_bbox(preds: List[predictions.BBoxPrediction], margin=.2):
        """
        Expands Bbox by a margin %

        Parameters
        ----------
        preds : List[ vap.predictions.classes.BBoxPrediction ]
            List of BboxPredictions that need to enlarged
        margin : float
            Between 0-1 to specifying expansion percentage

        Returns
        -------
        List [ vap.predictions.classes.BBoxPrediction ] with expanded coordinates
        """
        for i in range(len(preds)):
            x, y, w, h = preds[i].bbox.ltwh
            y2, x2 = preds[i].bbox.tlbr[2:]
            w *= margin
            h *= margin
            x1, y1, x2, y2 = [x-w, y-h, x2+w, y2+h]
            x1 = max(x1, 0)
            y1 = max(y1, 0)
            x2 = min(x2, 1)
            y2 = min(y2, 1)
            preds[i].bbox = vap.predictions.primitives.BBox(x1, y1, x2, y2)

        return preds

    @staticmethod
    def extract_crops_to_info(preds: List[predictions.BBoxPrediction], crop_size, cropimage):
        for d in preds:
            bbox = d.bbox.scaled(*cropimage.shape[:2])
            d.info.crop = cv2.resize(cropimage[bbox.ymin:bbox.ymax, bbox.xmin:bbox.xmax], crop_size)

        return preds