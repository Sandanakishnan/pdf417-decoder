""" Visualization Utility Functions """

from typing import List

import cv2
import numpy as np
from PIL import ImageFont, ImageDraw, Image, ImageColor

import vap.predictions
from vap import base, predictions
from vap.utils.classes import TextAlign, DisplayText
from vap.utils.image_utils import ImageUtil


class VisualizationUtil(base.UtilBase):
    """ Visualization Utility Functions """

    STANDARD_COLORS_LIST = [
        'AliceBlue', 'Chartreuse', 'Aqua', 'Aquamarine', 'Azure', 'Beige', 'Bisque',
        'BlanchedAlmond', 'BlueViolet', 'BurlyWood', 'CadetBlue', 'AntiqueWhite',
        'Chocolate', 'Coral', 'CornflowerBlue', 'Cornsilk', 'Crimson', 'Cyan',
        'DarkCyan', 'DarkGoldenRod', 'DarkGrey', 'DarkKhaki', 'DarkOrange',
        'DarkOrchid', 'DarkSalmon', 'DarkSeaGreen', 'DarkTurquoise', 'DarkViolet',
        'DeepPink', 'DeepSkyBlue', 'DodgerBlue', 'FireBrick', 'FloralWhite',
        'ForestGreen', 'Fuchsia', 'Gainsboro', 'GhostWhite', 'Gold', 'GoldenRod',
        'Salmon', 'Tan', 'HoneyDew', 'HotPink', 'IndianRed', 'Ivory', 'Khaki',
        'Lavender', 'LavenderBlush', 'LawnGreen', 'LemonChiffon', 'LightBlue',
        'LightCoral', 'LightCyan', 'LightGoldenRodYellow', 'LightGray', 'LightGrey',
        'LightGreen', 'LightPink', 'LightSalmon', 'LightSeaGreen', 'LightSkyBlue',
        'LightSlateGray', 'LightSlateGrey', 'LightSteelBlue', 'LightYellow', 'Lime',
        'LimeGreen', 'Linen', 'Magenta', 'MediumAquaMarine', 'MediumOrchid',
        'MediumPurple', 'MediumSeaGreen', 'MediumSlateBlue', 'MediumSpringGreen',
        'MediumTurquoise', 'MediumVioletRed', 'MintCream', 'MistyRose', 'Moccasin',
        'NavajoWhite', 'OldLace', 'Olive', 'OliveDrab', 'Orange', 'OrangeRed',
        'Orchid', 'PaleGoldenRod', 'PaleGreen', 'PaleTurquoise', 'PaleVioletRed',
        'PapayaWhip', 'PeachPuff', 'Peru', 'Pink', 'Plum', 'PowderBlue', 'Purple',
        'Red', 'RosyBrown', 'RoyalBlue', 'SaddleBrown', 'Green', 'SandyBrown',
        'SeaGreen', 'SeaShell', 'Sienna', 'Silver', 'SkyBlue', 'SlateBlue',
        'SlateGray', 'SlateGrey', 'Snow', 'SpringGreen', 'SteelBlue', 'GreenYellow',
        'Teal', 'Thistle', 'Tomato', 'Turquoise', 'Violet', 'Wheat', 'White',
        'WhiteSmoke', 'Yellow', 'YellowGreen', 'Black'
    ]

    LOADED_FONTS = {}

    def __init__(self):
        super().__init__()

    @staticmethod
    def _load_font(fontname, fontsize):
        try:
            font = VisualizationUtil.LOADED_FONTS[fontname]
        except:
            try:
                font = ImageFont.truetype(fontname, fontsize)
            except IOError:
                font = ImageFont.load_default()

            VisualizationUtil.LOADED_FONTS[fontname] = font

        return font

    @staticmethod
    def _resolve_align_xy(text_width, text_height, text_align, xmax, ymax, margin=0.05):
        margin_x = int(margin*text_width)
        margin_y = int(margin*text_height)

        if text_align == TextAlign.TopLeft:
            pivot_y = 0 + margin_y
            pivot_x = 0 + margin_x
        elif text_align == TextAlign.TopCenter:
            pivot_y = 0 + margin_y
            pivot_x = int(0.5 * xmax)
        elif text_align == TextAlign.TopRight:
            pivot_y = 0 + margin_y
            pivot_x = xmax - text_width - margin_x
        elif text_align == TextAlign.MiddleLeft:
            pivot_y = int(0.5*ymax)
            pivot_x = 0 + margin_x
        elif text_align == TextAlign.MiddleCenter:
            pivot_y = int(0.5*ymax)
            pivot_x = int(0.5*xmax)
        elif text_align == TextAlign.MiddleRight:
            pivot_y = int(0.5*ymax)
            pivot_x = xmax - text_width - margin_x
        elif text_align == TextAlign.BottomLeft:
            pivot_y = ymax - text_height - margin_y
            pivot_x = 0 + margin_x
        elif text_align == TextAlign.BottomCenter:
            pivot_y = ymax - text_height - margin_y
            pivot_x = int(0.5*xmax)
        elif text_align == TextAlign.BottomRight:
            pivot_y = ymax - text_height - margin_y
            pivot_x = xmax - text_width - margin_x

        return pivot_x, pivot_y

    @staticmethod
    def draw_text_(plimage, dtext: DisplayText):
        draw = ImageDraw.Draw(plimage)
        xmax, ymax = plimage.size
        fontname = dtext.fontname
        fontsize = dtext.size
        font = VisualizationUtil._load_font(fontname, fontsize)
        text_width, text_height = font.getsize(dtext.text)
        text_align = dtext.align
        font_color = dtext.color
        bg_color = dtext.bgcolor
        pivot_x, pivot_y = VisualizationUtil._resolve_align_xy(
            text_width, text_height, text_align, xmax, ymax, 0.05)
        txt = dtext.text
        if bg_color:
            margin_x = int(text_width*.05)
            margin_y = int(text_height*0.05)
            draw.rectangle([(pivot_x-margin_x, pivot_y-margin_y), (pivot_x + text_width +
                                                                   margin_x, pivot_y + text_height + margin_y)], fill=bg_color.name)

        draw.text(
            (pivot_x, pivot_y),
            txt,
            fill=font_color.name,
            font=font)

        return plimage
    @staticmethod
    def draw_text(plcvimage, dtext: DisplayText):
        if type(plcvimage) is Image.Image:
            return VisualizationUtil.draw_text_(plcvimage, dtext)
        else:
            plimage = ImageUtil.cv_to_pil(plcvimage)
            plimage = VisualizationUtil.draw_text_(plimage, dtext)
            return ImageUtil.pil_to_cv(plimage)

    @staticmethod
    def draw_detection_texts(draw, bbox: vap.predictions.primitives.BBox, display_text_list: List[DisplayText], boxcolor='red'):
        if len(display_text_list) == 0:
            return

        fontname = display_text_list[0].fontname
        fontsize = display_text_list[0].size
        font = VisualizationUtil._load_font(fontname, fontsize)
        ymin, xmin, ymax, xmax = bbox.tlbr
        display_str_list = {}

        for text in display_text_list:
            if text.fontname != fontname:
                fontname, fontsize = text.fontname, text.fontsize
                font = VisualizationUtil._load_font(fontname, fontsize)

            display_str_list.setdefault(text.align, []).append(text.text)
            color = text.color

        for align, str_list in display_str_list.items():
            if 'Top' in align.name:
                pivot_y = ymin
            elif 'Middle' in align.name:
                pivot_y = int((ymax - ymin) / 2)
            elif 'Bottom' in align.name:
                pivot_y = ymax

            if 'Left' in align.name:
                pivot_x = xmin
            elif 'Middle' in align.name:
                pivot_x = (xmax - xmin)/2
            elif 'Right' in align.name:
                pivot_x = xmax

            display_str_heights = [font.getsize(ds)[1] for ds in str_list]
            display_str_widths = [font.getsize(ds)[0] for ds in str_list]
            total_display_str_height = sum(display_str_heights)
            # margin = np.ceil(0.05* font.getsize(str_list[0])[1])
            max_text_width = max(display_str_widths)
            draw.rectangle([(pivot_x, pivot_y), (pivot_x + max_text_width,
                                                 pivot_y + total_display_str_height)], fill=boxcolor)

            for txt in str_list:
                text_width, text_height = font.getsize(txt)
                margin = np.ceil(0.05 * text_height)
                draw.text(
                    (pivot_x + margin, pivot_y + margin),
                    txt,
                    fill=color.name,
                    font=font)
                pivot_y += text_height - 2 * margin

    @staticmethod
    def draw_display_text(plimage, display_text: DisplayText):
        draw = ImageDraw.Draw(plimage)
        width, height = plimage.size
        fontname = display_text.fontname
        fontsize = display_text.size
        font = VisualizationUtil._load_font(fontname, fontsize)
        color = display_text.color
        align = display_text.align
        ymin, xmin, ymax, xmax = 0, 0, height, width
        txt = display_text.text

        if 'Top' in align.name:
            pivot_y = ymin
        elif 'Middle' in align.name:
            pivot_y = int((ymax - ymin) / 2)
        elif 'Bottom' in align.name:
            pivot_y = ymax

        if 'Left' in align.name:
            pivot_x = xmin
        elif 'Middle' in align.name:
            pivot_x = (xmax - xmin)/2
        elif 'Right' in align.name:
            pivot_x = xmax

        text_width, text_height = font.getsize(txt)
        margin = np.ceil(0.05 * text_height)
        draw.text(
            (pivot_x + margin, pivot_y + margin),
            txt,
            fill=color.name,
            font=font)
        pivot_y += text_height - 2 * margin

        return plimage

    @staticmethod
    def draw_list_of_display_texts(plimage, display_text_list: List[DisplayText]):
        if len(display_text_list) == 0:
            return plimage

        draw = ImageDraw.Draw(plimage)
        width, height = plimage.size
        fontname = display_text_list[0].fontname
        fontsize = display_text_list[0].size
        font = VisualizationUtil._load_font(fontname, fontsize)
        ymin, xmin, ymax, xmax = 0, 0, height, width
        display_str_list = {}

        for text in display_text_list:
            if text.fontname != fontname:
                fontname, fontsize = text.fontname, text.fontsize
                font = VisualizationUtil._load_font(fontname, fontsize)

            display_str_list.setdefault(text.align, []).append(text.text)
            color = text.color

        for align, str_list in display_str_list.items():
            if 'Top' in align.name:
                pivot_y = ymin
            elif 'Middle' in align.name:
                pivot_y = int((ymax - ymin) / 2)
            elif 'Bottom' in align.name:
                pivot_y = ymax

            if 'Left' in align.name:
                pivot_x = xmin
            elif 'Middle' in align.name:
                pivot_x = (xmax - xmin)/2
            elif 'Right' in align.name:
                pivot_x = xmax

            display_str_heights = [font.getsize(ds)[1] for ds in str_list]
            display_str_widths = [font.getsize(ds)[0] for ds in str_list]
            total_display_str_height = sum(display_str_heights)
            # margin = np.ceil(0.05* font.getsize(str_list[0])[1])
            max_text_width = max(display_str_widths)
            # draw.rectangle([(pivot_x, pivot_y), (pivot_x + max_text_width, pivot_y + total_display_str_height)])

            for txt in str_list:
                text_width, text_height = font.getsize(txt)
                margin = np.ceil(0.05 * text_height)
                draw.text(
                    (pivot_x + margin, pivot_y + margin),
                    txt,
                    fill=color.name,
                    font=font)
                pivot_y += text_height - 2 * margin

        return plimage

    @staticmethod
    def draw_bbox_detection_(draw, w, h, bbox_detection: predictions.BBoxPrediction, color=None, thickness=1, opacity=1.):
        
        display_str_list = []

        if hasattr(bbox_detection.info, 'hidden'):
            hidden = bbox_detection.info.hidden
        else:
            hidden = False

        if not hidden:
            #display_str_list = [DisplayText("score: %0.2f"%bbox_detection.score,TextAlign.BottomLeft)]
            if hasattr(bbox_detection.info, 'captions'):
                for k, v in bbox_detection.info.captions.items():
                    display_str_list.append(DisplayText(
                        "%s:%s" % (k, v), TextAlign.BottomLeft
                    ))

        if hasattr(bbox_detection.info, 'track'):
            display_str_list.append(DisplayText(
                "%s" % bbox_detection.info.track.track_id,
                TextAlign.TopLeft
            ))

        if hasattr(bbox_detection.info, 'track_id'):
            display_str_list.append(DisplayText(
                "%s" % bbox_detection.info.track_id,
                TextAlign.TopLeft
            ))

        if hasattr(bbox_detection.info, 'boxcolor'):
            color = bbox_detection.info.boxcolor

        if hasattr(bbox_detection.info, 'boxthickness'):
            thickness = bbox_detection.info.boxthickness   

        if hasattr(bbox_detection.info, 'boxopacity'):
            opacity = bbox_detection.info.boxopacity                       

        sbbox = bbox_detection.bbox.scaled(h, w)
        ymin, xmin, ymax, xmax = sbbox.tlbr

        opacity = int(opacity*255)
        color = ImageColor.getrgb(color)
        fill = None
        outline = color + (opacity, )
        if thickness == -1:
            fill = color + (opacity,)
            outline = None 


        if not hidden:
            draw.rectangle(
                ((xmin, ymin), (xmax, ymax)),
                outline=outline,
                width=thickness,
                fill=fill
            )

        VisualizationUtil.draw_detection_texts(
            draw, sbbox, display_str_list, color)
        
        return draw

    @staticmethod
    def draw_bbox_detections_(plimage, bbox_detections: List[predictions.BBoxPrediction], color=None, thickness=1, opacity=1.):
        """ draws bounding boxes in the given pil image """

        if color is None:
            color = 'red'

        overlay = Image.new('RGBA', plimage.size, (0,0,0,0,))
        draw = ImageDraw.Draw(overlay)
        w, h = plimage.size

        for bbox_detection in bbox_detections:
            VisualizationUtil.draw_bbox_detection_(
                draw, w, h, bbox_detection, color, thickness, opacity)
        
        # Alpha composite these two images together to obtain the desired result.
        plimage = plimage.convert("RGBA")
        plimage = Image.alpha_composite(plimage, overlay)
        plimage = plimage.convert("RGB") # Remove alpha 

        return plimage

    @staticmethod
    def draw_bbox_detections(plcvimage, bbox_detections: List[predictions.BBoxPrediction], color=None, thickness=1, opacity=1.):
        """
        Draws list of vap.predictions.classes.BBoxPrediction on opencv/pil image and returns it

        Parameters
        ----------
        plcvimage : Pillow or OpenCV image
            image on which to draw boxes
        bbox_detections : List [ vap.predictions.classes.BBoxPrediction ]
            List of predictions that need to be visualized

            Captions can be added to each detection by updating the `each_bbox_prediction.info.captions` dictionary

            Ex:
            To display a classname, set

            `each_bbox_prediction.info.captions['Class'] = each_bbox_prediction.clas`

        color : vap.utils.classes.STANDARD_COLORS
        thickness : int

        Returns
        -------
        Pillow or OpenCV image with bounding boxes drawn
        """
        if type(plcvimage) is Image.Image:
            plcvimage = VisualizationUtil.draw_bbox_detections_(
                plcvimage, bbox_detections, color, thickness, opacity)
            return plcvimage
        else:
            pilimage = ImageUtil.cv_to_pil(plcvimage)
            pilimage = VisualizationUtil.draw_bbox_detections_(
                pilimage, bbox_detections, color, thickness, opacity)
            return ImageUtil.pil_to_cv(pilimage).copy()

    @staticmethod
    def _alpha_blend(background, foreground, alpha=0.5):
        background = background.astype(float) / 255.
        foreground = foreground.astype(float) / 255.

        if foreground.shape[2] == 4:
            alpha = foreground[..., 3:]
            foreground = foreground[:, :, :3]

        # Multiply the foreground with the alpha matte
        foreground = alpha * foreground
        # Multiply the background with ( 1 - alpha )
        background = (1.0 - alpha) * background
        # Add the masked foreground and background.
        outImage = cv2.add(foreground, background)

        return outImage

    @staticmethod
    def transparent_overlay(background, overlay, offset_xy, alpha=0.5):
        if type(background) is Image.Image and type(overlay) is Image.Image:
            background = background.convert("RGBA")
            overlay = overlay.convert("RGB")
            outimage = Image.new("RGBA", background.size)
            outimage.paste(overlay, offset_xy)
            outimage = background
            background.paste(overlay, offset_xy)
            return background

        if type(background) is Image.Image:
            background = ImageUtil.pil_to_cv(background)

        if type(overlay) is Image.Image:
            overlay = ImageUtil.pil_to_cv(overlay)

        h, w = background.shape[:2]
        oh, ow = overlay.shape[:2]

        x, y = offset_xy
        y2 = min(h, y+oh)
        x2 = min(w, x+ow)

        fitted_overlay = np.zeros((h, w, 4), dtype=float)
        fitted_overlay[y:y2, x:x2, :3] = overlay[:y2-y, :x2-x]
        fitted_overlay[y:y2, x:x2, 3:] = alpha * 255

        outimage = VisualizationUtil._alpha_blend(
            background, fitted_overlay, alpha)
        return (outimage * 255.).astype(np.uint8)
