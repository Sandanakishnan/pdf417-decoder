from skimage import io
import cv2
import numpy as np
import pandas as pd 
import itertools

#Read Image and convert it into gray
img=cv2.imread('online_img/pdf417_error2.png')
# img=cv2.imread('D:/pdf417.png')
#img=cv2.imread('Images/pdf417_ema1il_en.png')
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
Start_pattern='81111113'
Stop_pattern='711311121'
import time

#Read Codeword Combo
def codeword_combo(filename):
    f=open(filename,'r')
    code_combos=f.read()
    f.close()
    
    code_combos=code_combos.split('\n')
    codeword_list=[]
    for i in range(len(code_combos)):
        codes=code_combos[i].split(' ')
        codeword_list.append(codes)    
    codeword_list=np.asarray(codeword_list)
    return codeword_list


#Read Text Codeword
def Text_codeword(filename):
    f=open(filename,'r')
    code=f.read()
    f.close()
    code=code.split('\n')
    Textcode=[]
    for j in range(31):
        Textcode.append(code[j].split('\t')[1:])
    
    Textcode=pd.DataFrame(Textcode[1:],columns=Textcode[0])
    return Textcode

codeword_list=codeword_combo('PDF417codes/pdf417_codeword_combos.txt')

def reformat(row_data):
    return [(i[0], len(list(i[1]))) for i in itertools.groupby(row_data)]


def find_x_y_codewordlen(gray):
    vv=reformat(gray[1,:])
    bar_x=vv[1][1]
    bar_y=bar_x*3
    codewordlen=17*bar_x
    return bar_x,bar_y,codewordlen

def get_latch_shiftlist():
    latchlist={'ll':['Lower',0],'as':['Alpha',1],'al':['Alpha',0],'ml':['Mixed',0],'pl':['Punctuation',0],'ps':['Punctuation',1]}
    return latchlist

def Texttype_decode(code,Prev_char=None,Preprev_char=None,Prev_mode=None,Preprev_mode=None):
    S_c=code%30
    F_c=(code-S_c)/30
    Textcode=Text_codeword('PDF417codes/pdf417_text_code.txt')
    latchlist=get_latch_shiftlist()
    if Prev_char is None and Textcode['Alpha'][F_c] in latchlist:
        first_char=Textcode['Alpha'][F_c]
        textmode=Textcode['Alpha'][F_c]
        mode=latchlist[textmode]
        second_char=Textcode[mode[0]][S_c]
        pm=mode[0]
        ppm='Alpha'
    elif Prev_char is None and Textcode['Alpha'][F_c] not in latchlist:
        first_char=Textcode['Alpha'][F_c]
        ppm='Alpha'
        if first_char in latchlist:
            mode=latchlist[first_char]
            second_char=Textcode[mode[0]][S_c]
            pm=mode[0]
        else:
            second_char=Textcode['Alpha'][S_c]
            pm='Alpha'
    elif Prev_char is not None and Prev_char in latchlist:
        mode=latchlist[Prev_char]
        first_char=Textcode[mode[0]][F_c]
        ppm=mode[0]
        if mode[1]==0:
            second_char=Textcode[mode[0]][S_c]
            pm=mode[0]
        else:
            second_char=Textcode[Preprev_mode][S_c]
            pm=Preprev_mode
    elif Prev_char is not None and Preprev_char in latchlist and latchlist[Preprev_char][1]==1:
         mode= latchlist[Preprev_char]
         first_char=Textcode[Preprev_mode][F_c]
         ppm=Preprev_mode
         if first_char in latchlist:
            mode1=latchlist[first_char]
            second_char=Textcode[mode1[0]][S_c]
            pm=mode1[0]
         else:
            second_char=Textcode[Preprev_mode][S_c]
            pm=Preprev_mode
    else:
        first_char=Textcode[Prev_mode][F_c]
        ppm=Prev_mode
        if first_char in latchlist:
            mode=latchlist[first_char]
            second_char=Textcode[mode[0]][S_c]
            pm=mode[0]
        else:
            second_char=Textcode[Prev_mode][S_c]
            pm=Prev_mode
        
            
    return first_char,second_char,ppm,pm

def Bytetype_decode(codewords):
    codewordvalue=codewords[0]
    for c in range(1,len(codewords)):
        codewordvalue=(codewordvalue*900)+codewords[c]
    decoded=[]
    decoded_data=''
    decoded.append(codewordvalue%256)
    decoded_data=chr(int(decoded[-1]))+decoded_data
    for c in range(5):
        codewordvalue=(codewordvalue-decoded[-1])/256
        decoded.append(codewordvalue%256)
        decoded_data=chr(int(decoded[-1]))+decoded_data
    return decoded_data

def Numerictype_decode(codewords):
    codewordvalue=codewords[0]
    for c in range(1,len(codewords)):
        codewordvalue=(codewordvalue*900)+codewords[c]
    codewordvalue=str(codewordvalue)[1:]
    return codewordvalue

def check_type(gray,bar_x,Stop_pattern):
    stop_zonelen=19*bar_x
    code=gray[0,stop_zonelen:]
    codepattern1=''
    cnt=0
    for k in range(0,len(code),bar_x):
        if k==0:
            cnt=cnt+1
        elif code[k]==code[k-1]:
            cnt=cnt+1
        else:
            codepattern1=codepattern1+str(cnt)
            cnt=1
        if k==len(code)-bar_x:
            codepattern1=codepattern1+str(cnt)
    stopzone=Stop_pattern
    if codepattern1[-9:]==stopzone:
        type_PDF=1# Normal PDF417
    else:
        type_PDF=0
        
    return type_PDF
start_time=time.time()    
bar_x,bar_y,codewordlen=find_x_y_codewordlen(gray)

Decoded_data=''
Rownumbers=[]
codepatterns=[]
latchlist=get_latch_shiftlist()

Prev_char=None
Preprev_char=None
Prev_mode=None
Preprev_mode=None
codewords=[]
flg=0
fin_flg=0
type_PDF=check_type(gray,bar_x,Stop_pattern)

if type_PDF==1:
    end_col=len(gray[1,:])-(2*codewordlen+bar_x)
elif type_PDF==0:
    end_col=len(gray[1,:])-bar_x
for i in range(0,len(gray),bar_y):
    for j in range(2*codewordlen,end_col,codewordlen):
        code=gray[i,j:j+codewordlen]
        codepattern=''
        cnt=0
        dat=''
        codepattern=''.join([str(int(len(list(i[1]))/bar_x)) for i in itertools.groupby(code)])
        
        rowno=int(codeword_list[np.where(codeword_list==codepattern)[0][0],0])
        if i==0 and j==3*codewordlen:
            Pdf_mode=rowno
            if rowno==900:
                print('Text type Decoder')
            elif rowno==901 or rowno==924:
                print('Byte type decoder')
            else:
                print('Numeric type decoder')
        if i==0 and j>3*codewordlen:
            flg=1
        if flg==1:
            if rowno==900 or rowno==901 or rowno==902:
                fin_flg=fin_flg+1
            if Pdf_mode==900 and fin_flg==0:
                first_char,second_char,Preprev_mode,Prev_mode=Texttype_decode(rowno,Prev_char,Preprev_char,Prev_mode,Preprev_mode)
                Prev_char=second_char
                Preprev_char=first_char
                
                if first_char not in latchlist:
                    if first_char=='space':
                        dat=dat+' '
                    else:
                        dat=dat+first_char

                if second_char not in latchlist:
                    if second_char=='space':
                        dat=dat+' '
                    else:
                        dat=dat+second_char
                        
            elif (Pdf_mode==901 or Pdf_mode==924) and fin_flg==0:
                if Pdf_mode==901:#Its not multiple of 6
                    codewords.append(rowno)
                    if len(codewords)%5==0: #Multiple of 6
                        dat=Bytetype_decode(codewords)
                    elif len(codewords)>5:
                        dat=chr(int(rowno))
                    
                elif Pdf_mode==924:
                    codewords.append(rowno)
                    if len(codeword_list)%5==0:
                        dat=Bytetype_decode(codewords)
                    elif len(codewords)>5:
                        dat=chr(int(rowno))
                
            elif Pdf_mode==902:
                codewords.append(rowno)
                if (len(codewords)%15==0 and fin_flg==0) or (len(codewords)<15 and fin_flg==1):
                    if fin_flg==1:
                        dat=Numerictype_decode(codewords[0:-1])
                    else:
                        dat=Numerictype_decode(codewords)
                
            Decoded_data=Decoded_data+dat
#        
        codepatterns.append(codepattern)
        Rownumbers.append(int(rowno))

end_time=time.time()-start_time

io.imshow(img)
print('Decoded Data')
print(Decoded_data)
print('execution time',end_time)


        