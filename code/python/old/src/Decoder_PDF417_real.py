from skimage import io
import cv2
import numpy as np
import pandas as pd 
import itertools
from PIL import Image
from math import ceil
#from skimage import io
#Read Image and convert it into gray
img=cv2.imread('/home/recode/Desktop/corrected.png')
# img=cv2.imread('Real_img/IMG_44.png')
# img=cv2.imread('Real_img/timg_55.jpg')

mask = cv2.inRange(img,(0,0,0),(200,200,200))
thresholded = cv2.cvtColor(mask,cv2.COLOR_GRAY2BGR)
inverted = 255-thresholded # black-in-white



gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

local_thresh=[]
for i in range(0,len(gray),1):
    for j in range(0,len(gray[0,:]),1):
        imblk=gray[i:i+1,j:j+1]
        local_thresh.append(imblk.mean())

global_thresh=(sum(local_thresh)/len(local_thresh))*0.95

binaryimg=np.zeros(gray.shape)

        
for i in range(len(gray)):
    for j in range(len(gray[0,:])):
        if gray[i,j]<global_thresh:
            binaryimg[i,j]=0
        else:
            binaryimg[i,j]=255



Start_pattern=81111113
Stop_pattern=711311121

def add_quiet_zone(im):
    box = (15, 15, im.size[0]+15, im.size[1]+15)
    img = Image.new('L', (im.size[0]+30, im.size[1]+30), 'white')
    img.paste(im, box)
    return img


# def find_startpattern(gray):
def reformat(row_data):
    return [(i[0], len(list(i[1]))) for i in itertools.groupby(row_data)]
    
def filter_quitezone(row_data):
    if row_data[0][0] == 255:  row_data = row_data[1:]
    if row_data[-1][0] == 255: row_data = row_data[0:-1]
    return row_data

#Read Codeword Combo
def codeword_combo(filename):
    f=open(filename,'r')
    code_combos=f.read()
    f.close()
    
    code_combos=code_combos.split('\n')
    codeword_list=[]
    for i in range(len(code_combos)):
        codes=code_combos[i].split(' ')
        codeword_list.append(codes)    
    codeword_list=np.asarray(codeword_list)
    return codeword_list


#Read Text Codeword
def Text_codeword(filename):
    f=open(filename,'r')
    code=f.read()
    f.close()
    code=code.split('\n')
    Textcode=[]
    for j in range(31):
        Textcode.append(code[j].split('\t')[1:])
    
    Textcode=pd.DataFrame(Textcode[1:],columns=Textcode[0])
    return Textcode

codeword_list=codeword_combo('PDF417codes/pdf417_codeword_combos.txt')



def find_x_y_codewordlen(gray):
    if gray[0,7]!=gray[0,8]:
        bar_x=1
        bar_y=3
        codewordlen=17
    elif gray[0,15]!=gray[0,16]:
        bar_x=2
        bar_y=6
        codewordlen=17*bar_x
    return bar_x,bar_y,codewordlen

def get_latch_shiftlist():
    latchlist={'ll':['Lower',0],'as':['Alpha',1],'al':['Alpha',0],'ml':['Mixed',0],'pl':['Punctuation',0],'ps':['Punctuation',1]}
    return latchlist

def Texttype_decode(code,Prev_char=None,Preprev_char=None,Prev_mode=None,Preprev_mode=None):
    S_c=code%30
    F_c=(code-S_c)/30
    Textcode=Text_codeword('PDF417codes/pdf417_text_code.txt')
    latchlist=get_latch_shiftlist()
    if Prev_char is None and Textcode['Alpha'][F_c] in latchlist:
        first_char=Textcode['Alpha'][F_c]
        textmode=Textcode['Alpha'][F_c]
        mode=latchlist[textmode]
        second_char=Textcode[mode[0]][S_c]
        pm=mode[0]
        ppm='Alpha'
    elif Prev_char is None and Textcode['Alpha'][F_c] not in latchlist:
        first_char=Textcode['Alpha'][F_c]
        ppm='Alpha'
        if first_char in latchlist:
            mode=latchlist[first_char]
            second_char=Textcode[mode[0]][S_c]
            pm=mode[0]
        else:
            second_char=Textcode['Alpha'][S_c]
            pm='Alpha'
    elif Prev_char is not None and Prev_char in latchlist:
        mode=latchlist[Prev_char]
        first_char=Textcode[mode[0]][F_c]
        ppm=mode[0]
        if mode[1]==0:
            second_char=Textcode[mode[0]][S_c]
            pm=mode[0]
        else:
            second_char=Textcode[Preprev_mode][S_c]
            pm=Preprev_mode
    else:
        first_char=Textcode[Prev_mode][F_c]
        ppm=Prev_mode
        if first_char in latchlist:
            mode=latchlist[first_char]
            second_char=Textcode[mode[0]][S_c]
            pm=mode[0]
        else:
            second_char=Textcode[Prev_mode][S_c]
            pm=Prev_mode
        
        
    return first_char,second_char,ppm,pm

def Bytetype_decode(codewords):
    codewordvalue=codewords[0]
    for c in range(1,len(codewords)):
        codewordvalue=(codewordvalue*900)+codewords[c]
    decoded=[]
    decoded_data=''
    decoded.append(codewordvalue%256)
    decoded_data=chr(int(decoded[-1]))+decoded_data
    for c in range(5):
        codewordvalue=(codewordvalue-decoded[-1])/256
        decoded.append(codewordvalue%256)
        decoded_data=chr(int(decoded[-1]))+decoded_data
    return decoded_data

def Numerictype_decode(codewords):
    codewordvalue=codewords[0]
    for c in range(1,len(codewords)):
        codewordvalue=(codewordvalue*900)+codewords[c]
    codewordvalue=str(codewordvalue)[1:]
    return codewordvalue
mindata=[]
def calc_codeword(row):
    # row=add_quiet_zone(row)
    sets=[]
    dat=reformat(row)
    dat=filter_quitezone(dat)
    cds=[]
    for cc in range(len(dat)):
        fg=0
        if len(cds)<8:
            cds.append(dat[cc][1])
        if len(cds)==8:
            minx=np.unique(cds)[0]
            # dif=np.unique(cds)[1]-np.unique(cds)[0]
            # if dif>1:
            #     minx=np.unique(cds)[0]
            # else:
            #     minx=np.unique(cds)[1]

            code=[]
            for i in cds:
                code.append(int(round(i/minx)))
            if sum(code)>17:
                fg=1
                minx=np.unique(cds)[1]
                code=[]
                for i in cds:
                    val=int(round(i/minx))
                    if val==0:
                        code.append(int(1))
                    else:
                        code.append(val)
            if sum(code)>10 and sum(code)<17:
                if fg==0:
                    minx=np.unique(cds)[0]
                else:
                    minx=np.unique(cds)[1]
                code=[]
                for i in cds:
                    val=int(round(i/minx))
                    if val==0:
                        code.append(int(1))
                    else:
                        code.append(val)
                        
            if sum(code)<17:
                code=[]
                for i in cds:
                    val=int(ceil(i/minx))
                    if val==0:
                        code.append(int(1))
                    else:
                        code.append(val)
            mindata.append([minx,np.unique(cds)[0]])
            cds=[]
            sets.append(code)
            
    return sets
            
            
words=[]    
wordrows=[]
for c in range(len(binaryimg)):
    row=binaryimg[c,:]
    per=len(row)*0.2
    zelen,onlen=len(np.where(row==0)[0]),len(np.where(row==255)[0])
    if zelen>per and onlen>per:
        codes=calc_codeword(row)
        words.append(codes)
        wordrows.append(row)
        

# bar_x,bar_y,codewordlen=find_x_y_codewordlen(gray)
Decoded_data=''
Rownumbers=[]
codepatterns=[]
latchlist=get_latch_shiftlist()

Prev_char=None
Preprev_char=None
Prev_mode=None
Preprev_mode=None
codewords=[]
flg=0
fin_flg=0
words[45][2]=[4,2,1,1,1,2,1,5]
cnt=1
for c in range(len(words)):
    rows=[]
    
    if c==0:
        rows=words[c]
        codepattern=('').join([str(i) for i in rows[3]])
        rowno=int(codeword_list[np.where(codeword_list==codepattern)[0][0],0])
        Pdf_mode=rowno
    elif words[c][1] != words[c-1][1] and sum(words[c][-2])==17:
        rows=words[c]
        
    if rows:
        newrows=[]
        cnt=c
        for i in range(cnt,len(words)-2):
            if words[i][1] == words[i+1][1] or words[i][2] == words[i+1][2]:
                newrows.append(words[i])
            else:
                break
        newrow=[]
        for cn in range(len(newrows[1])):
            cntdict={}
            for cc in range(len(newrows)):
                if sum(newrows[cc][cn])==17:
                    val=('').join([str(i) for i in newrows[cc][cn]])
                    if val in cntdict:
                        cntdict[val]=cntdict[val]+1
                    else:
                        cntdict[val]=1
            newrow.append(sorted (cntdict.keys())[-1])
        
    
        # for k in range(len(rows)):
        #     val=('').join([str(i) for i in rows[k]])
        #     if sum(rows[k])!=17 or val not in codeword_list:
        #         for i in range(len(newrows)):
        #             rowval=newrows[i]
        #             val=('').join([str(i) for i in rowval[k]])
        #             if sum(rowval[k])==17 and val in codeword_list:
                        
        #                 rows[k]=rowval[k]
        c=cnt
        rows=newrow
        if c==0:
            rows=rows[3:-2]
        else:
            rows=rows[2:-2]
                        
        flg=0    
        Pdf_mode=900   
        for val in rows:
            if fin_flg==0:
                dat=''
                # val=('').join([str(i) for i in val])
                rowno=int(codeword_list[np.where(codeword_list==val)[0][0],0])
                if flg==0:
                    if rowno==900 or rowno==901 or rowno==902:
                        fin_flg=fin_flg+1
                    if Pdf_mode==900 and fin_flg==0:
                        first_char,second_char,Preprev_mode,Prev_mode=Texttype_decode(rowno,Prev_char,Preprev_char,Prev_mode,Preprev_mode)
                        Prev_char=second_char
                        Preprev_char=first_char
                        
                        if first_char not in latchlist:
                            if first_char=='space':
                                dat=dat+' '
                            else:
                                dat=dat+first_char
        
                        if second_char not in latchlist:
                            if second_char=='space':
                                dat=dat+' '
                            else:
                                dat=dat+second_char
                    elif (Pdf_mode==901 or Pdf_mode==924) and fin_flg==0:
                        if Pdf_mode==901:#Its not multiple of 6
                            codewords.append(rowno)
                            if len(codewords)%5==0: #Multiple of 6
                                dat=Bytetype_decode(codewords)
                            elif len(codewords)>5:
                                dat=chr(int(rowno))
                            
                        elif Pdf_mode==924:
                            codewords.append(rowno)
                            if len(codeword_list)%5==0:
                                dat=Bytetype_decode(codewords)
                            elif len(codewords)>5:
                                dat=chr(int(rowno))
                        
                    elif Pdf_mode==902 and fin_flg==0:
                        codewords.append(rowno)
                        if (len(codewords)%15==0 and fin_flg==0) or (len(codewords)<15 and fin_flg==1):
                            if fin_flg==1:
                                dat=Numerictype_decode(codewords[0:-1])
                            else:
                                dat=Numerictype_decode(codewords)
                        
                    Decoded_data=Decoded_data+dat
              
                codepatterns.append(codepattern)
                Rownumbers.append(int(rowno))

io.imshow(img)
print('Decoded Data')
print(Decoded_data)



        