import math
import numpy as np

class Modulus():
    def __init__(self):
        self.mod=929
        
        "Create Exponent and Log Tables"
        
        ExpTable=np.zeros([self.mod])
        LogTable=np.zeros([self.mod])
        value=1
        
        for Index in range(len(ExpTable)):
            ExpTable[Index]=int(value)
            LogTable[value]=int(Index)
            value=int((3*value)%self.mod)
        self.ExpTable=ExpTable
        self.LogTable=LogTable
        
    """Add Two values"""    
    def Add(self,argA,argB):
        return (argA+argB)%self.mod
    
    """Subtract Two Values"""
    def Subtract(self,argA,argB):
        return (self.mod+argA-argB)%self.mod
    
    """Negate a value"""
    def Negate(self,arg):
        return (self.mod-arg)%self.mod
    
    """Invert A number"""
    def Inverse(self,arg):
        ExpTable=self.ExpTable
        LogTable=self.LogTable
        return ExpTable[int(self.mod-LogTable[int(arg)]-1)]
    
    """Multiply Two numbers"""
    def Multiply(self,argA,argB):
        ans=0
        ExpTable=self.ExpTable
        LogTable=self.LogTable
        if argA==0 or argB==0:
            ans=0
        else:
            ans=ExpTable[int((LogTable[int(argA)]+LogTable[int(argB)])%(self.mod-1))]
        return ans
    
    """Devide Two numbers"""
    def Divide(self,argA,argB):
        return self.Multiply(argA, int(self.Inverse(argB)))
    