import EC_Modulus
from copy import deepcopy
import numpy as np
class Polynomials():
   
    def __init__(self, Coefficients = None, Degree=None,Flg=None):
        self.Coefficients=[]    #Polynomial Coefficients
        self.PolyLength=0    #Polynomial Length
        self.PolyDegree=0    #Polynomial Degree
       
        if Flg is None:
            self.Zero=Polynomials([0],None,1)
        else:
            pass

        if Flg is None:
            self.One=Polynomials([1],None,1)
        else:
            pass
#        self.One = One
#        self.Zero = Zero


        
        if Coefficients is not None:
        
            if Degree is None:
                
                self.Polynomial(Coefficients)
            else:
                self.PolyDegree = Degree
                self.Polynomial_Deg(Degree, Coefficients)
                   
       
    def Polynomial(self,Coefficients):
        PolyLength=len(Coefficients)
       
        if PolyLength>1 and Coefficients[0]==0:
            FirstNonZero=0
           
            while(FirstNonZero < PolyLength and Coefficients[FirstNonZero] == 0):
                FirstNonZero = np.where(np.asarray(Coefficients) > 0)[0][0]
                if FirstNonZero == PolyLength:
                    self.Coefficients=[0]
                    PolyLength=1
                else:
                    PolyLength=PolyLength - FirstNonZero
                    self.Coefficients=[0] * PolyLength
                    self.Coefficients[0:PolyLength]=Coefficients[FirstNonZero:FirstNonZero+PolyLength]
                FirstNonZero = FirstNonZero + 1
                   
        else:
            self.Coefficients=Coefficients
            
        self.PolyDegree=PolyLength-1
        self.PolyLength=PolyLength
        self.LastCoefficient=self.LastCoefficient_val(self.Coefficients)
        self.LeadingCoefficient=self.LeadingCoefficient_val(self.Coefficients)
        self.IsZero=self.isZero_chk(self.Coefficients)
        return self        
    
    # polynomial constructor for monomial
    def Polynomial_Deg(self,Degree,Coefficient):
        #create polynomial coefficients array with one leading non zero value
        self.PolyDegree = Degree
        self.PolyLength = Degree + 1  
        Coefficients = [0] * self.PolyLength
        Coefficients[0] = Coefficient   
        self.Coefficients=Coefficients
        self.LastCoefficient=self.LastCoefficient_val(Coefficients)
        self.LeadingCoefficient=self.LeadingCoefficient_val(Coefficients)
        self.IsZero=self.isZero_chk(Coefficients)
        return self
    
    def isZero_chk(self,Coefficients):
        if Coefficients[0]==0:
            return True
        else:
            return False
    
    def GetCoefficient(self,Degree):
        coefficient=self.Coefficients
        return coefficient[self.PolyDegree-Degree]
    
    def LastCoefficient_val(self, Coefficients):
        coefficient=Coefficients
        return coefficient[self.PolyDegree]
    
    def LeadingCoefficient_val(self, Coefficients):
        coefficient=Coefficients
        return coefficient[0]
    
    def EvaluateAt(self,xvalue):
        Modulus = EC_Modulus.Modulus()
        if xvalue == 0: return self.Coefficients[0]
        
        Result= 0 
        
        if xvalue == 1:
            Result=0
            for coeff in self.Coefficients:
                Result=Modulus.Add(int(Result),int(coeff))
        else:
            Result=self.Coefficients[0]
            for index in range(1,self.PolyLength):
                Result = Modulus.Add(Modulus.Multiply(int(xvalue), int(Result)), int(self.Coefficients[index]))
        return Result
    
    
    def Add(self,other_poly):
        Modulus = EC_Modulus.Modulus()
        if self.IsZero:
            return other_poly
        if other_poly.IsZero:
            return self
        smaller=self.Coefficients
        larger=other_poly.Coefficients
        
        if len(smaller) > len(larger):
            temp = deepcopy(smaller)
            smaller = deepcopy(larger)
            larger = deepcopy(temp)
        
        Result=[0] * len(larger)
        
        Delta = len(larger) - len(smaller)
        Result[0:Delta]=larger[0:Delta]
        
        for  index in range(Delta,len(larger)):
            Result[index]=Modulus.Add(smaller[index - Delta], larger[index])
        return Polynomials(Result)
        
        
    def Subtract(self,other_poly):
        if other_poly.IsZero:
            return self
        prev_res = deepcopy(other_poly.MakeNegative())
        Result = self.Add(deepcopy(prev_res))
        return Result
    
    def Multiply(self,other_poly):
        Modulus = EC_Modulus.Modulus()
        if self.IsZero or other_poly.IsZero:
            return self.Zero
        
        otherCoefficients = other_poly.Coefficients
        otherlength = other_poly.PolyLength
        Result=[0] * (self.PolyLength + otherlength -1)
        for i in (range(self.PolyLength)):
            coeff=self.Coefficients[i]            
            for j in range(otherlength):
                Result[i + j]= Modulus.Add(Result[i + j], Modulus.Multiply(coeff, otherCoefficients[j]))
        return Polynomials(Result)

    def MakeNegative(self):
        Modulus = EC_Modulus.Modulus()
        Result = [0] * self.PolyLength
        for i in range(self.PolyLength):
            Result[i]= Modulus.Negate(self.Coefficients[i])
        return Polynomials(Result)
    
    """Multiply by a constant""" 
    def MultiplyByConstant(self,Constant):
        Modulus = EC_Modulus.Modulus()
        if Constant == 0:
            return self.Zero
        if Constant == 1:
            return self
        Result  = [0] * self.PolyLength
        for index in range(self.PolyLength):
            Result[index] = Modulus.Multiply(self.Coefficients[index],Constant)
        
        return Polynomials(Result)
    
    """Multiply by monomial"""
    def MultiplyBymonomial(self,Degree,Constant):
        Modulus = EC_Modulus.Modulus()
        if Constant == 0:
            return self.Zero
        Result = [0] * (self.PolyLength + Degree)
        
        for index in range(self.PolyLength):
            Result[index] = Modulus.Multiply(self.Coefficients[index],Constant)
        return Polynomials(Result)
    
                
        
cc=Polynomials().One        