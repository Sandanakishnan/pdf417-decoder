import EC_Modulus
import EC_Polynomial
from copy import deepcopy
import time

class ErrorCorrection():
    def Testcodewords(self,Codewords,ErrorCorrectionLength):
        Modulus=EC_Modulus.Modulus()
        #Create Codewords Polynomial
        PolyCodewords=EC_Polynomial.Polynomials(Codewords)
#        PolyCodewords=PolyCodewords.Polynomial(Codewords)
        
        #Create Syndrome Coefficients Array
        Syndrome = [0] * ErrorCorrectionLength
        Error = False
        
        # test for errors
		 #  if the syndrom array is all zeros, there is no error
        for index in range(ErrorCorrectionLength,0,-1):
             Syndrome[ErrorCorrectionLength - index] = PolyCodewords.EvaluateAt(Modulus.ExpTable[index])
             lhs = Syndrome[ErrorCorrectionLength - index]
             if lhs != 0:
                 Error = True
        if not Error:
            return 0
        
        PolySyndrome=EC_Polynomial.Polynomials(Syndrome)
#        PolySyndrome=PolySyndrome.Polynomial(Syndrome)
        ErrorLocator, ErrorEvaluator, Euc_result = self.EuclideanAlgorithm(ErrorCorrectionLength, PolySyndrome)
        if Euc_result == False:
            return -1
        # error locator (return -1 if error cannot be corrected)
        ErrorLocations = self. FindErrorLocations(ErrorLocator)
        
        if ErrorLocations is None:
            return -1
        
        FormalDerivative = self.FindFormalDerivatives(ErrorLocator)
        
        Errors = len(ErrorLocations)
        
        for index in range(Errors):
            ErrLoc = ErrorLocations[index]    # Error Location
            ErrPos = int(len(Codewords) - 1 - Modulus.LogTable[int(Modulus.Inverse(ErrLoc))])
            
            ErrorMagnitude = Modulus.Divide(Modulus.Negate(ErrorEvaluator.EvaluateAt(ErrLoc)), FormalDerivative.EvaluateAt(ErrLoc))    #error magnitude
            if ErrPos < 0:
                return -1
            Codewords[ErrPos] = int(Modulus.Subtract(Codewords[ErrPos], ErrorMagnitude))
            ErrorLocations[index] = ErrPos    #save error position in error locations array
            
        return Codewords
    
    def EuclideanAlgorithm(self,ErrorCorrectionLength, PolyR):
        Modulus=EC_Modulus.Modulus()
        ErrorLocator = None
        ErrorEvaluator = None
        
        PolyRLast=EC_Polynomial.Polynomials(1, ErrorCorrectionLength)
#        PolyRLast=PolyRLast.Polynomial_Deg(ErrorCorrectionLength, 1)
        
        PolyTLast = EC_Polynomial.Polynomials()
        PolyTLast = PolyTLast.Zero
        PolyT = EC_Polynomial.Polynomials()
        PolyT = PolyT.One
        
        while (PolyR.PolyDegree >= ErrorCorrectionLength / 2):
            PolyRLast2 = deepcopy(PolyRLast)
            PolyTLast2 = deepcopy(PolyTLast)
            PolyRLast = deepcopy(PolyR)
            PolyTLast = deepcopy(PolyT)
            
            if PolyRLast.IsZero:
                return ErrorLocator, ErrorEvaluator, False
            
            PolyR=deepcopy(PolyRLast2)
            
            Quotient = EC_Polynomial.Polynomials().Zero
#            Quotient = Quotient.One
            
            dltInverse = Modulus.Inverse(PolyRLast.LeadingCoefficient)
            
            while ((PolyR.PolyDegree >= PolyRLast.PolyDegree) and PolyR.IsZero is False ):
                Scale = Modulus.Multiply(PolyR.LeadingCoefficient, dltInverse)
                DegreeDiff = PolyR.PolyDegree - PolyRLast.PolyDegree
                
                In_Quotient=EC_Polynomial.Polynomials(Scale, DegreeDiff)
#                In_Quotient=PolyRLast.Polynomial_Deg(DegreeDiff, Scale)
                Quotient = Quotient.Add(In_Quotient)    # Build Quotient
                Prev_res = deepcopy(PolyRLast.MultiplyBymonomial(DegreeDiff, Scale))
                PolyR = deepcopy(PolyR.Subtract(Prev_res))
            
            PolyT = Quotient.Multiply(PolyTLast).Subtract(PolyTLast2).MakeNegative()
        
        SigmaTildeAtZero = PolyT.LastCoefficient
        if SigmaTildeAtZero == 0:
            return ErrorLocator, ErrorEvaluator, False
        
        Inverse = Modulus.Inverse(SigmaTildeAtZero)
        ErrorLocator = PolyT.MultiplyByConstant(Inverse)
        ErrorEvaluator = PolyR.MultiplyByConstant(Inverse)
        
        return ErrorLocator, ErrorEvaluator, True
    
    def FindErrorLocations(self, ErrorLocator):
        Modulus=EC_Modulus.Modulus()
        LocatorDegree = ErrorLocator.PolyDegree
        ErrorLocations = [0] * LocatorDegree
        ErrCount = 0
        
        Index=1
        while(Index < Modulus.mod and ErrCount < LocatorDegree):
            if(ErrorLocator.EvaluateAt(Index) == 0):
                ErrorLocations[ErrCount] = Index
                ErrCount = ErrCount +1
            Index = Index + 1
        
        if ErrCount == LocatorDegree:
            return ErrorLocations
        else:
            return None
        
    def FindFormalDerivatives(self, ErrorLocator):
        #Finds the error magnitudes by directly applying Forney's Formula
        Modulus=EC_Modulus.Modulus()
        
        LocatorDegree = ErrorLocator.PolyDegree
        DerivativesCoefficients = [0] * (LocatorDegree)
        Index=1
        while Index <= LocatorDegree:
            DerivativesCoefficients[LocatorDegree - Index] = Modulus.Multiply(Index, ErrorLocator.GetCoefficient(Index))
            Index = Index + 1
        
        Res_poly = EC_Polynomial.Polynomials(DerivativesCoefficients)
#        Res_poly = Res_poly.Polynomial(DerivativesCoefficients)
        return  Res_poly
        
