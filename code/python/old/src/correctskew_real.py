import cv2
import itertools
import numpy as np

img=cv2.imread('timg_55.jpg')

gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

def binarize(gray):
    local_thresh=[]
    for i in range(0,len(gray),1):
        for j in range(0,len(gray[0,:]),1):
            imblk=gray[i:i+1,j:j+1]
            local_thresh.append(imblk.mean())
    
    global_thresh=(sum(local_thresh)/len(local_thresh))*0.96
    
    binaryimg=np.zeros(gray.shape)
    
            
    for i in range(len(gray)):
        for j in range(len(gray[0,:])):
            if gray[i,j]<global_thresh:
                binaryimg[i,j]=0
            else:
                binaryimg[i,j]=255
    return binaryimg

def reformat(row_data):
    return [(i[0], len(list(i[1]))) for i in itertools.groupby(row_data)]

def filter_quitezone(row_data):
    if row_data[0][0] == 255:  row_data = row_data[1:]
    if row_data[-1][0] == 255: row_data = row_data[0:-1]
    return row_data

def calc_codeword(row):
    # row=add_quiet_zone(row)
    sets=[]
    dat=reformat(row)
    dat=filter_quitezone(dat)
    cds=[]
    for cc in range(len(dat)):
        if len(cds)<8:
            cds.append(dat[cc][1])
        if len(cds)==8:
            minx=min(cds)
            code=[]
            for i in cds:
                code.append(round(i/minx))
            if sum(code)>17:
                minx=np.unique(cds)[1]
                code=[]
                for i in cds:
                    val=int(round(i/minx))
                    if val==0:
                        code.append(int(1))
                    else:
                        code.append(val)
            cds=[]
            sets.append(code)
    return sets

binaryimg=binarize(gray)

startpattern=[]
stoppattern=[]
for i in range(len(binaryimg)):
    row=binaryimg[i,:]
    per=len(row)*0.2
    zelen,onlen=len(np.where(row==0)[0]),len(np.where(row==255)[0])
    if zelen>per and onlen>per:
        pattern=calc_codeword(row)
        startpattern.append(pattern[0])
        stoppattern.append(pattern[-1])