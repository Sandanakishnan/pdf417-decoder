import cv2
import itertools

img=cv2.imread('a.png')

gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)


def reformat(row_data):
    return [(i[0], len(list(i[1]))) for i in itertools.groupby(row_data)]


def calc_codeword(row):
    sets=[]
    dat=reformat(row)
    cds=[]
    for cc in range(len(dat)):
        if len(cds)<8:
            cds.append(dat[cc][1]) 
        if len(cds)==8:
            sets.append(cds)
            cds=[]
    return sets

startpattern=[]
stoppattern=[]
for i in range(len(gray)):
    pattern=calc_codeword(gray[i,:])
    startpattern.append(pattern[0])
    stoppattern.append(pattern[-1])