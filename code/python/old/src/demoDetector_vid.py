import cv2
import math
from scipy import ndimage
import numpy as np
import warptransform
from dodo_detector.detection import KeypointObjectDetector
from vap.sources import VideoFeed
import time
import random


def rotation_img(img):
    random.seed(0)
    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    img_edges = cv2.Canny(img_gray, 100, 100, apertureSize=3)
    cv2.imshow('edges',img_edges)
    lines = cv2.HoughLinesP(img_edges, 1, 1, 50, 50, maxLineGap=5)
    _,contours,hierarchy = cv2.findContours(img_edges,1,2)
    angles = []
    if lines is not None:
        for x1, y1, x2, y2 in lines[0]:
            cv2.line(img, (x1, y1), (x2, y2), (255, 0, 0), 3)
            angle = math.degrees(math.atan2(y2 - y1, x2 - x1))
            angles.append(angle)
            
        
        median_angle = np.median(angles)
        
        
        img_rotated = ndimage.rotate(img, median_angle)
        return img_rotated

def find_rotate(image,obj_dict):
    r=10
    x,y,w,h=obj_dict['pdf417'][0]['box']
    
    cropimage=image.copy()
    croped = cropimage[x-r:w+r,y-r:h+r,:]
    
    gray = cv2.cvtColor(croped, cv2.COLOR_BGR2GRAY)
        
    clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
    gray = clahe.apply(gray)
    
    # edge enhancement
    edge_enh = cv2.Laplacian(gray, ddepth = cv2.CV_8U, 
                             ksize = 3, scale = 1, delta = 0)

    # bilateral blur, which keeps edges
    blurred = cv2.bilateralFilter(edge_enh, 13, 50, 50)
    
    # use simple thresholding. adaptive thresholding might be more robust
    (_, thresh) = cv2.threshold(blurred, 55, 255, cv2.THRESH_BINARY)
    
    # do some morphology to isolate just the barcode blob
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (9, 9))
    closed = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel)
    closed = cv2.erode(closed, None, iterations = 4)
    closed = cv2.dilate(closed, None, iterations = 4)
    
    # find contours left in the image
    _,cnts,hierarchy= cv2.findContours(closed.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    c = sorted(cnts, key = cv2.contourArea, reverse = True)[0]
    rect = cv2.minAreaRect(c)
    box = np.int0(cv2.boxPoints(rect))
    cv2.drawContours(croped, [box], -1, (0, 255, 0), 3)
    
    
    warp_img=warptransform.four_point_transform(gray,box)
    return warp_img

def main():
    detector = KeypointObjectDetector('/home/recode/Downloads/Pdf417_barcodes/','KAZE')
    
    cnt=1
    cam=VideoFeed(2)
    while True:
            for frame in cam:
                start=time.time()
                cv2.imshow('Image', frame)
                orimg=frame.copy()
                marked_image, obj_dict = detector.from_image(frame)
                print(obj_dict)
                stop=time.time()-start
                # cv2.imwrite('k1/'+str(cnt)+'.png',marked_image)
                cv2.imshow('Detected', marked_image)
                cv2.waitKey(10)
                if obj_dict:
                    dict_ob=obj_dict['pdf417'][0]['box']
                    x,y,w,h=dict_ob
                    crp_img=orimg[x:w,y:h,:]
                    if sum(1 for number in dict_ob if number < 0)<=0 and len(crp_img)>70 and len(crp_img[1:])>70:
                        rotated=find_rotate(orimg,obj_dict)
        
                        # x,y,w,h=obj_dict['pdf417'][0]['box']
                        # cropedimage=orimg[x:w,y:h,:]
                        # rotated=rotation_img(cropedimage)
                        # if rotated is not None:
                        cv2.imshow('Rotated_image',rotated)
                cnt=cnt+1
                print(stop)
    del cam
    
    
main()