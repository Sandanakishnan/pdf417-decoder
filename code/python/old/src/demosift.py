import cv2
import math
from scipy import ndimage
import numpy as np
from dodo_detector.detection import KeypointObjectDetector
detector = KeypointObjectDetector('/home/recode/Downloads/Pdf417_barcodes/','KAZE')

def rotation_img(img):
    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    img_edges = cv2.Canny(img_gray, 100, 100, apertureSize=3)
    
    lines = cv2.HoughLinesP(img_edges, 1, math.pi / 360.0, 50, minLineLength=100, maxLineGap=6)
    
    angles = []
    
    for x1, y1, x2, y2 in lines[0]:
        # cv2.line(img, (x1, y1), (x2, y2), (255, 0, 0), 3)
        angle = math.degrees(math.atan2(y2 - y1, x2 - x1))
        angles.append(angle)
        
    
    # angles
    # Out[18]: [13.807519016201312]
    
    median_angle = np.median(angles)
    
    # median_angle
    # Out[20]: 13.807519016201312
    
    img_rotated = ndimage.rotate(img, median_angle)
    return img_rotated


import time
from glob import glob
files=glob('input1/**')
cnt=1
for i in files:
    start=time.time()
    im=cv2.imread(i)
    orimg=im.copy()
    marked_image, obj_dict = detector.from_image(im)
    print(obj_dict)
    stop=time.time()-start
    cc=marked_image.copy()
    cc=cv2.resize(cc,(920,720))
    cv2.imshow('Detected Barcode',cc)
    cv2.waitKey(10)
    if obj_dict:
        x,y,w,h=obj_dict['pdf417'][0]['box']
        cropedimage=orimg[x:w,y:h,:]
        rotated=rotation_img(cropedimage)
        cv2.imshow('Rotated_image',rotated)
    
    
    cv2.imwrite('k1/'+str(cnt)+'.png',marked_image)
    cnt=cnt+1
    print(stop)
    
    

# kps = alg.detect(image)
# img1 = cv2.imread('timages_1.png',0)
# img1 = cv2.imread('WIN_20200203_14_41_51_Pro.jpg')
# kps = alg.detect(img1)
# kps = sorted(kps, key=lambda x: -x.response)[:vector_size]
# vector_size=32
# kps = sorted(kps, key=lambda x: -x.response)[:vector_size]
# kps, dsc = alg.compute(image, kps)