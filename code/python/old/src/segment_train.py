import segmentation_models as sm
import cv2
from glob import glob
from sklearn.model_selection import train_test_split
# import numpy as np
imagefiles=glob('ELPrz/**')
# GTfiles=glob('webgtrz/**')

# data=[]
# gt=[]
# for i in range(len(imagefiles)):
#     imname=imagefiles[i].split('/')[1]
#     img=cv2.imread(imagefiles[i])
#     # img=cv2.resize(img,(576,384))
#     # cv2.imwrite('webRz/'+imname.split('.')[0]+'.png',img)
#     gtimg=cv2.imread('webgtrz/'+imname.split('.')[0]+'.png')
#     # gtimg=cv2.resize(gtimg,(576,384))
#     # cv2.imwrite('webgtrz/'+imname.split('.')[0]+'.png',gtimg)
#     data.append(img)
#     gt.append(gtimg)

# data=np.asarray(data)    
# gt=np.asarray(gt)    
# # load your data
# x_train, x_val,y_train, y_val = train_test_split(data,gt,test_size=0.2)



# BACKBONE = 'resnet34'
# # preprocess_input = sm.get_preprocessing(BACKBONE)



# # preprocess input
# # x_train = preprocess_input(x_train)
# # x_val = preprocess_input(x_val)

# # define model
# model = sm.Linknet(input_shape=(384,576,3),classes=2)
# model.compile(
#     'Adam',
#     loss=sm.losses.bce_jaccard_loss,
#     metrics=[sm.metrics.iou_score],
# )

# # fit model
# # if you use data generator use model.fit_generator(...) instead of model.fit(...)
# # more about `fit_generator` here: https://keras.io/models/sequential/#fit_generator
# model.fit(
#    x=x_train,
#    y=y_train,
#    batch_size=16,
#    epochs=10,
#    validation_data=(x_val, y_val),
# )





from keras_segmentation.models.pspnet import pspnet

model1 = pspnet(n_classes=2 ,  input_height=576, input_width=1152  )

model1.train(
    train_images =  "webRz/",
    train_annotations = "webgtrz/",
    epochs=2
)


for c in range(len(imagefiles)):
    imname=imagefiles[c]
    out = model1.predict_segmentation(
        inp=imname,
        out_fname="outputELP/"+imname.split('/')[1]
    )