import numpy as np
from skimage import io
from slidingwindow import slidingWindow
from glob import glob
from descriptor import Descriptor
import cv2
import pickle
from sklearn.preprocessing import StandardScaler
import time
imagefiles=glob('WebTest/**')
# trainfeat=np.load('webfeatures.npy')
# 
# scaler = StandardScaler().fit(trainfeat)


filename = 'websvm64.svm'
TrainedSvm = pickle.load(open(filename, 'rb'))

for i in range(len(imagefiles)):
    imname=imagefiles[i].split('/')[1]
    start=time.time()
    img=cv2.imread(imagefiles[i])
    outimg=img.copy()
    img=cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    windows=slidingWindow((1280,720))
    
    for j in range(len(windows)):
        sz=windows[j]
        winimg=img[sz[2]:sz[3],sz[0]:sz[1]]
        descriptor = Descriptor(hog_features=True, hist_features=True,spatial_features=False, stat_features=True,hog_lib=None, size=(64,64),hog_bins=9, pix_per_cell=(8,8),cells_per_block=(2,2), block_stride=None,block_norm='L1', transform_sqrt='True',signed_gradient='False', hist_bins=16,spatial_size=(16,16))
        testfeat=descriptor.getFeatureVector(winimg)
        tc=np.zeros([1,len(testfeat)])
        tc[0,:]=testfeat
        # testfeat = scaler.transform(testfeat) 
        prdresult=TrainedSvm.predict(tc)
        
        if prdresult==1:
            # print(prdresult)
            outimg[sz[2]:sz[3],sz[0]:sz[1],2]=255
    print('exec_tim= ',(time.time()-start))
    cv2.imwrite('out/'+imname,outimg)